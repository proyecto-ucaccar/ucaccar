==========================================================
Listado de ficheros contenidos UWHPSC
==========================================================

Esto es un listado de los ficheros contenidos en el repositorio_ (en
el día 16/3/2015). En principio, la idea es traducirlos todos ellos, o
al menos la mayor parte de ellos (los que creamos más interesantes). Además, si vamos a incluir paralelización con C/C++, habrá que añadir secciones adicionales.

Varios (Rafa)
=============
- `about.rst`_ (lineas=52, palabras=306): **Traducido por Dani**
- `aws.rst`_ (lineas=261, palabras=1572): **Por ahora no lo traduzco (¿lo vamos a incluir?)**
- `biblio.rst`_ (lineas=367, palabras=934): *¿Asignado a Rafa?* (buscar bibliografía en español)
- `class_format.rst`_ (lineas=59, palabras=396) **Por ahora no lo traduzco (¿lo vamos a incluir?)**
- `index.rst`_ (lineas=172, palabras=226) **Mirar la documentación de Sphinx para la creación automátia de páginas**
- `memory.rst`_ (lineas=250, palabras=1800)
- `metrics.rst`_ (lineas=128, palabras=883)
- `random.rst`_ (lineas=18, palabras=43)
- `outline.rst`_ (lineas=55, palabras=229)
- `smc.rst`_ (lineas=165, palabras=916)
- `software_carpentry.rst`_ (lineas=36, palabras=132)
- `software_installation.rst`_ (lineas=314, palabras=1226)
- `toc_condensed.rst`_ (lineas=10, palabras=14)

Python (Sandra)
===============
- `animation.rst`_ (lineas=82, palabras=341)
- `ipython_notebook.rst`_ (lineas=64, palabras=257)
- `nose.rst`_ (lineas=35, palabras=179)
- `numerical_python.rst`_ (lineas=234, palabras=1149)
- `python_and_fortran.rst`_ (lineas=38, palabras=231)
- `python_debugging.rst`_ (lineas=239, palabras=1001)
- `python_functions.rst`_ (lineas=80, palabras=272) **Traducido por Sandra**
- `python_plotting.rst`_ (lineas=112, palabras=385) **Traducido por Sandra pero dudas**
- `python.rst`_ (lineas=646, palabras=3291) **En proceso**
- `python_scripts_modules.rst`_ (lineas=407, palabras=1934)
- `python_strings.rst`_ (lineas=64, palabras=299) **Traducido por Sandra**
- `sage.rst`_ (lineas=25, palabras=84) **Traducido por Sandra**

Control de versiones (Daniel) **TRADUCIDO**
============================================
- `bitbucket.rst`_ (lineas=47, palabras=329) **Traducido por Dani**
- `git_more.rst`_ (lineas=39, palabras=202) **Traducido por Dani**
- `git.rst`_ (lineas=556, palabras=3018) **Traducido por Dani**
- `versioncontrol.rst`_ (lineas=90, palabras=652) **Traducido por Dani**

Informática y sistemas Unix (Daniel) **TRADUCIDO**
====================================================
- `computer_arch.rst`_ (lineas=82, palabras=562) **Traducido por Dani**
- `computing_options.rst`_ (lineas=61, palabras=323) **Traducido por Dani**
- `editors.rst`_ (lineas=74, palabras=310) **Traducido por Dani**
- `makefiles.rst`_ (lineas=93, palabras=285) **Traducido por Dani**
- `punchcard.rst`_ (lineas=49, palabras=291) **Traducido por Dani**
- `reproducibility.rst`_ (lineas=94, palabras=776) **Traducido por Dani**
- `shells.rst`_ (lineas=182, palabras=1019) **Traducido por Dani**
- `special_functions.rst`_ (lineas=127, palabras=743) **Traducido por Dani**
- `sphinx.rst`_ (lineas=107, palabras=585) **Traducido por Dani**
- `ssh.rst`_ (lineas=65, palabras=313) **Traducido por Dani**
- `timing.rst`_ (lineas=192, palabras=809) **Traducido por Dani**
- `top.rst`_ (lineas=133, palabras=845) **Traducido por Dani**
- `unix.rst`_ (lineas=467, palabras=2424) **Traducido por Dani**
- `vm.rst`_ (lineas=393, palabras=2476) **Traducido por Dani**


Fortran (Alejandro)
===================
- `fortran_arrays.rst`_ (lineas=263, palabras=1503)
- `fortran_debugging.rst`_ (lineas=85, palabras=387)
- `fortran_io.rst`_ (lineas=115, palabras=498)
- `fortran_modules.rst`_ (lineas=155, palabras=542)
- `fortran_newton.rst`_ (lineas=56, palabras=169)
- `fortran.rst`_ (lineas=501, palabras=2037) **Traducido por Alejandro (faltan detalles).**
- `fortran_sub.rst`_ (lineas=156, palabras=729)
- `fortran_taylor.rst`_ (lineas=52, palabras=199)
- `gfortran_flags.rst`_ (lineas=168, palabras=990)
- `project_hints.rst`_ (lineas=135, palabras=1058)
- `project.rst`_ (lineas=357, palabras=2095)

Homework
========
- `homework1.rst`_ (lineas=262, palabras=1298)
- `homework2.rst`_ (lineas=269, palabras=1340)
- `homework3.rst`_ (lineas=202, palabras=1162)
- `homework4.rst`_ (lineas=267, palabras=1693)
- `homework4_solution.rst`_ (lineas=57, palabras=260)
- `homeworks.rst`_ (lineas=31, palabras=152)
- `lapack_examples.rst`_ (lineas=36, palabras=112)
- `linalg.rst`_ (lineas=84, palabras=222)

Paralelización (Estefanía)
==========================
- `jacobi1d_mpi.rst`_ (lineas=25, palabras=66)
- `jacobi1d_omp1.rst`_ (lineas=25, palabras=54)
- `jacobi1d_omp2.rst`_ (lineas=24, palabras=56)
- `mpi.rst`_ (lineas=132, palabras=491)
- `openmp.rst`_ (lineas=150, palabras=723)
- `poisson.rst`_ (lineas=144, palabras=1117)

.. _repositorio: https://bitbucket.org/rrgalvan/uwhpsc


.. _`about.rst`: notes-es/about.rst
.. _`aws.rst`: notes-es/aws.rst
.. _`biblio.rst`: notes-es/biblio.rst
.. _`class_format.rst`: notes-es/class_format.rst
.. _`index.rst`: notes-es/index.rst
.. _`memory.rst`: notes-es/memory.rst
.. _`metrics.rst`: notes-es/metrics.rst
.. _`random.rst`: notes-es/random.rst
.. _`outline.rst`: notes-es/outline.rst
.. _`smc.rst`: notes-es/smc.rst
.. _`software_carpentry.rst`: notes-es/software_carpentry.rst
.. _`software_installation.rst`: notes-es/software_installation.rst
.. _`toc_condensed.rst`: notes-es/toc_condensed.rst
.. _`animation.rst`: notes-es/animation.rst
.. _`nose.rst`: notes-es/nose.rst
.. _`python.rst`: notes-es/python.rst
.. _`sage.rst`: notes-es/sage.rst
.. _`bitbucket.rst`: notes-es/bitbucket.rst
.. _`git.rst`: notes-es/git.rst
.. _`versioncontrol.rst`: notes-es/versioncontrol.rst
.. _`editors.rst`: notes-es/editors.rst
.. _`makefiles.rst`: notes-es/makefiles.rst
.. _`punchcard.rst`: notes-es/punchcard.rst
.. _`reproducibility.rst`: notes-es/reproducibility.rst
.. _`shells.rst`: notes-es/shells.rst
.. _`sphinx.rst`: notes-es/sphinx.rst
.. _`ssh.rst`: notes-es/ssh.rst
.. _`timing.rst`: notes-es/timing.rst
.. _`top.rst`: notes-es/top.rst
.. _`unix.rst`: notes-es/unix.rst
.. _`vm.rst`: notes-es/vm.rst
.. _`fortran.rst`: notes-es/fortran.rst
.. _`project.rst`: notes-es/project.rst
.. _`homework1.rst`: notes-es/homework1.rst
.. _`homework2.rst`: notes-es/homework2.rst
.. _`homework3.rst`: notes-es/homework3.rst
.. _`homework4.rst`: notes-es/homework4.rst
.. _`homeworks.rst`: notes-es/homeworks.rst
.. _`linalg.rst`: notes-es/linalg.rst
.. _`mpi.rst`: notes-es/mpi.rst
.. _`openmp.rst`: notes-es/openmp.rst
.. _`poisson.rst`: notes-es/poisson.rst
.. _`numerical_python.rst`: notes-es/numerical_python.rst
.. _`python_debugging.rst`: notes-es/python_debugging.rst
.. _`python_functions.rst`: notes-es/python_functions.rst
.. _`python_plotting.rst`: notes-es/python_plotting.rst
.. _`python_strings.rst`: notes-es/python_strings.rst
.. _`git_more.rst`: notes-es/git_more.rst
.. _`computer_arch.rst`: notes-es/computer_arch.rst
.. _`computing_options.rst`: notes-es/computing_options.rst
.. _`special_functions.rst`: notes-es/special_functions.rst
.. _`fortran_arrays.rst`: notes-es/fortran_arrays.rst
.. _`fortran_debugging.rst`: notes-es/fortran_debugging.rst
.. _`fortran_io.rst`: notes-es/fortran_io.rst
.. _`fortran_modules.rst`: notes-es/fortran_modules.rst
.. _`fortran_newton.rst`: notes-es/fortran_newton.rst
.. _`fortran_sub.rst`: notes-es/fortran_sub.rst
.. _`fortran_taylor.rst`: notes-es/fortran_taylor.rst
.. _`gfortran_flags.rst`: notes-es/gfortran_flags.rst
.. _`project_hints.rst`: notes-es/project_hints.rst
.. _`homework4_solution.rst`: notes-es/homework4_solution.rst
.. _`lapack_examples.rst`: notes-es/lapack_examples.rst
.. _`jacobi1d_mpi.rst`: notes-es/jacobi1d_mpi.rst
.. _`jacobi1d_omp1.rst`: notes-es/jacobi1d_omp1.rst
.. _`jacobi1d_omp2.rst`: notes-es/jacobi1d_omp2.rst
.. _`ipython_notebook.rst`: notes-es/ipython_notebook.rst
.. _`python_and_fortran.rst`: notes-es/python_and_fortran.rst
.. _`python_scripts_modules.rst`: notes-es/python_scripts_modules.rst
