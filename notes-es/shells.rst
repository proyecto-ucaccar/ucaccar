
.. _shells:

=======
Shells
=======

.. A shell is a program that allows you to interact with the computer's
   operating system or some software package by typing in commands.  The shell
   interprets (parses) the commands and typically immediately performs some
   action as a result.  Sometimes a shell is called a *command line interface*
   (CLI), as opposed to a *graphical user interface* (GUI), which generally is
   more point-and-click.

Una shell es un programa que te permite interactuar con el sistema operativo del ordenador o algún paquete
de software escribiendo comandos. El intérprete de la shell (parses) interpreta el comando y típicamente
realiza alguna acción como resultado. Algunas veces a la shell se la llama *interfaz de línea de comado*
(CLI, del ingés *comand line interface*), como opuesto a la *interfaz gráfica de usuario* (GUI, del inglés
*graphical user interface*), que en general es más de señalar y clicar.

.. On a Windows system, most people use the point-and-click approach,
   though it is also possible to open a window in command-line mode
   for its DOS operating system. Note that DOS is different from Unix, and 
   we will *not* be using DOS.  Using :ref:`cygwin` is one way to get a
   unix-like environment on Windows, but if have a Windows PC, we
   recommend that you use one of the other options listed in
   :ref:`software_installation`.

En un sistem Windows, la mayoría de la gente usa la aproximación de señalar y clicar, aunque también es
posible abrir una ventana en un modo línea de comando para su sistema operativo DOS. Nota que DOS es
diferente de Unix, y *no* usaremos DOS. Usar :ref:`cygwin` es una manera de tener un entorno como unix
ejecutandose en Windows, pero si tienes un PC Windows, recomendamos que uses alguna otra opción listada en
:ref:`software_installation`.

.. On a Unix or Linux computer, people normally use a shell in a "terminal
   window" to interact with the computer, although most flavors of Linux also
   have point-and-click interfaces depending on what "Window manager" is being
   used.  

En un ordenador Unix o Linux, la gente normalmente usan una shell en una "terminal" para interactuar con
su ordenador, aunque la mayoría de las versiones de Linux también tiene interfaces de señalar y clicar
dependiendo de qué "administrador de ventana" que se esté usando.

.. On a Mac there is also the option of using a Unix shell in a terminal window
   (go to Applications --> Untilities --> Terminal to open a terminal).
   The Mac OS X operating system (also known as Leopard, Lion, 
   etc. depending on version) is essentially a flavor of Unix.

En un Mac está tambien la opción de usar una shell de Unix en una terminal (ve a Aplicaciones --> Utilidades
--> Terminal para abrir una terminal). El sistema operativo Mac OS X (también conocido como Leopard, Lion,
etc. dependiendo de su versión) es esencialmente una versión de Unix.

..  Unix shells
   -----------

Shells de Unix
---------------

.. See also the Software Carpentry lectures on `The Shell
   <http://software-carpentry.org/4_0/shell/index.html>`_.

Ve también las clases de Carpintería de Software en `La Shell
<http://software-carpentry.org/4_0/shell/index.html>`_.

.. When a terminal opens, it shows a *prompt* to indicate that it is waiting
   for input. In these notes a single $ will generally be used to indicate a
   Unix prompt, though your system might give something different.  Often the
   name of the computer appears in the prompt.   (See :ref:`prompt` for
   information on how you can change the Unix prompt to your liking.)
 
Cuando se abre una terminal, muestra un prompt para indicar que está esperando una entrada. En estas notas
un solo $ se usará generalmente para indicar un prompt de Unix, aunque tu sistema puede tener algo diferente. A veces el nombre de tu ordenador aparece en el prompt. (Mira :ref:`prompt` para obtener información sobre 
cómo cambiar el prompt de Unix como te guste).

.. Type a command at the prompt and hit return, and in general you should get
   some response followed by a new prompt.  For example::

Escribe un comando en el prompt y pulsa enter, y en general deberías obtener una respuesta seguida de un
nuevo prompt. Por ejemplo::

    $ pwd
    /Users/rjl/
    $

.. In Unix the *pwd* command means "print working directory", and the result is
   the full path to the directory you are currently working in.  (Directories
   are called "folders" on windows systems.)  The output above shows that on my
   computer at the top level there is a directory named */Users* that has a
   subdirectory for each distinct user of the computer.  The directory
   */Users/rjl* is where Randy LeVeque's files are stored, and within this we
   are several levels down.  

En Unix el comando *pwd* significa *escribir directorio de trabajo*, y el resultado es la ruta completa al
directorio en el que estás actualmente trabajando. (Los directorios se llaman "carpetas" en los sistemas windows). La salida anterior muestra que en mi ordenador en el nivel superior hay un directorio llamado
*/Users* que tiene un subdirectorio para cada usuario distinto en el ordenador. El directorio */Users/rjl*
es donde los archivos de Randy LeVeque se almacenan, y dentro tenemos varios subniveles más.


.. To see what files are in the current working directory, the *ls* (list)
   command can be used::

Para ver que archivos hay en el directorio actual de trabajo puede usarse el comando *ls* (listar)::

    $ ls


.. For more about Unix commands, see the section :ref:`unix`.

Para más comandos de Unix, ver la sección :ref:`unix`.

.. There are actually several different shells that have been developed for
   Unix, which have somewhat different command names and capabilities.  Basic
   commands like *pwd* and *ls* (and many others) are the same for any Unix
   shell, but they more complicated things may differ.

Hay varias shells diferentes que han sido desarrolladas para Unix, que tienen algunos nombres de comandos
y prestaciones diferentes. Los comandos básicos como *pwd* y *ls* (y muchos otros) son los mismos para
cualquier shell de Unix, pero las cosas más complicadas pueden ser diferentes.

.. In this class, we will assume you are using the bash shell (see :ref:`bash`).
   See :ref:`unix` for more Unix commands.

En esta clase, asumiremos que estamos usando la shell de bash (ver :ref:`bash`). Mira :ref:`unix` para más comandos de Unix.

------------
Matlab shell
------------

.. If you have used Matlab before, you are familiar with the Matlab shell,
   which uses the prompt >>.  If you use the GUI version of Matlab then this
   shell is running in the "Command window".  You can also run Matlab from the
   command line in Unix, resulting in the Matlab prompt simply showing up in
   your terminal window.  To start it this way, use the *-nojvm* option::

Si has usado Matlab antes, estás familiarizado con la shell de Matlab, que usa el prompt >>. Si usas la
versión GUI de Matlab entonces esta shell se ejecuta en la "Ventana de Comandos". Puedes usar Matlab desde
la línea de comando de Unix, lo que produce simplemente que el prompt de Matlab aparezca en tu terminal.
Para usarlo de esta manera, usa la opción *-nojvm*::

    $ matlab -nojvm
    >> 


------------
Python shell
------------

.. We will use Python extensively in this class.  For more information see the
   section :ref:`python`.

Usaremos Python de manera extensiva en esta clase. Para más información ver la sección :ref:`python`.

.. Most Unix (Linux, OSX) computers have Python available by default, invoked by::

La mayoría de los ordenadores Unix (Linux, OSX) tienen Python disponible por defecto, invocado por::

    $ python
    Python 2.7.3 (default, Aug 28 2012, 13:37:53) 
    [GCC 4.2.1 Compatible Apple Clang 4.0 ((tags/Apple/clang-421.0.60))] on darwin
    Type "help", "copyright", "credits" or "license" for more information.
    >>> 

.. This prints out some information about the version of Python and then gives
   the standard Python prompt, >>>.  At this point you are in the Python shell
   and any commands you type will be interpreted by this shell rather than the
   Unix shell. You can now type Python commands, e.g.::

Esto imprime alguna información sobre la versión de Python y entonces nos da el prompt estandar de Python.
En este punto estás en la shell de Python y cualquier comando que escribas será interpretado por esta shell
en lugar de la shell de Unix. Ahora puedes escribir comandos de Python, por ejemplo::

    >>> x = 3+4
    >>> x
    7
    >>> x+2
    9
    >>> 4/3
    1

.. The last line might be cause for concern, since 4/3 is not 1.  For more
   about this, see :ref:`numerical_python`.  The problem is that since 4 and 3 are
   both integers, Python gives an integer result.  To get a better result,
   express 4 and 3 as real numbers (called *float*s in Python) by adding
   decimal points::

La última línea puede causar preocupación, ya que 4/3 no es 1. Para más información sobre esto, ver
:ref:`numerical_python`. El problema es que como 4 y 3 son enteros, Python da un resultado entero. Para
obtener un mejor resultado expresa 4 y 3 como número reales (llamados *float* en Python) añadiendo
un punto decimal::

    >>> 4./3.
    1.3333333333333333

.. The standard Python shell is very basic; you can type in Python commands and
   it will interpret them, but it doesn't do much else.

La shell estandar de Python es muy básica; puedes escribir comandos de Python y los interpretará pero no
hará mucho más.

.. _ipython_shell:

IPython shell
-------------

.. A much better shell for Python is the *IPython shell*, which has
   extensive documentation at [IPython-documentation]_.

Una shell mucho mejor de Python es *Ipython shell*, que tiene una documentación más extensiva en
[IPython-documentation]_. 

.. Note that IPython has a different sort of prompt::

Nota que IPython tiene un prompt diferente::

    $ ipython

    Python 2.7.2 (default, Jun 20 2012, 16:23:33) 
    Type "copyright", "credits" or "license" for more information.

    IPython 0.14.dev -- An enhanced Interactive Python.
    ?         -> Introduction and overview of IPython's features.
    %quickref -> Quick reference.
    help      -> Python's own help system.
    object?   -> Details about 'object', use 'object??' for extra details.

    In [1]: x = 4./3.

    In [2]: x
    Out[2]: 1.3333333333333333

    In [3]: 

.. The prompt has the form *In [n]* and any output is preceeded by 
   by *Out [n]*.  IPython stores all the inputs and outputs in an array of
   strings, allowing you to later reuse expressions.  

El prompt tiene la forma *In [n]* y cualquier output está precedido por *Out [n]*. IPython almacena
en un array todas las entradas y salidas en un vector de cadenas, permitiendo reusard luego las
expresiones.

.. For more about some handy features of this shell, see :ref:`ipython`.

Para más información sobre características útiles sobre esta shell consulta :ref:`ipython`.

.. The IPython shell also is programmed to recognize many commands that are not
   Python commands, making it easier to do many things.  For example, IPython
   recognizes *pwd*, *ls* and various other Unix commands, e.g. to print out
   the working directory you are in while in IPython, just do::

La shell de IPython también está programada para reconocer muchos comandos que no son comandos de Python,
haciendo que más fácil realizar muchas cosas. Por ejemplo, Ipython reconoce *pwd*, *ls* y otros comandos de
Unix, por ejemplo, para imprimir el directorio de trabajo en el que te hallas en IPython, solamente haz::

    In [3]: pwd
    
.. Note that IPython is not installed by default on most computers, you will
   have to download it and install yourself (see [IPython-documentation]_).  It
   is installed on the :ref:`vm`.

Nota que IPython no está instalado por defecto en la mayoría de los ordenadores, tendrás que descargarlo
e instalarlo tu mismo (ver [IPython-documentation]_). Está instalado en la :ref:`vm`.

.. If you get hooked on the IPython shell, you can even use it as a Unix shell,
   see `documentation <http://ipython.scipy.org/doc/rel-0.10/html/interactive/shell.html>`_.

Si te enganchas a la shell de IPython, puedes incluso usarla como shell de Unix, ver
`documentacion <http://ipython.scipy.org/doc/rel-0.10/html/interactive/shell.html>`_.

.. Further reading
   ---------------

Más lecturas
-------------

Ver [IPython-documentation]_

