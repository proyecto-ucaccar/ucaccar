

.. _timing:

.. =============================================================
   Timing code
   =============================================================

=========================
Cronometrando el código
=========================

.. _timing_unix:

.. Unix time command
   -----------------

Comando time de Unix
---------------------

.. There is a built in command `time` that can be used to time other commands.
   Just type `time command` at the prompt, e.g.::
Hay un comando `time` que puede ser usado para cronometrar otros comandos. Sólo escribe `time comando` en el
prompt, por ejemplo::

    $ time ./a.out
    <output from code>

    real    0m5.279s
    user    0m1.915s
    sys     0m0.006s

.. This executes the command `./a.out` in this case (running a Fortran
   executable) and then prints information
   about the time required to execute, where, roughly speaking,
   *real* is the wall-clock time, *user* is the time spent executing the
   user's program, and *sys* is the time spent on system tasks required by the
   program.

Esto ejecuta en este caso el comando `./a.out` (ejecutando un ejecutable de Fortran) y entonces imprime
información sobre el tiempo requerido para ejecutarlo, donde, toscamente hablando *real* es el tiempo que ha transcurrido, *user* es el tiempo gastado en ejecutar el programa del usuario y *sys* es el tiempo gastado
en las tareas del sistema requeridas por el programa.


.. There may be a big difference between the *real* time and the sum of the
   other two times if the computer is simulataneously executing many other
   tasks and your program is only getting some of its attention.

Puede haber una gran diferencia entre el tiempo *real* y la suma de los otros dos tiempos si la computadora
está ejecutando simultáneamente muchas tareas y tu programa esta solo tenido parte de su atención.

.. Using *time* does not allow you to examine how much CPU time different parts
   of the code required, for example.

Usar *time* no te permite, por ejemplo, examinar cuánto tiempo de la CPU requiere diferentes partes de tu código.

.. _timing_fortran:

.. Fortran timing utilities
   ------------------------

Herramientas para cronometrar en Fortran
-----------------------------------------

.. There are two Fortran intrinsic functions that are very useful.

Hay dos funciones intrínsecas en Fortran que son muy útiles.

.. `system_clock` tells the elapsed wall time between two successive calls, and
   might be used as follows::

`system_clock` nos dice el tiempo transcurrido entre dos llamadas sucesivas, y puede usarse de la siguiente
manera::

    integer(kind=8) :: tclock1, tclock2, clock_rate
    real(kind=8) :: elapsed_time

    call system_clock(tclock1)

    <code to be timed>

    call system_clock(tclock2, clock_rate)
    elapsed_time = float(tclock2 - tclock1) / float(clock_rate)


.. `cpu_time` tells the CPU time used between two successive calls::

`cpu_time` dice el tiempo de CPU usado entre dos llamadas sucesivas::

    real(kind=8) :: t1, t2, elapsed_cpu_time

    call cpu_time(t1) 

    <code to be timed>

    call cpu_time(t2) 
    elapsed_cpu_time = t2 - t1

    
.. Here is an example code that uses both, and tests matrix-matrix multiply.

Aquí hay un código de ejemplo que usa ambos y prueba la multiplicación entre matrices.

.. literalinclude:: ../codes/fortran/timings.f90
   :language: fortran
   :linenos:

.. Note that the matrix-matrix product is computed 20 times over to give a
   better estimate of the timings.  

Nota que el producto entre matrices se calcula 20 veces para dar una mejor estimación de los tiempos.

.. You might want to experiment with this code and various sizes of the
   matrices and optimization levels.  Here are a few sample results on a
   MacBook Pro. 

Quizás quieras experimentar con este código y varios tamaños de matrices y niveles de optimización. Aquí hay
unos pocos resultados de ejemplo en un MacBook Pro.

.. First, with no optimization and :math:`200\times 200` matrices::

Primero, sin optimización y matrices :math:`200\times 200` ::

    $ gfortran timings.f90 
    $ ./a.out
     Will multiply n by n matrices, input n: 
    200
    Performed   20 matrix multiplies: CPU time =   0.81523600 seconds
    Elapsed time =   5.94083357 seconds

.. Note that the elapsed time include the time required to type in the response
   to the prompt for `n`, as well as the time required to allocate and
   initialize the matrices, whereas the CPU time is just for the matrix
   multiplication loops.

Nota que el tiempo transcurrido incluye el tiempo requerido en escribir la reacción al prompt para `n`, así
como el tiempo requerido para asignar e inicializar las matrices, mientras que el tiempo de CPU es solo
para los bucles de la multiplicación de matrices.

.. Trying a larger :math:`400 \times 400` case gives::

Probando un caso mayor :math:`400 \times 400` obtenemos::

    $ ./a.out
     Will multiply n by n matrices, input n: 
    400
    Performed   20 matrix multiplies: CPU time =   7.33721500 seconds
    Elapsed time =   9.87114525 seconds

.. Since computing the product of :math:`n \times n` matrices takes
   :math:`O(n^3)` flops,
   we expect this to take about 8 times as much CPU time as the previous test.
   This is roughly what we observe.

Ya que calcular el producto de matrices :math:`n \times n` requiere :math:`O(n^3)` flops, esperamos que
que tarde 8 veces más el tiempo de CPU que el test anterior. Esto es lo que observamos aproximadamente.

.. Doubling the size again gives requires much more that 8 times as long
   however::

Doblar el tamaño de nuevo, sin embargo, requiere mucho más que 8 veces el tiempo anterior::

    $ ./a.out
    Will multiply n by n matrices, input n: 
    800
    Performed   20 matrix multiplies: CPU time =  90.49682200 seconds
    Elapsed time =  93.98917389 seconds

.. Note that 3 matrices that are :math:`400\times 400` require 3.84 MB of memory,
   whereas three :math:`800 \times 800` matrices require 15.6 MB.  Since the MacBook
   used for this experiment 
   has only 6 MB of L3 cache, the data no longer fit in cache.

Nota que 3 matrices que son :math:`400\times 400` requieren 3.84 MB de memoria, mientras que tres matrices
:math:`800 \times 800` requieren 15.6 MB. Como el MacBook usado en este experimento tiene sólo 6B de cache L3,
los datos ya no caben en la cache.

.. **Compiling with optimization**

**Compilando con optimización**

.. If we recompile with the -O3 optimization flag, the last two experiments
   give these results::

Si recompilamos con la bandera de optimización -O3, los últimos dos experimentos nos dan estos resultados::

    $ gfortran -O3 timings.f90 
    $ ./a.out
     Will multiply n by n matrices, input n: 
    400
    Performed   20 matrix multiplies: CPU time =   1.39002200 seconds
    Elapsed time =   3.58041191 seconds

.. and ::

y ::

    $ ./a.out
     Will multiply n by n matrices, input n: 
    800
    Performed   20 matrix multiplies: CPU time =  66.39167200 seconds
    Elapsed time =  68.29921722 seconds

.. Both have sped up relative to the un-optimized code, the first much more
   dramatically.  

Ambos se han acelerado de manera relativa al código sin optimizar, el primero de manera más dramática.

.. timing_openmp:

.. Timing OpenMP code
   ------------------

Cronometrando el código OpenMP
-------------------------------

.. The code in `$UWHPSC/codes/openmp/timings.f90` shows an analogous code for
   matrix multiplication using OpenMP. 

El código en `$UWHPSC/codes/openmp/timings.f90` muestra un código análogo para la multiplicación de matrices
usando OpenMP.

.. The code has been slightly modified so that `system_clock` is only timing
   the inner loops in order to illustrate that `cpu_time` now computes the sum
   of the CPU time of all threads.

El código ha sido ligeramente modificado de tal manera que `system_clock` esta solo cronometrando el bucle
interior para ilustrar que `cpu_time` ahora calcula la suma de los tiempos de CPU en todos los hilos.

.. Here's one sample result::

Aquí hay un resultado de ejemplo::

    $ gfortran -fopenmp -O3 timings.f90 
    $ ./a.out
     Using OpenMP, how many threads? 
    4
     Will multiply n by n matrices, input n: 
    400
    Performed   20 matrix multiplies: CPU time =   1.99064000 seconds
    Elapsed time =   0.58711302 seconds

.. Note that the CPU time reported is nearly 2 seconds but the elapsed time is
   only 0.58 seconds in this case when 4 threads are being used.

Nota que el tiempo de CPU es cercano a dos segundos mientras que el tiempo transcurrido es solo de 0.58
segundos cuando se usan 4 hilos.

.. The total CPU time is slightly more than the previous code that did not use
   OpenMP, but the wall time is considerably less.

El tiempo de CPU es ligeramente superior que en el ejemplo anterior que no usa OpenMP, pero el tiempo total
es considerablemente menor.

.. For :math:`800\times 800` matrices there is a similar speedup::

Para matrices :math:`800\times 800` hay una aceleración similar::

    $ ./a.out
     Using OpenMP, how many threads? 
    4
     Will multiply n by n matrices, input n: 
    800
    Performed   20 matrix multiplies: CPU time =  79.70573500 seconds
    Elapsed time =  21.37633133 seconds

.. Here is the code:

El código está aquí::

.. literalinclude:: ../codes/openmp/timings.f90
   :language: fortran
   :linenos:

