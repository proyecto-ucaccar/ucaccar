
.. _sphinx:

.. =============================================================
   Sphinx documentation 
   =============================================================

========================
Documentación de Sphinx
========================

.. Sphinx is a Python-based documentation system that allows writing
   documentation in a simple mark-up language called ReStructuredText, which
   can then be converted to html or to latex files (and then to pdf or
   postscript).  See [sphinx]_ for general information, 
   [sphinx-documentation]_ for a
   complete manual, and [sphinx-rst]_ or [rst-documentation]_
   for a primer on ReStructuredText.
   See also [sphinx-cheatsheet]_.

Sphinx es un sistema de documentación basado en Python que permite escribir en un lenguaje mark-up simple
llamado ReStructuredText, que puede ser convertido a archivos html o latex (y luego a pdf o postscript).
Ver [sphinx]_ para información general, [sphinx-documentation]_  para un manual completo, y [sphinx-rst]_ o
[rst-documentation]_ para una introducción a ReStructuredText. Consulta también [sphinx-cheatsheet]_.

.. Although originally designed for aiding in documentation of Python software,
   it is now being used for documentation of packages in other languages as
   well.  See [sphinx-examples]_ for a list of other projects that use Sphinx.

Aunque originalmente se desarrolló para ayudar a la documentación de software en Python, ahora está siendo
usado para paquetes de documentación en otros lenguajes también . Ver[sphinx-examples]_ para una lista de
otros proyectos que usan Sphinx.

.. It can also be used for things beyond software documentation.  In
   particular, it has been used to create the class note webpages that you are
   now reading.  It was chosen for two main reasons:

También puede ser usado para cosas más allá de la documentación. En particular, se ha usado para crear
las páginas web de las notas de clase que estás leyendo ahora. Fue elegido principalmente por dos razones:

.. #. It is a convenient way to create a set of hyper-linked web pages on a variety
      of topics without having to write raw html or worry much about formatting.

.. #. Writing good documentation is a crucial aspect of high performance
      scientific computing and students in this class should learn ways to
      simplify this task.  For this reason many homework assignments must be
      "submitted" in the form of Sphinx pages.

#. Es una manera conveniente  de crear un conjunto de páginas web hiper-enlazadas de una gran variedad
   de temas sin tener que escribir html puro o preocuparse mucho por el formato.

#. Escribir una buena documentación es un aspecto crucial de la computación científica de alto rendimiento
   y los estudiantes en esta clase deberían aprender maneras de simplificar esta tarea. Por esta razón, muchas
   tareas deben ser "entregadas" en forma de páginas en Sphinx.

.. The easiest way to learn how to create Sphinx pages is to read some of the
   documentation 
   and then examine Sphinx pages such as the one you are now reading to see how
   it is written.  You can do this with these class notes or you might look at
   one of the other [sphinx-examples]_ to see other styles.

La manera más fácil de crear páginas en Sphinx es leer alguna documentación y entonces examinar páginas en 
Sphinx como la que estás leyendo para ver como están escrita. Puedes hacer esto con estas notas de clase o
puedes mirar uno de los otros [sphinx-examples]_ para ver otros estilos.

.. Note that any time you are reading a page of these class notes (or other
   things created with Sphinx) there is generally a menu item *Show Source* (on
   this page it's on the left, under the heading *This Page*) and clicking on
   this link shows the raw text file as originally written.  *Try this now*.

Date cuenta que cuando estas leyendo una página de estas notas de clase (o cualquier otra cosa creada con
Sphinx) hay generalmente un item *Mostrar fuente* (en esta página está a la izquierda, bajo la cabecera
*Esta página*) y haciendo click  en este enlace puedes ver el archivo de texto como fue originalmente escrito. *Intenta esto ahora*.

.. It is possible to customize Sphinx so the pages look very different, as
   you'll see if you visit some other projects listed at [sphinx-examples]_.

Es posible personalizar Sphinx de forma que las páginas sean muy diferentes, como verás si visitas algún
otro proyecto listado en [sphinx-examples]_.

.. Using Sphinx for your own project
   ---------------------------------

Usando Sphinx para tu propio proyecto
--------------------------------------

.. If you want to create your own set of Sphinx pages for some project, it is
   easy to get started following the instructions at [sphinx]_, or for a quick
   start with a different look and feel, see [sphinx-sampledoc]_.

Si quieres crear tu propio conjunto de páginas de Sphinx para algún proyecto, es fácil comenzar siguiendo
las instrucciones en [sphinx]_, o para un comienzo rápido con una vista y un sentido diferente, mira
[sphinx-sampledoc]_.

.. Using Sphinx to create these webpages
   -------------------------------------

Usando Sphinx para crear estas páginas web
-------------------------------------------

.. If you clone the git repository for this class (see :ref:`git`), you will find
   a subdirectory called *notes*, containing a number of files with the
   extension *.rst*, one for each webpage, containing the ReStructuredText
   input.  

Si clonas el repositorio git para esta clase (ver :ref:`git`), encontraras un subdirectorio llamado *notes*,
que contiene un gran número de archivos con la extensión *.rst*, uno para cada página web, que tienen la
entrada en ReStructuredText.

.. To create the html pages, first make sure you have Sphinx installed via::

Para crear páginas html, primero asegurate que tienes Sphinx instalado via::

        $ which sphinx-build

.. (see :ref:`software_installation`) and then type::

(ver :ref:`software_installation`) y entonces escribe::

        $ make html

.. This should process all the files (see :ref:`makefiles`) and create a
   subdirectory of *notes* called *_build/html*.  The file
   *_build/html/index.html* contains the main Table of Contents page.
   Navigate your browser to this page using *Open File* on the *File* menu of
   your browser. 

Esto debería procesar todos los archivos (ver ref:`makefiles`) y crear un subdirectorio de *notes* llamado
*_build/html*. El archivo *_build/html/index.html* contiene la página principal del Índice. Navega con el
navegador a esta página usando *Abrir Archivo* en el menú *Archivo* de tu navegador.

.. Or you may be able to direct Firefox directly to this page via::

O quizás podrías dirigir Firefox directamente a esta página via::

        $ firefox _build/html/index.html

.. Making a pdf version
   --------------------

Haciendo una versión en pdf
----------------------------

.. If you type::

Si escribes::

        $ make latex

.. then Sphinx will create a subdirectory *_build/latex* containing latex
   files.  If you have latex installed, you can then do::

entonces Sphinx creará un subdirectorio *_build/latex* que contiene archivos en latex. Si tienes latex
instalado, entonces puedes hacer::

        $ cd _build/latex
        $ make all-pdf

.. to run latex and create a pdf version of all the class notes.

para ejecutar latex y crear una versión en pdf de todas las notas de clase.

.. Further reading
   ---------------

Más lecturas
------------

.. See [sphinx]_ for general information, [sphinx-documentation]_ for a
   complete manual, and [sphinx-rst]_ or [rst-documentation]_.
   See also [sphinx-cheatsheet]_.

Ver [sphinx]_ para información general, [sphinx-documentation]_ para un manual completo, y [sphinx-rst]_ o [rst-documentation]_. Ver también [sphinx-cheatsheet]_.

.. See [sphinx-sampledoc]_ to get started on your own project.

Ver [sphinx-sampledoc]_ para empezar con tu propio proyecto.
