

.. _ssh:

.. =============================================================
   Using ssh to connect to remote computers
   =============================================================

=============================================
Usando ssh para conectar ordenadores remotos
=============================================

.. Some computers allow you to remotely log and start a Unix shell running
   using ssh (secure shell).  To do so you generally type something like::

Algunos ordenadores te permiten conectarte e iniciar una shell de Unix usando ssh (shell segura). Para hacerlo
generalmente escribes algo como::

    $ ssh username@host

.. where username is your account name on the machine you are trying to connect
   to and host is the host name.

donde username es tu nombre de cuenta en la máquina donde te estás intentando conectar y hostname es el nombre
del anfitrión.

.. On Linux or a
   Mac, the `ssh` command should work fine in a terminal.  On Windows, you may
   need to install something like `putty <http://www.putty.org/>`_.  

En Linux o en un Mac, el comando `ssh` debería funcionar bien en una terminal. En Windows, puedes necesitar
instalar algo como `putty <http://www.putty.org/>`_.

.. _ssh_X:

.. X-window forwarding
   -------------------

Llamando a X-window
--------------------

.. If you plan on running a program remotely that might pop up its own
   X-window, e.g. when doing plotting in Python or Matlab, you should use::

Si planeas ejecutar remotamente un programa que puede lanzar su propia X-windows, por ejemplo, cuando haces 
una gráfica en Python o en Matlab, deberías usar::

    $ ssh -X username@host

.. In order for X-windows forwarding to work you must be running
   a X-window server on your machine.  If you are running on a linux machine
   this is generally not an issue.  On a Mac you need to install the *Xcode
   developer tools* (which you will need anyway).

Para que funcione la llamada a X-window debes estar ejecutando un servidor de X-window en tu máquina. Si estás en una máquina con 
linux, esto no es generalmente un problema. En Mac necesitas instalar *Xcode developer tools* (la cuál
necesitarás de todas formas).

.. On Windows you will need something like `xming
   <http://sourceforge.net/projects/xming/>`_.  A variety of tutorials on
   using *putty* and *xming* together can be found by googling "putty and
   xming".

En Windows necesitarás algo como `xming
   <http://sourceforge.net/projects/xming/>`_. Una amplia gama de tutoriales sobre *putty* y *xming* juntos
pueden encontrarse buscando en google "putty and xming".

scp
---------------

.. To transfer files you can use `scp`, similar to the copy command `cp` but used
   when the source and destination are on different computers::

Para transferir archivos puedes usar `scp`, de forma similar al comando copiar `cp` pero se usa
cuando la fuente y el destino están en ordenadores distinto::

    $ scp somefile username@host:somedirectory

.. which would copy `somefile` in your local directory to `somedirectory`
   on the remote `host`, which is an address like `homer.u.washington.edu`,
   for example.  

copiaría `somefile` de tu directorio local en `somedirectory` a `host` (remoto) que es una dirección
como `homer.u.washington.edu`, por ejemplo.

.. Going  in the other direction, you could copy a remote file to your local
   machine via::

Llengo en la otra dirección, podrias copiar un archivo remoto en tu máquina local via::

    $ scp username@host:somedirectory/somefile . 

.. The last "." means "this directory".  You could instead give the path to a
   different local directory.

El último "." significa "este directorio". Podrías dar una ruta en su lugar a un directorio local diferente.

.. You will have to type your password on the remote host each
   time you do this, unless you have remote ssh access set up, see for example
   `this page <http://www.debian.org/devel/passwordlessssh>`_.

Tendrás que introducir tu contraseña en un anfitrión remoto cada vez que hagas esto, a menos que tengas
configurado el acceso remoto de ssh, ver por ejemplo
`this page <http://www.debian.org/devel/passwordlessssh>`_.
