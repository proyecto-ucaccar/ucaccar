

.. _git_more:

.. =============================================================
   More about Git
   =============================================================

==============================
Más sobre Git
==============================

.. Using *git* to stay synced up on multiple computers
   ----------------------------------------------------

Usar *git* para mantener sincronizados varios ordenadores
----------------------------------------------------------

.. If you want to use your *git* repository on two or more computers, staying
   in sync via bitbucket should work well. To avoid having merge conflicts or
   missing something on one computer because you didn't push it from the other,
   here are some tips:

Si quieres usar tu repositorio *git* en dos o más ordenadores, estar sincronizado
via bitbucket debería funcionar bien. Para evitar conflictos o perdidas en un ordenador porque
no hicistes push en el otro, aquí hay algunos consejos.

   .. * When you finish working on one machine, make sure your directory is
      "clean" (using "git status") and if not, add and commit any changes.

* Cuando acabes de trabajar en una máquina, asegurate de que tu directorio está "limpio"
  (usando "git status") y si no, añade y registra los cambios.

   .. * Use "git push" to make sure all commits are pushed to bitbucket.

* Usa "git push" para asegurarte que todos los registros están en bitbucket.

   .. * When you start working on a different machine, make sure you are up to
      date by doing::

.. These can probably be replaced by simply doing::
.. but for more complicated merges it's recommended that you do the steps
      separately to have more control over what's being done, and perhaps to
      inspect what was fetched before merging.
.. If you do this in a clean directory that was pushed since you made any
      changes, then this merge should go fine without any conflicts.
      
* Cuando comiences a trabajes en una máquina diferente, asegurate que estás actualizado haciendo::

        $ git fetch origin          # fetch changes from bitbucket
        $ git merge origin/master   # merge into your working directory
      
  Esto probablemente puede ser remplazado por::

        $ git pull

  pero para fusiones más complicadas se recomienda que hagas los pasos de forma separada para
  tener más conrol sobre lo que se está haciendo, y quizás para inspeccionar que se encontró antes de
  unirlos.

  Si haces esto en un directorio limpio al que se le hizo push cuando hiciste algún cambio,
  entonces la fusión debería hacerse bien sin ningún conflicto.

