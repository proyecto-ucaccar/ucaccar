
.. _reproducibility:

.. =============================================================
   Reproducibility
   =============================================================

==========================
Reproducibilidad
==========================

.. Whenever you do a computation you should strive to make it reproducible, so
   that if you want to produce the same result again at a later time you will
   be able to.  This is particularly true if the result is important to
   your research and will ultimately be used in a paper or thesis 
   you are writing, for example.

Siempre que hagas un cálculo deberias esforzarte en hacerlo reproducible, así si quieres producir el mismo resultado luego podrás hacerlo. Esto es particularmente cierto, por ejemplo, si el resultado es
importante para tu investigación y será usado en un documento o tesis que estés escribiendo.

.. This may take a bit more time and effort initially, but doing so routinely
   can save you an enormous amount of time in the future if you need to
   slightly modify what you did, or if you need to reconstruct what you did 
   in order to convince yourself or others that you did it properly, or as the
   basis for future work in the same direction.

Esto puede tomar un poco más de esfuerzo y tiempo inicial, pero hacerlo de manera rutinaria puede ahorrarte
cantidades enormes de tiempo en el futuro si necesitas modificar ligeramente lo que hiciste, o si necesitas
reconstruir lo que hiciste para convencerte a ti mismo o a otros que lo hicistes correctamente, o como base
para un futuro trabajo en la misma dirección.

.. In addition to the potential benefits to your own productivity in the
   future, reproducibility of scientific results is a cornerstone of the
   scientific method --- it should be possible for other researchers to
   independently verify scientific claims by repeating experiments.  If it is
   impossible to explain exactly what you did, then it will be impossible for
   others to repeat your computational experiments.  Unfortunately most
   publications in computational science fall short when it comes to
   reproducibility.  Algorithms and data analysis techniques are usually
   inadequately described in publications, and readers would find it very hard
   to reproduce the results from scratch.  An obvious solution is to make the
   actual computer code available.  At the very least, the authors of the paper
   should maintain a complete copy of the code that was used to produce the
   results.  Unfortunately even this is often not the case.

Además de los beneficios potenciales a tu propia productividad en el futuro, la reproductividad de resultados
es una piedra angular del método científico -- debería ser posible para otros investigadores verificar de
manera independiente afirmaciones científicas repitiendo experimentos. Si es imposible explicar exactamente
lo que hicistes, será imposible para los demás repetir tus experimentos computacionales. Desafortunadamente
la mayoría de las publicaciones en ciencia de la computación se quedan cortas cuando se trata de
reproducibilidad. Los algoritmos y las técnicas de análisis de datos habitualmente son descritas de manera
inadecuada en las publicaciones, y los lectores encontrarán que es muy difícil reproducir los resultados del
borrador. Una solución obvia es hacer que código fuente actual esté disponible. Como poco, los autores del documento deberían mantener una copia completa del código que fue usado para producir los
resultados. Desafortunadamente no es el caso ni de esto último.

.. There is growing concern in many computational science communities with the
   lack of reproducibility, not only because of the negative effect on
   productivity and scientific progress, but also because the results of
   simulations are increasingly important in making policy decisions that can
   have far-reaching consequences.

Hay una creciente preocupación en muchas comunidades de computación científicas con la falta de
reproducibilidad, no solo por el efecto negativo de la productividad y del progreso científico, si también
porque los resultados de las simulaciones están incrementando su importancia en las políticas de decisión
que pueden tener consecuencias más allá de la investigación.

.. Various efforts underway to develop better tools
   for assisting in doing computational work in a reproducible manner and to
   help provide better access to code and data that is used to obtain
   research results.

Hay varios esfuerzos en marcha para desarrollar mejores herramientas para asistir con el trabajo computacional
de una manera reproductible y para ayudar a conseguir mejor acceso al código y los datos que se estén
usando para conseguir los resultados.

.. Those interested in learning more about this topic might start with the
   resources listed in :ref:`biblio_repro`.

Aquellos interesados en aprender más sobre este tema pueden empezar con los recursos listados
en :ref:`biblio_repro`.


.. In this class we will concentrate on version control as one easy way to
   greatly improve the reproducibility of your own work.  If you create a *git*
   repository for each project and are diligent about checking in versions of
   the code that create important research products, with suitable commit
   messages, then you will find it much easier in the long run to reconstruct
   the version used to produce any particular result.

En esta clase nos centraremos en el control de versiones como una manera fácil de mejorar en gran medida
la reproductibilidad de tu propio trabajo. Si creas un repositorio *git* por cada proyecto y eres diligente
a la hora de comprobar  las versiones de tu código que crean un producto de investigación importante, con
mensajes de registro apropiados, entonces encontrarás mucho más fácil a largo plazo reconstruir la versión
usada para producir un resultado particular.

.. Scripting vs. using a shell or GUI
  -----------------------------------

Usar scripts vs. usar una shell o GUI
--------------------------------------

.. Many graphics packages let you type commands in at a shell prompt and/or
   click buttons on a GUI to generate data or adjust plot parameters.  While
   very convenient, this makes it hard to reproduce the resulting plot unless
   you remember exactly what you did.  
   It is a very good idea to instead write a script that produces the plot and
   that sets all the appropriate plotting parameters in such a way that
   rerunning the script gives exactly the same plot.  With some plotting tools
   such a script can be automatically generated after you've come up with the
   optimal plot by fiddling around with the GUI or by typing commands at the
   prompt.  It's worth figuring out how to do this most easily for your own
   tools and work style.  If you always create a script for each figure, and
   then check that it works properly and commit it to the *git* repository
   for the project, then you will be able to easily reproduce the figure again
   later.

Muchos paquetes gráficos te permiten escribir comandos en el prompt de una shell y/o hacer click en botones
de una GUI para generar datos o ajustar parámetros para dibujar. Aunque muy conveniente, esto hace difícil
reproducir las gráficas resultantes a menos que recuerdes exáctamente lo que hicistes. Es una muy buena
idea que en su lugar escribas un script que produzca la gráfica y que establezcas los parámetros apropiados de
dibujo de tal manera que volviendo a ejecutar el scrit obtengamos el mismo dibujo. Con algunas herramientas
de dibujo tal script puede ser automáticamente generado después de que encuentres la gráfica
adecuada despueś de trastear con la GUI o escribiendo comando en el prompt. Es digno figurarse como hacer
eso más fácil a tus herramientas y estilo de trabajo. Si siempre creas un script para cada figura, compruebas que funciona correctamente y lo registras en el repositorio *git*  para el proyecto,
entonces podrás reproducir la figura de nuevo posteriormente con facilidad.

.. Even if you are not concerned with allowing others to create the same plot,
   it is in your own best interest to make it easy for you to do so again in
   the future.  For example, it often happens that the referees of a paper or
   members of a thesis committee will suggest improving a figure by plotting
   something differently, perhaps as simple as increasing the font size so that
   the labels on the axes can be read.  If you have the code that produced the
   plot this is easy to do in a few minutes.  If you don't, it may take days
   (or longer) to figure out again exactly how you produced that plot to begin
   with.

Incluso si no te preocupas en permitir que otros creen el mismo dibujo, es en tu propio interés hacer las
cosas fáciles para ti para repetirlas de nuevo en el futuro. Por ejemplo, a veces ocurre que el evaluador de un
documento o el miembro del comité de una tesis sugiere mejorar una figura dibujando algo de manera distinta,
quizás, tan simple como incrementar el tamaño de la fuente de manera que la etiqueta del eje pueda ser leida.
Si tienes el código que producce la gráfica es fácil hacerlo en pocos minutos. Si no, puedes tardar días (o
más) en descubrir cómo produciste exáctamente esa gráfica.

.. IPython history command
   -----------------------

comando History de IPython
---------------------------

.. If you are using IPython and typing in commands at the prompt,
   the *history* command can be used to print out a list of all the commands
   entered, or something like::

Si estás usando IPyton y escribiendo comandos en el prompt, el comando *history* puede ser usado para imprimir
la lista de todos los comandos introducidos, o algo como::

    In[32]:  history 19-31

.. to print out commands *In[19]* through *In[31]*. 

que imprime los comandos *In[19]* a *In[31]*.

