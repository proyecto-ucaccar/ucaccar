
.. _bitbucket:

.. =============================================================
.. Bitbucket repositories: viewing changesets, issue tracking
.. =============================================================

=======================================================================
Repositorios en Bitbucket: viendo los cambios, seguimiento de archivos
=======================================================================


.. The directions in Section :ref:`git` explain how to use the class Bitbucket
.. repository to download these class notes and other resources used in the
.. class, and also how to set up your own repository there.

Las direcciones en la Sección :ref:`git` explican como usar el repositorio de clase
de BitBucket para descargar estas notas y otros recursos utilizados en clase,
y también cómo establecer tu propio repositorio.

.. See also `Bitbucket 101
..  <https://confluence.atlassian.com/display/BITBUCKET/Bitbucket+101>`_.

Ver también `Bitbucket 101 <https://confluence.atlassian.com/display/BITBUCKET/Bitbucket+101>`_.

.. In addition to providing a hosting site for repositories to allow them to be
.. easily shared, Bitbucket provides other web-based resources for working with
.. a repository.  (So do other sites such as 
.. `github <http://github.com/repositories>`_, for
.. example.)

Además, para proporcionar un sitio de alojamiento para repositorios  que les permita ser
fácilmete compartidos, Bitbucket provee otros recursos en la red para trabajar con un repositorio.
(Así también lo hacen otros sitios tales como `github <http://github.com/repositories>`_, por ejemplo.)

.. To get a feel for what's possible, take a look at one of the major software
.. projects hosted on bitbucket, for example
.. `<http://bitbucket.org/birkenfeld/sphinx/>`_ which is the repository for the
.. Sphinx software used for these class notes pages (see :ref:`sphinx`).  You
.. will see that software is being actively developed.    

Para tener una idea de lo que es posible, echa un ojo a alguno de los grandes proyectos de software albergados
en bitbucket, por ejemplo a 
`<https://bitbucket.org/birkenfeld/sphinx/>`_ que es el repositorio para el software
Sphinx usado en estas notas de clase (ver :ref: `sphinx`). Verás que este software esta siendo activamente desarrollado.

.. If you click on the "Source" tab at the top of the page you can browse
.. through the source code repository in its current state.

Si haces click en la pestaña "Source" arriba de la página puedes navegar através
del repositorio de códigos en su estado actual.

.. If you click on the "Changesets" tab you can see all the changes ever
.. committed, with the message that was written following the -m flag when the
.. commit was made.  If you click on one of these messages, it will show all
.. the changes in the form of the lines of the files changed, highlighted in
.. green for things added or red for things deleted.

Si haces click en la pestaña "Changesets" podrás ver todos los cambios realizados, junto con
los mensajes que se escribieron tras la bandera -m cuando se realizaron. Si haces click en alguno de
estos mensajes, se mostrarán todos los cambios en las líneas del fichero cambiado, marcadas en verde si
se han añadido cosas o en rojo si se han borrado. 

.. If you click on the "Issues" tab, you will see the issue-tracking page.  If
.. someone notices a bug that should be fixed, or thinks of an improvement that
.. should be made, a new issue can be created (called a "ticket" in some systems).

Si haces click en la pestaña "Issues" verás las pagínas de rastreo de temas. Si alguien
avisa de un error que debería ser solucionado, o piensa en una mejora que se puede hacer,
puede crear un tema (llamado "tickets" en algunos sistemas).

.. If you want to try creating a ticket, **don't** do it on the Sphinx page,
.. the developers won't appreciate it.  Instead try doing it on your own
.. bitbucket repository that you set up following :ref:`myhg`.

Si quieres probar a crear un ticket, **no** lo hagas en la página de Sphinx,
ya que los desarrolladores no lo apreciarán. En su lugar prueba a hacerlo en tu propio
repositorio de bitbucking que creaste siguiendo :ref:`myhg`.

.. You might also want to look at the bitbucket page for this class repository,
.. at `<http://bitbucket.org/rjleveque/uwhpsc/>`_ to keep track
.. of changes made to notes or code available.

Porías querer mirar en la página de bitbucket el repositorio de este curso en 
`<http://bitbucket.org/rjleveque/uwhpsc/>`_ para seguir la pista a los cambios
realizados en las notas o en los códigos disponibles,
