

.. _python_functions:

.. =============================================================
.. Python functions
.. =============================================================

=============================================================
Python funciones
=============================================================

.. Functions are easily defined in Python using *def*, for example::
Las funciones en Python se definen facilmente usando *def*, por ejemplo:
    >>> def myfcn(x):
    ...     import numpy as np
    ...     y = np.cos(x) * np.exp(x)
    ...     return y
    ... 

    >>> myfcn(0.)
    1.0

    >>> myfcn(1.)
    1.4686939399158851

.. As elsewhere in Python, there is no begin-end notation except the
.. indentation.  If you are defining a function at the command line as above,
.. you need to input a blank line to indicate that you are done typing in the
.. function.  

Como en otros lenguajes, en Python, no hay notación para el inicio y final, sólo la sangría.
Si tu estás definiendo una función en la línea de comandos superior, tu necesitas una línea en blanco para indicar que has acabado la función.
(Vea el ejemplo anterior)

.. Defining functions in modules
.. -----------------------------

Definir funciones en módulos.
-----------------------------

Se definen las funciones en módulos.

.. Except for very simple functions, you do not want to type it in at the
.. command line in Python.  Normally you want to create a text file containing
.. your function and import the resulting module into your interactive session.

Excepto en algunas funciones muy simples, no se quiere escribir en la línea de comando de Python.
Normalmente se desea crear un archivo de texto que contenga su función e importar el módulo resultante en la sesión interactiva.

.. If you have a file named *myfile.py* for example that contains::
Si tiene un archivo llamado *myfile.py*, por ejemplo, que contenga:

    def myfcn(x):
        import numpy as np
        y = np.cos(x) * np.exp(x)
        return y

.. and this file is in your Python search path (see :ref:`python_path`), then
.. you can do::

y este archivo está en su ruta de búsqueda de Python (vea :ref:`python_path`), entonces

    >>> from myfile import myfcn
    >>> myfcn(0.)
    1.0
    >>> myfcn(1.)
    1.4686939399158851

.. In Python a function is an object that can be manipulated like any other
.. object. 

En Python una función es un objeto que puede ser manipulado como cualquier otro objeto.

.. _lambda_functions:

.. Lambda functions
.. ----------------

Función lambda 
----------------

.. Some functions can be easily defined in a single line of code, and it is
.. sometimes useful to be able to define a function "on the fly" using "lambda"
.. notation.  To define a function that returns 2*x for any input x, rather
.. than::

Algunas funciones pueden ser facilmente definidas en una línea de código, y esto es a veces útil para definir
una función "sobre la marcha" usando la notación "lambda".
Para definir una función que devuelva 2x para cualquier x, tenemos

    def f(x):
        return 2*x

.. we could also define *f* via::

También podríamos definir *f* como:

    f = lambda x: 2*x


.. You can also define functions of more than one variable, e.g.::
También se pueden definir funciones con más de una variable, por ejemplo:
    g = lambda x,y: 2*(x+y)


.. Further reading
.. ---------------

Otras lecturas:
---------------

