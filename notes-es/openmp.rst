
.. _openmp:

=============================================================
OpenMP
=============================================================



OpenMP es tratado en :ref:`slides` comenzando en la lección 13.

Códigos de ejemplo
------------------

Podemos encontrar códigos de ejemplos en el directorio `$UWHPSC/codes/openmp`.
En el archivo `README.txt` podemos encontrar las instrucciones sobre compilación y ejecución.


Aquí tenemos un código muy simple, que evalúa una función costosa en muchos puntos:

.. literalinclude:: ../codes/openmp/yeval.f90
   :language: fortran
   :linenos:

Téngase en cuenta lo siguiente:

* Las lines que empiezan por `!$` solo se ejecutan si el código se compila y ejecuta con la orden `-fopenmp`, en caso contrario funcionan como comentarios.

* `x` tiene que ser declarada como variable del tipo `private` en
  el bucle `omp p  arallel do`, para que cada hilo tenga su propia versión.
  En otro caso, otro hilo puede resetear `x` en
  el momento en el que se le asigna un valor y ese valor
  se usa para el conjunto `y(i)`.

* El iterador del bucle `i` es de tipo `private` por defecto,
  pero todas las demás variables son el tipo `shared`.

* Si intentas hacer funcionar el programa y obtienes "segmentation fault"
  es probable que el límite del tamaño de la pila sea muy pequeño.
  Puedes ver el límite con la siguiente orden::

    $ ulimit -s

  En linux puedes solucionarlo de la siguiente forma::

        $ ulimit -s unlimited

  Pero en Mac hay un límite fijo y lo mejor que puedes hacer es::

        $ ulimit -s hard



Si todavía obtienes el mismo error tendrás que reducir `n` para ese ejemplo.

Otras directrices
-----------------

Este ejemplo ilustra otras directrices más allá del "parallel do":

.. literalinclude:: ../codes/openmp/demo2.f90
   :language: fortran
   :linenos:

Notas:

* `!$omp sections` se usa para separar el trabajo entre hilos.

* Hay una barrera implícita después de `!$omp end sections`, así que la
  barrera explícita en este caso es opcional.

* La declaración del print sólo se ejecuta cuando está en `!$omp single`.
  La condición `nowait` indica que otro hilo puede continuar sin
  esperar a este print para ser ejecutado.

Paralelismo fine-grain vs. coarse-grain
---------------------------------------
Consideremos el problema de normalizar un vector dividiendo cada elemento
por la norma del vector, definido por :math:`\|x\|_1 = \sum_{i=1}^n |x_i|`.

Primero tenemos que dar una vuelta alrededor de todos los puntos para calcular
la norma. Después tenemos que dar otra vuelta alrededor de los puntos y
el conjunto :math:`y_i = x_i / \|x\|_1`. Hay que tener en cuenta que
no podemos combinar ambas vueltas en una.

Aquí hay un ejemplo de *paralelismo fine-grain*, donde usamos la directriz
 `omp parallel do` de OpenMP o la directriz `omp do` sin un
bloque `omp parallel`.


.. literalinclude:: ../codes/openmp/normalize1.f90
   :language: fortran
   :linenos:

Téngase en cuenta lo siguiente:

* Probamos inicializando :math:`x_i=i`, así que :math:`\|x\|_1 = n(n+1)/2`.

* El compilador decide como dividir el bucle entre los hilos.
  El bucle que comienza en la línea 38 debe ser dividido de forma distinta
  al bucle que comienza en la línea 45.

* A causa de esto, todos los hilos deben tener acceso a toda la memoria.

El siguiente es una version con *paralelismo coarse-grain*, donde decidimos como
separar el vector entre los hilos y posteriormente ejecutamos el mismo código
en cada hilo, pero cada hilo compilará su propia versión de `istart` y de `iend`
para su trozo del vector. Con este código tenemos garantía de que el hilo 0,
por ejemplo, siempre maneja `x(1)`, luego en principio los datos pueden ser
distribuidos. Cuando usamos OpenMP en un ordenador con memoria compartida
esto no importa, pero esta versión puede ser más facilmente generalizada a MPI.

.. literalinclude:: ../codes/openmp/normalize2.f90
   :language: fortran
   :linenos:

Nótese lo siguiente:

* `istart` y `iend`, los valores de comienzo y finalización de `i`
  tomados por cada hilo, se calculan explícitamente en terminos de cada
  número del hilo. Tenemos que ser cuidadosos para manejar el caso en el que
  el número de hilos no divide a `n`.

* Varias variables tienen que ser declaradas `private` en las líneas 37-38.

* `norm` tiene que ser inicializado por 0 antes del bloque `omp parallel`.
  En otro caso, algún hilo podría guardarlo como 0 después de que otro hilo
  lo haya actualizado por su `norm_thread`.

* La actualización de `norm` en la línea 60 debe ser un bloque `omp critical`,
así dos hilos no van a tratar de actualizarla simultáneamente (data race).

* Debe haber una `omp barrier` en la línea 65 entre actualizar `norm`
  por cada hilo y usar `norm` para calcular cada `y(i)`.
  Tenemos que asegurarnos de que cada hilo tiene actualizado `norm`
  o no tendrá el valor correcto cuando lo usemos.

Para una comparación de paralelismo fine-grain y
coarse-grain en una iteracción de Jacobi, véase

* :ref:`jacobi1d_omp1`
* :ref:`jacobi1d_omp2`

Lectura extra
-------------

* En la bibliografía :ref:`biblio_openmp`
