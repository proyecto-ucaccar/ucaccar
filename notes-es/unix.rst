

.. _unix:

.. =============================================================
   Unix, Linux, and OS X
   =============================================================

=============================================================
Unix, Linux,  OS X
=============================================================

.. A brief introduction to Unix shells appears in the section :ref:`shells`.
   Please read that first before continuing here.

Una breve introducción a las shells de Unix aparece en la sección :ref:`shells`. Por favor,
leela antes de continuar aquí.

.. There are many Unix commands and most of them have many optional arguments
   (generally specified by adding something like -x after the command name,
   where x is some letter).   Only a few important commands are reviewed here.
   See the references (e.g. [Wikipedia-unix-utilities]_)
   for links with many more details.

Hay muchos comandos en Unix y la mayoría de ellos tienen muchos argumentos opcionales (generalmente
especificados añadiendo algo como -x después del nombre del comando, donde x es una letra). Solo algunos
comandos importantes serán revisados aquí. Ver las referencias (por ejemplo, [Wikipedia-unix-utilities]_)
para links con más detalles.

pwd and cd
----------

.. The command name *pwd* stands for "print working directory" and tells you
   the full path to the directory you are currently working in, e.g.::

El nombre del comando *pwd* viene de "print working directory" (imprimir directorio de trabajo) y te dice la
ruta completa del directorio donde estás trabajando actualmente, por ejemplo::

     $ pwd
     /Users/rjl/uwhpsc


.. To change directories, use the *cd* command, either relative to the current
   directory, or as an absolute path (starting with a "/", like the output of
   the above pwd command).  To go up one level::

Para cambiar los directorios, usa el comando *cd*, que viene de "current directory" (directorio actual),
o como una ruta absoluta (comenzando con una "/", como la salida del comando pwd). Para subir un nivel::

    $ cd ..
    $ pwd
    /Users/rjl


ls
----

.. *ls* is used to list the contents of the current working directory.
   As with many commands, *ls* can take both *arguments* and *options*.  An
   option tells the shell more about what you want the command to do, and is
   preceded by a dash, e.g.::

*ls* se usa para listar los contenidos del derectorio actual de trabajo. Como muchos comandos,
*ls* puede tomar *argumentos* y *opciones*. Una opción le dice a la shell más sobre que es lo que
quieres que el comando haga, y va precedido de un guión, por ejemplo::

    $ ls -l

.. The *-l* option tells *ls* to give a long listing that contains additional
   information about each file, such as how large it is, who owns it, when it
   was last modified, etc.   The first 10 mysterious characters tell who has
   permission to read, write, or execute the file, see `[Wikipedia]
   <http://en.wikipedia.org/wiki/File_system_permissions>`_.

El comando *-ls* le dice a *ls* que de una lista larga que contenga información adicional sobre cada archivo,
como que tamaño tiene, quién es el dueño, cuando se modificó por última vez, etc... Los primeros 10
misteriosos caracteres nos dice quien tiene permiso para leer, escribir o ejecutar el archivo, ver 
`[Wikipedia]
   <http://en.wikipedia.org/wiki/File_system_permissions>`_.

.. Commands often also take *arguments*, just like a function takes an argument
   and the function value depends on the argument supplied.  In the case of
   *ls*, you can specify a list of files to apply *ls* to. For example, if we
   only want to list the information about a specific file::

Los comandos en ocasiones también toman *argumentos*, al igual que una función toma un argumento y el valor
de la funció depende del argumento dado. En el caso de *ls*, puedes especificar una lista de archivos para
aplicarles *ls*. Por ejemplo, si sólo queremos listar la información sobre un archivo específico::

    $ ls -l fname

.. You can also use the *wildcard* * character to match more than one file::

Puedes también usar el carácter *comodín* para relacionar más de un archivo::

    $ ls *.x

.. If you type

Si tecleas

        $ ls -F

.. then directories will show up with a trailing / and executable files with a
   trailing asterisk, useful in distinguishing these from ordinary files.

entonces los directorios se mostrarán con una / y los archivos ejecutables con un asterisco, útil para distinguirlos de archivos ordinarios.

.. When you type *ls* with no arguments it generally shows most files and
   subdirectories, but may not show them all.  By default it does not show
   files or directories that start with a period (dot).  These are "hidden"
   files such as *.bashrc* described in Section :ref:`bashrc`.  

Cuando escribes *ls* sin argumentos en general muestra la mayoría de los archivos y subdirectorios, 
pero puede que no muestre todos. Por defecto no muestra los archivos y directorios que comiencen con
un punto. Estos son archivos "ocultos" como *.bashrc* descrito en la sección 
:ref:`bashrc`.

.. To list these hidden files use::

Para listar estos archivos ocultos usa::

        $ ls -aF

.. Note that this will also list two directories *./* and *../*  These appear
   in every directory and always refer to the current directory and the parent
   directory up one level.  The latter is frequently used to move up one level
   in the directory structure via::

Nota que esto listará también dos directorios *./* y *../* Estos aparecen en cada directorio
y siempre se refieren al directorio actual y al directorio padre del nivel superior. El último
se usa frecuentemente para moverse un nivel hacia arriba en la estructura de directorios via::

        $ cd ..


.. For more about *ls*, try::

Para más infomación sobre *ls*, prueba::

    $ man ls

.. Note that this invokes the *man* command (manual pages) with the argument
   *ls*, and causes Unix to print out some user manual information about *ls*.

Nota, que esto invoca el comando *man* (páginas del manual) con el argumento *ls*, y provoca que
Unix muestre información del manual sobre *ls*.

.. If you try this, you will probably get one page of information with a ':' at
   the bottom.  At this point you are in a different shell, one designed to
   let you scroll or search through a long file within a terminal.  The ':' is
   the prompt for this shell.  The commands you can type at this point are
   different than those in the Unix shell.  The most useful are::

Si haces esto, probablemente obtengas una página de información con ':' en la parte inferior. En este punto
estas en una shell diferente, una diseñada para permitirte desplazarte o buscar a través de un gran archivo con una terminal. Los ':' son el prompt de esta shell. Los comandos que puedes escribir en este punto son diferentes de los de la shell de Unix. Los más útiles son::

   : q [para salir de esta shell y volver a Unix]
   : <SPACE> [presiona la Barra Espaciadora para mostrar
              la siguiente pantalla completa]
   : b [vuelve a la pantalla completa anterior]

.. : q  [to quit out of this shell and return to Unix]
   : <SPACE>   [tap the Spacebar to display the next screenfull]
   : b  [go back to the previous screenfull]

more, less, cat, head, tail
---------------------------

.. The same technique to paging through a long file can be applied to your own
   files using the *less* command (an improvement over the original *more*
   command of Unix), e.g.::

Las mismas técnicas para avanzar páginas a través de un archivo largo pueden ser aplicadas a tus propios archivos usando el comando *less* (una mejora del comando original de Unix *more*).

    $ less filename

.. will display the first screenfull of the file and give the : prompt.

mostrará la primera pantalla completa del archivo y dará el prompt :.

.. The *cat* command prints the entire file rather than a page at a time.
   *cat* stands for "catenate" and *cat* can also be used to combine multiple
   files into a single file::

El comando *cat* imprime el archivo completo en lugar de una página cada vez. *cat* proviene de "catenate" (concatenar) y *cat* también puede ser usado para combinar multiples archivos en uno solo.

    $ cat file1 file2 file3 > bigfile

.. The contents of all three files, in the order given, will now be in
   *bigfile*.  If you leave off the "> bigfile" then the results go to the
   screen instead of to a new file, so "cat file1" just prints file1 on the
   screen.  If you leave off file names before ">" it takes input from the
   screen until you type <ctrl>-d, as used in the example at :ref:`myhg`.

El contenido de estos tres archivos en el orden dado, estará ahora en *bigfile*. Si no escribes "> bigfile"
entonces los resultados irán a la pantalla en lugar de a un nuevo archivo, así "cat file1" solo imprime file1
por pantalla. Si no escribes ningún nombre de archivo antes de ">" toma la entrada de la pantalla hasta que
escribas <ctrl>-d, como se usó en el ejemplo de :ref:`myhg`.

.. Sometimes you want to just see the first 10 lines or the last 5 lines of a
   file, for example.  Try::

A veces, solo quieres ver las primeras 10 líneas o las últimas 5 líneas del mismo, por ejemplo. Prueba::

    $ head -10 filename
    $ tail -5 filename

.. removing, moving, copying files
   -------------------------------

eliminando, moviendo, copiando archivos
----------------------------------------

.. If you want to get rid of a file named *filename*, use *rm*::

Si quieres deshacerte de un archivo llamado *filename*, usa *rm*::

    $ rm -i filename
    remove filename?

.. The -i flags forces *rm* to ask before deleting, a good precaution.  Many
   systems are set up so this is the default, possibly by including the
   following line in the :ref:`bashrc`::

La bandera -i fuerza a *rm* a preguntar antes de eliminar, una buena precaución. Muchos sistemas están
configurados para hacer esto por defecto, posiblemente incluyendo la siguiente línea en :ref:`bashrc`::

    alias rm='rm -i'

.. If you want to force removal without asking (useful if you're removing a
   bunch of files at once and you're sure about what you're doing), use the -f
   flag.

Si quieres forzar a remover sin preguntar (útil si estás eliminando un grupo de archivos de una vez y
si estás seguro de lo que estás haciendo), usa la bandera -f.

.. To rename a file or move to a different place (e.g. a different directory)::

Para renombrar un archivo o moverlo a un lugar diferente (por ejemplo, un directorio diferente)::

    $ mv oldfile newfile

.. each can be a full or relative path to a location outside the current
   working directory.

puede ser una ruta completa o relativa a una localización fuera del directorio actual de trabajo.

.. To copy a file use *cp*::

Para copiar un archivo usa *cp*::

    $ cp oldfile newfile

.. The original *oldfile* still exists.
   To copy an entire directory structure recursively (copying all files in it
   and any subdirectories the same way), use "cp -r"::

El original *oldfile* todavía existe. Para copiar un estructura entera recursivamente (copiando todos los
archivos y cualquier directorio de la misma manera), usa "cp -r"::

    $ cp -r olddir newdir

.. background and foreground jobs
   ------------------------------

trabajos en primer y segundo plano
-----------------------------------

.. When you run a program that will take a long time to execute, you might want
   to run it in *background* so that you can continue to use the Unix command
   line to do other things while it runs.  For example, suppose
   *fortrancode.exe* is a Fortran executable in your current directory 
   that is going to run for a long time.  You can do::

Cuando ejecutas un programa que tardará mucho en ejecutarse, quizás quieras ejecutarlo en *segundo plano*
y así puedes seguir usando la línea de comando de Unix para hacer otras cosas mientras se ejecuta. Por
ejemplo, supongamos que *fortrancode.exe* es un ejectutable de Fortran en tu directorio actual que va a
ejecutarse durante mucho tiempo. Puedes hacer::

    $ ./fortrancode.exe &
    [1] 15442

.. if you now hit return you should get the Unix prompt back and can continue
   working.
   
si ahora presionas return deberías poder volver al prompt de Unix y poder continuar trabajando.
 
.. The ./ before the command in the example above is 
   to tell Unix to run the executable in this
   directory (see :ref:`paths`), and the & at the end of the line tells it to
   run in background.  The "[1] 15442" means that it is background job number 1
   run from this shell and that it has the *processor id* 15442.

./ antes del comando en el ejemplo superior es para decirle a Unix que ejecute el ejecutable en este
directorio (ver :ref:`paths`), y el & al final de la línea le dice que lo haga en segundo plano. El
"[1] 15442" significa que es el trabajo en segundo plano número 1 ejectutado por esta shell y que tiene
el *processor id* 15442.

.. If you want to find out what jobs you have running in background and their
   pid's, try::

Si quieres encontrar que trabajos estás ejecutando en segundo plano y sus pid's, prueba::..

     $ jobs -l
     [1]+ 15443 Running                 ./fortrancode.exe &

.. You can bring the job back to the foreground with::

Puedes traer un trabajo de vuelta al primer plano con::

    $ fg %1

.. Now you won't get a Unix prompt back until the job finishes (or you put it
   back into background as described below). The %1 refers to job 1.  In this
   example *fg* alone would suffice since there's only one job running, but
   more generally you may have several in background.

Ahora no obtendrás el prompt de Unix hasta que el trabajo (o hasta que lo mandes de vuelta al
segundo plano como se describe abajo). El %1 se refiere al trabajo 1. En este ejemplo *fg* solo
hubiera bastado ya que sólo hay un trabajo ejecutándose, pero en general tendrás varios en segundo plano.

.. To put a job that is foreground into background, you can often type
   <ctrl>-z, which will pause the job and give you the prompt back::

Para poner un trabajo que está en primer plano en segundo plano, puedes escribir <ctrl>-z, que pausará
el trabajo y te devolverá el prompt::

    ^Z
    [1]+  Stopped                 ./fortrancode.exe
    $ 

.. Note that the job is not running in background now, it is stopped.  To get
   it running again in background, type::

Nota que el trabajo no está corriendo en segundo plano ahora, se ha parado. Para hacer que se ejecute de nuevo en segundo plano, escribe::

    $ bg %1

.. Or you could get it running in foreground with "fg %1".

O podrías mantenerlo ejecutándose en primer plano con "fg %1".

.. nice and top
   ------------

nice y top
------------

.. If you are running a code that will run for a long time you might want to
   make sure it doesn't slow down other things you are doing.  You can do this
   with the *nice* command, e.g.::

Si estás ejecutando un código que se ejecutará durante mucho tiempo quizás quieras asegurarte que no
enlentece otras cosas que estés haciendo. Puedes hacer esto con el comando *nice*, por ejemplo::

    $ nice -n 19 ./fortrancode.exe &

.. gives the job lowest priority (nice values between 1 and 19 can be used) so
   it won't hog the CPU if you're also trying to edit a file at the same time,
   for example.

le da al trabajo la menor prioridad (en nice puedes usar valores entre 1 y 19) así no acapara la CPU
si estás intentando editar un archivo al mismo tiempo, por ejemplo.

.. You can change the priority of a job running in background with *renice*,
   e.g.::

You can change la prioridad de un trabajo ejecutándose en segundo plano con *renice*, por ejemplo::

    $ renice -n 19 15443

.. where the last number is the process id.

donde el último número es la identidad del proceso.

.. Another useful command is *top*.  This will fill your window with a page of
   information about the jobs running on your computer that are using the most
   resources currently.  See :ref:`topcommand` for some examples.

Otro comando útil es *top*. Esto llenará tu ventana con una página de información sobre los trabajos que
se están ejecutando en tu ordenador que usan la mayor parte de los recursos. Ver :ref:`topcommand` para
algunos ejemplos.

.. _kill:

.. killing jobs
   ------------

matando trabajos
-----------------

.. Sometimes you need to kill a job that's running, perhaps because you realize
   it's going to run for too long, or you gave it or the wrong input data.  Or
   you may be running a program like the IPython shell and it freezes up on you
   with no way to get control back.  (This sometimes happens when plotting when
   you give the *pylab.show()* command, for example.)

A veces necesitas matar un trabajo que se está ejecutando, quizás porque te has dado cuenta de que se
va a estar ejecutando durante mucho tiempo, o porque le diste unos datos erróneos de entrada. O porque estás
ejecutando un programa como la shell de IPython, se congela y no hay manera de volver a coger el control. (Esto a veces ocurre cuando dibujas con el comando *pylab.show()*, por ejemplo).

.. Many programs can be killed with <ctrl>-c.  For this to work the job must be
   running in the foreground, so you might need to first give the *fg* command.

Se puede acabar con muchos programas con <ctrl>-c. Para que esto funcione, el trabajo debe estar ejecutándose
en primer plano, así que quizás necesites usar el comando *fg*.

.. Sometimes this doesn't work, like when IPython freezes.  Then try stopping
   it with <ctrl>-z (which should work), find out its PID, and use the *kill*
   command::

A veces, esto no funciona, como cuando se congela IPython. Entonces intenta pararlo con <ctrl>-z
(que debería funcionar), descubre su PID, y usa el comando *kill*.

    $ jobs -l
    [1]+ 15841 Suspended               ipython

    $ kill 15841

.. Hit return again you with luck you will see::

Pulsa enter de nuevo y con suerte verás::

    $
    [1]+ Terminated              ipython
    $ 

.. If not, more drastic action is needed with the -9 flag::

Si no, es necesario una acción más drástica con la bandera -9::

    $ kill -9 15841

.. This almost always kills a process.  Be careful what you kill.

   Esto casi siempre acaba con el proceso. Ten cuidado con lo que acabas.

.. _sudo:

sudo
----

.. A command like::

Un comando como::

    $ sudo rm 70-persistent-net.rules

.. found in the section :ref:`vm` means to do the remove command as super user.
   You will be prompted for your password at this point.

encontrad en la sección :ref:`vm` significa usar el comando borrar como super usuario. Se te
pedirá tu contraseña en este punto.

.. You cannot do this unless you are registered on a list of super users. You
   can do this on the VM because the *amath583* account has sudo privileges. The
   reason this is needed is that the file being removed here is a system file
   that ordinary users are not allowed to modify or delete.

No puedes hacer esto a menos que estés registrado en una lista de super usuario. Puedes hacer esto en
la VM porque la cuenta *amath583* tiene privilegios sudo. La razón por la que hace falta esto es que el archivo que va a ser eliminado aquí es un archivo de sistema que a los usuarios ordinarios no se les permite modificar o borrar.

.. Another example is seen at :ref:`apt-get`, where only those with super user
   permission can install software on to the system.

Otro ejemplo puede verse en :ref:`apt-get`, donde solo aquellos con permisos de super usuario pueden instalar
software en el sistema.

.. _bash:

.. The bash shell
   --------------

La shell bash
--------------

.. There are several popular shells for Unix.  The examples given in these
   notes assume the bash shell is used.  If you think your shell is different,
   you can probably just type::

Hay muchas shells populares para Unix. Los ejemplos dados en estas notas asumen que se está usando la shell
bash. Si crees que tu shell es diferente, simplemente puedes teclear::

    $ bash

.. which will start a new bash shell and give you the bash prompt.

lo que iniciará una nueva shell bash y te dará el prompt de bash.

.. For more information on bash, see for example 
   [Bash-Beginners-Guide]_, [gnu-bash]_, [Wikipedia-bash]_.

Para más información sobre bash, mira por ejemplo
[Bash-Beginners-Guide]_, [gnu-bash]_, [Wikipedia-bash]_.

.. _bashrc:

.. .bashrc file
   ----------------

archivo .bashrc
----------------

.. Everytime you start a new bash shell, e.g. by the command above, or when you
   first log in or open a new window (assuming bash is the default), a file
   named ".bashrc" in your home directory is executed as a bash script.  You
   can place in this file anything you want to have executed on startup, such
   as exporting environment variables, setting paths, defining aliases, setting
   your prompt the way you like it, etc.  See below for more about these
   things.

Cada vez que inicias una nueva shell bash, por ejemplo, con el comando anterior, cuando accedes por primera
vez o cuando abres una nueva ventana (asumiendo que por defecto es en bash), un archivo llamado ".bashrc" de
tu directorio home se ejecuta como un script de bash. Puedes colocar en este archivo cualquier cosa que quieras que se ejecute cuando se inicie, como exportar variables de entorno, establecer las rutas, definir alias, establecer el prompt de la manera que quieras, etc. Mira abajo para más cosas.

.. _env:

.. Environment variables
   ---------------------

Variables de entorno
---------------------

.. The command *printenv* will print out any environment variables you have
   set, e.g.::

EL comando *printenv* mostrará todas las variables de entorno establecidas, por ejemplo::

    $ printenv
    USER=rjl
    HOME=/Users/rjl
    PWD=/Users/rjl/uwhpsc/sphinx
    FC=gfortran
    PYTHONPATH=/Users/rjl/claw4/trunk/python:/Applications/visit1.11.2/src/lib:
    PATH=/opt/local/bin:/opt/local/sbin:/Users/rjl/bin
    etc.

.. You can also print just one variable by, e.g.::

También puedes mostrar una sola variable, por ejemplo::

    $ printenv HOME
    /Users/rjl

.. or::

o::

    $ echo $HOME
    /Users/rjl

.. The latter form has $HOME instead of HOME because we are actually *using*
   the variable in an echo command rather than just printing its value.  This
   particular variable is useful for things like

La último forma tiene $HOME en lugar de HOME porque een realidad estamos *usando* la variable en un
comando echo en lugar de sólamente imprimiendo su valor. Essta variable particular es útil para un montón de
cosas como

    $ cd $HOME/uwhpsc

.. which will go to the uwhpsc subdirectory of your home directory no
   matter where you start.

que irá al subdirectorio uwhpsh de tu directorio home no importa donde empieces.

.. As part of Homework 1 you are instructed to define a new environment
   variable to make this even easier,  for example by::

Como parte de la Tarea 1 se te pide definir una nueva variable de entorno para hacer esto incluso más
sencillo, por ejemplo mediante::

    $ export UWHPSC=$HOME/uwhpsc

.. Note there are no spaces around the =.   This defines a new environment
   variable and *exports* it, so that it can be used by other programs you
   might run from this shell (not so important for our purposes, but sometimes
   necessary).

Date cuenta que no hay espacios alredodor de =. Esto define una nueva variable y la *exporta*, así que puede
ser usada por otros programas que ejecutes en esta shell (esto no es muy importante para nuestro propósitos,
pero a veces es necesario).

.. You can now just do::

Ahora puedes hacer sólo esto::

    $ cd $UWHPSC

.. to go to this directory.

para ir a este directorio.

.. Note that I have set an environment variable FC as::

Nota que he definido una variable de entorno FC como::

    $ printenv FC
    gfortran

.. This environment variable is used in some Makefiles (see :ref:`makefiles`)
   to determine which Fortran compiler to use in compiling Fortran codes.

Esta variable de entorno se usa en algunos Makefiles (ver :ref:`makefiles`) para determinar
que compilador de Fortran se usará al compilar códigos en Fortran.

.. _unix_path:

.. PATH and other search paths
   ---------------------------

PATH y otras rutas de búsqueda
-------------------------------

.. Whenever you type a command at the Unix prompt, the shell looks for a
   program to run.  This is true of built-in commands and also new commands you
   might define or programs that have been installed.  To figure out where to
   look for such programs, the shell searches through the directories specified
   by the PATH variable (see :ref:`env` above).  This might look something
   like::

Cuandp escribas un comando en el prompt de Unix, la shell busca un programa para ejecutar. Esto es cierto
para los comandos incorporados y también para los nuevos comandos que definas o para los programas que hallas instalados. Para saber donde buscar dichos programas, la shell busca en los directorios especificados por la
variable PATH (ver :ref:`env` arriba). Esto puede ser algo como::

    $ printenv PATH
    PATH=/usr/local/bin:/usr/bin:/Users/rjl/bin

.. This gives a list of directories to search through, in order, separated by
   ":".   The PATH is usually longer than this, but in the above example there
   are 3 directories on the path.  The first two are general system-wide
   repositories and the last one is my own *bin* directory (bin stands for
   binary since the executables are often binary files, though often the bin
   directory also contains shell scripts or other programs in text).

Esto da una lista de directorios de búsqueda, en orden, separadas por ":". PATH es en general más largo que
esto, pero en el ejemplo superior hay 3 directorios en la ruta. Los primeros dos son reposiorios generales de todo el sistema y el último es mi propio *bin* directory (bin viene de binario ya que los ejecutables son frecuentemente archivos binarios, además a menudo los directorios bin también contiene scripts de shell u
otros programas en texto).

.. _which:

which
-----

.. The *which* command is useful for finding out the full path to the code that
   is actually being executed when you type a command, e.g.::

El comando *which* es útil para averiguar la ruta completa del código que está siendo actualmente ejecutado
cuando escribes un comando, por ejemplo::

    $ which gfortran
    /usr/bin/gfortran

    $ which f77
    $

.. In the latter case it found no program called f77 in the search path, either
   because it is not installed or because the proper diretory is not on the
   PATH.

En el último caso no se encuentra ningún programa llamado f77 en la ruta de búsqueda, o porque no
está instalado o porque el directorio no está en PATH.

.. Some programs require their own path to be set if it needs to search for
   input files.  For example, you can set MATLABPATH or PYTHONPATH 
   to be a list of directories (separated by ":") to search for .m files 
   when you execute a command in Matlab, or for .py files when you import
   a module in Python.

Algunos programas requieren que se establezca su propia ruta si necesitan buscar archivos de entrada. Por
ejemplo, puedes establecer MATLABPATH o PYTHONPATH para que sea una lista de directorios (separados por ":")
para buscar archivos .m cuando ejecutes un comando en Matlab o archivos .py cuando importes módulos en Python,

.. _prompt:

.. Setting the prompt
   ------------------

Estableciendo el promt
-----------------------

.. If you don't like the prompt bash is using you can change it by changing the
   environment variable PS1, e.g.::
 
Si no te gusta el prompt de bash que se está usando puedes modificarlo cambiando la variable de entorno
PS1, por ejemplo::

    $ PS1='myprompt* '
    myprompt* 

.. This is now your prompt.  There are various special characters you can use
   to specify things in your prompts, for example::

Este es ahora tu nuevo prompt. Ha varios caracteres especiales que puedes usar para especificar cosas en tus
prompts, por ejemplo::

    $ PS1='[\W] \h% '
    [sphinx] aspen% 

.. tells me that I'm currently in a directory named sphinx on a computer named
   aspen.  This is handy to keep track of where you are, and what machine the
   shell is running on if you might be using ssh to connect to remote machines
   in some windows.

me dice que esto actualmente en un directorio llamado sphinx en un ordenador llamado aspen. Esto es útil
para mantener la pista de donde estas, y la shell de que máquina se está ejecutando si estuvieras usando ssh
para conectarte a máquinas remotas en algunas ventanas.

.. Once you find something you like, you can put this command in your .bashrc
   file.

Una vez que encuentres algo que te guste, puedes poner este comando en tu archivo .bashrc.

.. Further reading
   ---------------

Más lecturas
-------------

[Wikipedia-unix-utilities]_
