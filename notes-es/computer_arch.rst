

.. _computer_arch:

.. =============================================================
   Computer Architecture
   =============================================================

============================
Arquitectura del Ordenador
============================

.. This page is currently a very brief survey of a few issues.  See the links
   below for more details.

Esta página es un breve repaso de varios temas. Ver los enlaces de abajo para más detalles.

.. Not so long ago, most computers consisted of a single central processing unit
   (CPU) together with some a few registers (high speed storage locations for
   data currently being processed) and *main memory*, typically a hard disk
   from which data is moved in and out of the registers as needed (a slower
   operation).  Computer speed, at least for doing large scale scientific
   computations, was generally limited by the speed of the CPU.  Processor
   speeds are often quoted in terms of *Hertz* the number of *machine cycles
   per second* (or these days in terms of GigaHertz, GHz, in billions of cycles
   per second).  A single operation like adding or multiplying two numbers
   might take more than one cycle, how many depends on the architecture and
   other factors we'll get to later.  For scientific codes, the most important
   metric was often how many floating point operations could be done per
   second, measured in flops (or Gigaflops, etc.).   
   See :ref:`flops` for more about this.

No hace mucho, la mayoría de los ordenadores consistían una sola unidad cetral de procesos (CPU, del iglés
central processing unit) junto con algunos registros (localizaciones de almacenamiento de alta velocidad para
los datos que se están procesado en el momento) y una *memoria principal*, tipicamente un disco duro cuyos
datos se mueven dentro y fuera de los registros cuando es necesario (una operación más lenta). La velocidad del ordenador, al menos para hacer cálculos científicos a gran escala, estaba limitada por la velocidad de la CPU. La velocidad de los procesadores medía, en términos de *Hercios*, el número de *ciclos de la máquina por
segundo* (o estos días en términos de GigaHercios,GHz, miles de millones de ciclos por segundo). Una sola
operación como sumar o multiplicar dos números puede tomar más de un ciclo, dependiendo de la arquitectura
y de otros factores que veremos más adelante. Para códigos científicos, la medida más importante solía ser
cuantas operaciones en punto flotante podían ser hecha por segundo, medidas en flops (o Gigaflops, etc). Ver
:ref:`flops`  para ver más información sobre esto.

.. _moores_law:

.. Moore's Law
   -----------

Ley de Moore
-------------

.. For many years computers kept getting faster primarily because the clock
   cycle was reduced and so the CPU was made faster.  In 1965, Gordon Moore
   (co-founder of Intel) predicted that the transistor density (and hence the
   speed) of chips would double every 18 months for the forseeable future.
   This is know as **Moore's law** 
   This proved remarkably accurate for more than 40 years, see the graphs at
   `[wikipedia-moores-law] <http://en.wikipedia.org/wiki/Moore%27s_law>`_.
   Note that doubling every 18 months means an increase by a factor of 4096
   every 14 years.

Duranto muchos años los ordenadores eran cada vez más rápidos principalmente porque el tiempo de los ciclos
disminuían y por tanto, las CPU eran más veloces. En 1954, Gordon Moore (co-fundador de Intel) predijo que la 
densidad de los transistores (y así su velocidad) se duplicarían cada 18 meses en un futuro previsible. Esto
se conoce como **ley de Moore**. Este fue bastante preciso durante más de 40 años, ver el gráfico en
`[wikipedia-moores-law] <http://en.wikipedia.org/wiki/Moore%27s_law>`_.
Nota que duplicar cada 18 meses significa un incremento de un factor de 4096 cada 14 años.

.. Unfortunately, the days of just waiting for a faster computer in order to do
.. larger calculations has come to an end.  Two primary considerations are:

Desafortunadamente, los días de esperar a ordenadores más rápidos para hacer cálculos más grandes han llegado
a su fin. Las dos consideraciones principales son:

..  * The limit has nearly been reached of how densely transistors can be
      packed and how fast a single processor can be made.

.. * Even current processors can generally do computations much more quickly
     than sufficient quantities of data can be moved between memory and
     the CPU.  If you are doing 1 billion meaningful multiplies per second 
     you need to move lots of data around.

 * El límite de cuán denso puede ser un transistor y cuán rápido puede ser hecho casi se ha alcanzado.

 * Incluso los procesadores actuales pueden hacer generalmente cálculos mucho más rápido que mover una
   cantidad de datos

.. There is a hard limit imposed by the speed of light.  A 2 GHz
   processor has a clock cycle of 0.5e-9 seconds.  The speed of light is
   about 3e8 meters per second. So in one clock cycle information cannot
   possibly travel more than 0.15 meters.  (A light year is a long distance but
   a light nanosecond is only about 1 foot.)  If you're trying to move billions
   of bits of information each second then you have a problem.

Hay un duro limite impuesto por la velocidad de la luz. Un procesador de 2 GHz tiene un ciclo de 0.5e-9
segundos. La velocidad de la luz es de 3e8 metros por segundo. Así, en un ciclo, la información no puede
viajar más de 0.15 metros. (Un año luz es una distancia larga pero nanosegundo luz es aproximadamente un pie).
Si estás intentando mover miles de millones de bits de información cada segundo entonces tiene un problema.
 
.. Another major problem is power consumption.  Doubling the clock speed of a
   processor takes roughly 8 times as much power and also produces much more
   heat.  By contrast, doubling the number of processors only takes twice as
   much power.

Otro gran problema es el consumo de energía. Doblar la velocidad del procesador consume aproximadamente 8
veces más energía y produce mucho más calor. Por contraste, doblar el número de procesadores solo consume 
el doble de energía.

.. There are ways to continue improving computing power in the future, but they
   must include two things:

Hay maneras de mejorar la potencia de cálculo en el futuro, pero deben incluir dos cosas:

 ..  * Increasing the number of cores or CPUs that are being used simultaneously
       (i.e., parallel computing)

 ..  * Using memory hierachies to improve the ability to have large amounts of
       data available to the CPU when it needs it.


 * Incrementar el número de núcleos o CPUs que estén siendo usadas simultaneamente (i.e. cálculo en paralelo)

 * Usar jerarquías de memoria para mejorar la habilidad de tener grandes cantidades de datos disponibles en
   la CPU cuando se necesite.

.. Further reading
   ---------------

Más lecturas
-------------

.. See the :ref:`biblio_computer_arch` section of the bibliography.

Ver la sección :ref:`biblio_computer_arch` de la bibliografía.

.. See also the slides from [Yelick-UCB]_, [Gropp-UIUC]_ and other courses
   listed in the bibliography.

Ver también las diapositivas de [Yelick-UCB]_, [Gropp-UIUC]_ y otros cursos listados en la bibliografía.
