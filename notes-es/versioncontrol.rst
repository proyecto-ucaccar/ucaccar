.. _versioncontrol:

.. =============================================================
   Version Control Software
   =============================================================

==============================================
Software de Control de Versiones
==============================================

.. In this class we will use *git*.  See the section :ref:`git`
   for more information on using *git* and the repositories required for this
   class.

En esta clase usarmos *git*. Ver la sección :ref:`git` para más infomación sobre el
uso de *git* y sobre los repositorios requieridos para esta clase.

.. There are many other version control systems that are currently popular,
   such as cvs, Subversion, Mercurial, and Bazaar.
   See [wikipedia-revision-control-software]_ for a much longer list with
   links.
   See [wikipedia-revision-control]_ for a general discussion of such systems.

Hay otros muchos sistemas de control de versiones que son populares en la actualidad, tales
como cvs, Subversion, Mercurial y Bazaar. Ver [wikipedia-revision-control-software]_ para una lista
más larga con enlaces. Ver [wikipedia-revision-control]_ para una discusión general de estos sistemas.

.. Version control systems were originally developed to aid in the development
   of large software projects with many authors working on inter-related
   pieces.  The basic idea is that you want to work on a file (one piece of the
   code), you check it out of a repository, make changes, and then check it
   back in when you're satisfied.  The repository keeps track of all changes
   (and who made them) and can restore any previous version of a single file or
   of the state of the whole project.  It does not keep a full copy of every
   file ever checked in, it keeps track of differences (*diff*s) between
   versions, so if you check in a version that only has one line changed from
   the previous version, only the characters that actually changed are kept
   track of.  

Los sistemas de control de versiones se desarrollaron originalmente para ayudar en
el desarrollo de grandes proyectos de softwares con muchos autores trabajando con partes
interrelacionadas. La idea básica es que quieres que trabajar en un archivo (un trozo de código),
lo revisas fuera del repositorio, haces cambios, y entonces lo vuelves a revisar cuando estes
satisfecho. El repositorio guarda todos los cambios (y quien los ha hecho) y puede restaurar cualquier
versión de un único archivo o el proyecto completo al estado anterior. No guarda una copia completa de
cada archivo cada vez que se contrasta, guarda las diferencias (*diff*) entre las versiones, así si
compruebas una versión en la que sólo se ha cambiado una línea desde la versión anterior, sólo los
caracteres que se hallan cambiado serán guardados.

.. It sounds like a hassle to be checking files in and out, but there are a
   number of advantages to this system that make version control an
   extremely useful tool even for use with you own projects if you are the only
   one working on something.  Once you get comfortable with it you may wonder
   how you ever lived without it.

Suena un fastidio estar comprobando los archivos dentro y fuera, pero hay numerosas ventajas
en estos sistemas para hacer del control de versiones una herramienta extremadamente útil incluso
para usarlos en tus propios proyectos cuando trabajas sólo en algo. Una vez que te acostumbres te preguntaras
como viviste sin él.

.. Advantages include:

Las ventajas incluyen:

   .. * You can revert to a previous version of a file if you decide the changes
     you made are incorrect.  You can also easily compare different versions
     to see what changes you made, e.g. where a bug was introduced.

   * Puedes volver a una versión previa de un archivo si decides que los cambios realizados
     eran incorrectos. Puedes comparar fácilmente diferentes versiones para ver los cambios que
     hicistes, por ejemplo, donde introducistes un bug.


   .. * If you use a computer program and some set of data to produce some
      results for a publication, you can check in exactly the code and data
      used.  If you later want to modify the code or data to produce new results,
      as generally happens with computer programs, you still have access to the
      first version without having to archive a full copy of all files for
      every experiment you do.  Working in this manner is crucial if you want
      to be able to later reproduce earlier results, as if often necessary if
      you need to tweak the plots for to some journal's specifications or if a
      reader of your paper wants to know exactly what parameter choices you
      made to get a certain set of results.   This is an important aspect of
      doing *reproducible research*, as should be required in science.  (See
      Section :ref:`reproducibility`).  If nothing else you can save yourself
      hours of headaches down the road trying to figure out how you got your
      own results.

   * Si usas un programa de cálculo y algún conjunto de datos para producir algún resultado
     para una publicación, puedes comprobar exáctamente el código y los datos usados. Si luego
     quieres modificar el código o los datos para producor nuevos resultados, como ocurre en
     general en los programas de cálculo, todavía tienes acceso a la primera versión sin tener que
     almacenar una copia completa de todos los archivos por cada experimento que hagas. Trabajar
     de esta manera es crucial si quieres ser capaz de reproducir resultados tempranos, así como si
     necesitas ajustar las gráficas para las especificaciones de alguna revista o si un lector de
     tu trabajo quiere saber exactamente que parámetros elegistes para obtener un cierto conjunto
     de resultados. Este es un aspecto importante de hacer *investigación reproducible*, como debería
     ser requerido en ciencias. (Ver la Sección :ref:`reproducibility`). Además puede ahorrarte horas de
     dolor de cabeza intentando averiguar como obtuvistes tus propios resultados.

   .. * If you work on more than one machine, e.g. a desktop and laptop, version
      control systems are one way to keep your projects synched up between
      machines.

   * Si trabajas en más de un ordenador, por ejemplo, en uno de escritorio y en un portatil,
     los sistemas de control de versiones son maneras de mantener tus proyectos sincronizados
     entre las máquinas.

.. Client-server systems
   ---------------------

Sistemas cliente-servidor
--------------------------

.. The original version control systems all used a client-server model, in
   which there is one computer that contains **the repository** and everyone
   else checks code into and out of that repository.

Originalmente, todos los sistemas de control de versiones usaban un modelo cliente-servidor,
en los cuales hay un ordenador que contiene **el repositorio** y el resto comprueba el código
dentro y fuera de ese repositorio.

.. Systems such as CVS and Subversion (svn) have this form.
   An important feature of these systems is that only the repository has the
  full history of all changes made.  

Sistemas como CVS y Subversion (svn) tienen esta forma. Un aspecto importante de estos sistemas es que
solo un repositorio tiene un historial completo de todos los cambios realizados.

.. There is a `software-carpentry webpage on version control
    <http://software-carpentry.org/4_0/vc/>`_ that gives a brief overview
   of client-server systems.

Hay una `página web de carpintería de software sobre control de versiones
    <http://software-carpentry.org/4_0/vc/>`_ que ofreven un breve resumen de sistemas cliente-servidor.

.. Distributed systems
   -------------------

Sistemas distribuidos
---------------------

.. Git, and other systems such as Mercurial and Bazaar, use a distributed
   system in which there is not necessarily a "master repository".  Any working
   copy contains the full history of changes made to this copy.  

Git, y otros muchos sistemas como Mercurial y Bazaar, usan un sistema distribuido en los cuales
no es necesario un "repositorio maestro". Cualquier copia de trabajo contiene un historial completo de los cambios hechos a dicha copia.

.. The best way to get a feel for how *git* works is to use it, for example
   by following the instructions in Section :ref:`git`.

La mejor manera de ver como *git* funciona es usándolo, por ejemplo siguiendo las
instrucciones en la Sección :ref:`git`.

.. Further reading
   ---------------

Más lecturas
------------

.. See the :ref:`biblio_vcs` section of the bibliography.

Ver la sección :ref:`biblio_vcs` en la bibliografía.
