

.. _sagemath:

=============================================================
Sage
=============================================================

.. `Sage <http://sagemath.org>`_ is an open source collection of mathematical
.. software with a common Python interface.  The lead developer is William
.. Stein, a number theorist in the Mathematics Department at the University of
.. Washington.  

Sage es una colección de código abierto de software matemático con una interfaz común de Python.
El desarrollador principal es William Stein, un investigador en teoría de números en el departamenteo de matemáticas de la universidad de Washington.

Sage <http://sagemath.org>`_ es una 

.. The Sage notebook was an original model for the :ref:`ipython_notebook`. 
Sage notebook era un modelo original para: ref:`ipython_notebook`

.. You can try Sage online by typing into a cell at
.. `<https://sagecell.sagemath.org/>`_.  You can type straight 
.. Python as well as using the enhanced capabilities of Sage. 

Se puede probar Sage online aquí: `<https://sagecell.sagemath.org/>`_.
Puede escribir python usando las mejoradas capacidades de Sage.


.. See:

Vea:

* `<http://interact.sagemath.org/top-rated-posts>`_ 
* `<http://trac.sagemath.org/sage_trac/>`_

.. for some online sage examples.

para algunos ejemplos online.