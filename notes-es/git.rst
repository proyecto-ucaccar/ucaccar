 .. _git:

=============================================================
Git
=============================================================

.. See :ref:`versioncontrol` and the links there
.. for a more general discussion of the concepts.

Ver :ref:`versioncontrol` y los enlaces que contiene
para una explicación más general de los conceptos.

.. _classgit:
.. _classgit:

.. Instructions for cloning the class repository
.. ---------------------------------------------

Instrucciones para clonar el repositiorio de clase
---------------------------------------------------

.. All of the materials for this class, including homeworks, sample programs,
.. and the webpages you are now
.. reading (or at least the *.rst* files used to create them, see
.. :ref:`sphinx`), are in a Git repository hosted at Bitbucket, located
.. at  
.. `<http://bitbucket.org/rjleveque/uwhpsc/>`_.
.. In addition to viewing the files via the link above, you can also view
.. changesets, issues, etc. (see :ref:`bitbucket`).

Todos los materiales del curso, incluidas las tareas, los programas de ejemplo,
y las páginas web que estás leyendo (o al menos los archivos *.rst* usados para crearlas, ver
:ref:`sphinx`), estan en un repositorio Git almacenado en Bitbucket, localizado
en 
`<http://bitbucket.org/rjleveque/uwhpsc/>`_.
Además de ver los archivos en el enlace anterior, puedes ver también los cambios realizados en ellos, los
temas, etc. (ver :ref:`bitbucket`).

.. **Note:** This repository contains all of the files used during the 2013
.. version of this course, some of which are referred to in the Lecture Videos.
.. The repository also contains some new material for 2014 and will continue to
.. be added to during this quarter. See the overview at the top of the page
.. `<http://bitbucket.org/rjleveque/uwhpsc/>`_
.. for an outline of the directory structure.  (This overview is showing the
.. file README.md that is in the top level directory of the repository.)

**Nota:** Este repositorio contiene todos los archivos usados en la versión 2013
de este curso, algunos de los cuales aparecen en los Vídeos de Clase. Este repositorio
también contiene algún material nuevo para 2014 y se le continuará añadiendo material
durante el semestre. Mira el resumen en la parte superior de la página
`<http://bitbucket.org/rjleveque/uwhpsc/>`_
para un esbozo general de la estructura del directorio. (Este resumen aparece en el archivo README.md que
está en el nivel superior del repositorio).

.. To obtain a copy, simply move to the directory where you want your copy to
.. reside (assumed to be your home directory below)
.. and then *clone* the repository::

Para obtener una copia, simplemente colocate en el directorio donde quieras que se
almacene (se asume que sea debajo de tu directorio home) y entonces *clona* el repositorio::

        $ cd 
        $ git clone https://rjleveque@bitbucket.org/rjleveque/uwhpsc.git

.. Note the following:
.. * It is assumed you have *git* installed, see
..   :ref:`software_installation`.
.. * The clone statement will download the entire repository as a new
..   subdirectory called *uwhpsc*, residing in your home directory.  If you
..   want *uwhpsc* to reside elsewhere, you should first *cd* to that
..  directory.

Nota lo siguiente:

 * Se asume que tienes *git* instalado, ver :ref:`software_installation`.

 * La orden clone descargará el repositiorio completo como un nuevo subdirectorio llamado *uwhpsc*, residiendo en tu directorio home. Si quieres que *uwhpsc* resida en cualquier otra parte, deberías hacer *cd* primero a ese directorio.

.. It will be useful to set a Unix environment variable (see :ref:`env`) called
.. *UWHPSC* to refer to the directory you have just created.  Assuming you are
.. using the bash shell (see :ref:`bash`), and that you cloned uwhpsc
.. into your home directory, you can do this via::

Es útil establecer una varible de entorno de Unix (ver :ref:`env`) llamada
*UWHPSC* para referenciar al directorio que acabas de crear. Asumiendo que 
estás usando la shell de bash (ver :ref:`bash`), y que has clonado uwhpsc en tu 
directorio home, puedes hacerlo via::

        $ export UWHPSC=$HOME/uwhpsc

.. This uses the standard environment variable *HOME*, which is the full path
.. to your home directory.

Esto usa la variable de entorno estandard *HOME, que es la ruta completa a tu
directorio home.

.. If you put it somewhere else, you can instead do::

Si lo colocas en cualquier otra parte, puedes hacer::

        $ cd uwhpsc
        $ export UWHPSC=`pwd`

.. The syntax
.. *`pwd`* means to run the *pwd* command (print working directory) and insert the
.. output of this command into the export command.  


La sintaxis  *`pwd`* significa ejecutar el comando *pwd* (imprimir tu directorio de trabajo) e insertar
la salida de esta orden en el comando export.

.. Type::
Teclea::

        $ printenv UWHPSC

.. to make sure *UWHPSC* is set properly. This should print the full path to the
.. new directory.

para asegurarte que *UWHPSC* está establecido correctamente. Esto debería imprimir la ruta completa
al nuevo directorio.

.. If you log out and log in again later, you will find that this environment
.. variable is no longer set.  Or if you set it in one terminal window, it
.. will not be set in others.  To have it set automatically every time a new
.. bash shell is created (e.g. whenever a new terminal window is opened), add a
.. line of the form::

Si te desconectas y vuelves a conectarte de nuevo, descubrirás que esta variable de entorno
ya no existe. O si la creas en una terminal, no estará creada en otras. Para inicializada cada vez
que se crea una shell de bash (ej. si se abre una nueva terminal), añade una línea de la forma::

        export UWHPSC=$HOME/uwhpsc

.. to your *.bashrc* file.  (See :ref:`bashrc`).  This assumes it is in your
.. home directory.  If not, you will have to add a line of the form::

en tu archivo  *.bashrc*. (Ver :ref:`bashrc`). Se asume que estás en tu directorio
home. Si no, tendrás que añadir una línea de la forma::

        export UWHPSC=full-path-to-uwhpsc

.. where the full path is what was returned by the *printenv* statement above.

donde la ruta completa es lo que se devuelve mediante el comando  *printenv* de arriba.

.. _uwhpsc_update:

.. Updating your clone
.. -------------------

Actualizando tu clon
--------------------

.. The files in the class repository will change as the quarter progresses ---
.. new notes, sample programs, and homeworks will be added.  In order
.. to bring these changes over to your cloned copy, all you need to do is::

Los archivos en el repositorio de clase cambiarán a medida que el semestre avance ---
se le añadirán nuevos apuntes, ejemplos de programas y tareas. Para traer estos cambios
a tu directorio actualizado, todo lo que necesitas hacer es::

        $ cd $UWHPSC
        $ git fetch origin
        $ git merge origin/master

.. Of course this assumes that *UWHPSC* has been properly set, see above.

Por supuesto se asume que *UWHPSC* se ha establecido correctamente, ver arriba.

.. The *git fetch* command instructs *git* to fetch any changes from *origin*,
.. which points to the remote bitbucket repository that you originally cloned
.. from.  In the merge command, `origin/master` refers to the master branch 
.. in this repository (which
.. is the only branch that exists for this particular repository).
.. This merges any changes retrieved into the files in your current working
.. directory.

El comando *git fetch* de *git* busca cualquier cambio de *origin*,
que apunta al repositorio remoto que clonaste originalmente. En el comando merge,
`origin/master` se refiere a la rama principal de este repositorio (que es la única rama
que existe en este repositorio particular).
Esto funde cualquier cambio recuperado en en los archivos de tu directorio de trabajo. 

.. The last two command can be combined as::
Los últimos dos comandos pueden combinarse como::

        $ git pull origin master

.. or simply::
o simplemente

        $ git pull
 
.. since `origin` and `master` are the defaults.
ya que `origin`y `master` son predeterminados.

.. _mygit:

.. Creating your own Bitbucket repository
.. --------------------------------------

Creando tu propio repositorio de Bitbucket
------------------------------------------

.. In addition to using the class repository, students in AMath 483/583 are
.. also required to create their own repository on Bitbucket.  It is possible
.. to use *git* for your own work without creating a repository on a hosted
.. site such as Bitbucket (see :ref:`newgit` below), but there are several
.. reasons for this requirement:

Además de usar el repositorio de clase, a los estudiantes de AMath 483/583 se les
pide crear su propio repositorio en Bitbucket. Es posible usar *git* para tu propio
trabajo sin crear un repositorio en un sitio como Bitbucket (ver :ref:`newgit` mas abajo),
pero hay bastantes razones para esta petición: 


 * Deberías aprender a cómo usar Bitbucket para algo más que para extraer los cambios.

 * Aprenderás a usar este repositorio para "presentar" las soluciones a tus tareas.
   Le darás a tu instructor y TA permiso para clonar tu repositorio para que así podamos
   evaluar tu tarea (otras personas no podrán clonar o verlo a menos que les des permiso).

 * Se recomienda que cuando el curso acabe continues usando tu repositorio como 
   una manera de proteger tu trabajo importante en otro ordenador (que también tiene
   todos los beneficios del control de versiones). En este punto, puedes cambiar los permisos
   para que el instructor y TA no tengan accesos.

..  * You should learn how to use Bitbucket for more than just pulling changes.

..  * You will use this repository to "submit" your solutions to homeworks.
   You will give the instructor and TA permission to clone your repository so
   that we can grade the homework (others will not be able to clone or view it
   unless you also give them permission).

..  * It is recommended that after the class ends you 
   continue to use your repository as a way to back up your important work on
   another computer (with all the benefits of version control too!).
   At that point, of course, you can change the permissions so the
   instructor and TA no longer have access.


.. Below are the instructions for creating your own repository.  Note that
.. this should be a *private repository* so nobody can view or clone it unless
.. you grant permission.

Abajo hay instrucciones para crear tu propio repositorio. Observa que esto
debería ser un *repositorio privado* así que nadie podría ver o clonarlo a menos
que le des permisos.

.. Anyone can create a free private repository on Bitbucket.  
.. Note that you can also create an unlimited number of public repositories
.. free at Bitbucket, which you might want to do for open source software
.. projects, or for classes like this one.

Cualquiera puede crear un repositorio privado gratuito en Bitbucket. Oberva que puedes crea un número
ilimitado de repositorios públicos gratis en Bitbucket, que es posible que quieras para proyectos
de software libres, o para clases como esta.

.. (To make free open access repositories that can be viewed by anyone, 
.. `GitHub <https://github.com/>`_
.. is recommended, which allows an unlimited number of 
.. open repositories and is widely used for open source projects.)


(Para hacer repositorios gratuitos que puedan ser visto por cualquiera, se recomienda
`GitHub <https://github.com/>`_
que permite un número ilimitado de repositorios abiertos y es ampliamente usado para projectos libres).

.. To get started, follow the directions below exactly.  
.. We will work through this as part of Lab 2 (Thursday, April 3), but feel
.. free to start earlier.

Para empezar,  sigue las direcciones de abajo exactamente. Trabajaremos esto como
parte del Laboratorio 2. (Jueves, 3 Abril), pero sientete libre de empezar antes.

.. Doing this will be part of :ref:`homework1`.
.. We will clone your repository and check that *testfile.txt* has been created
.. and modified as directed below.

Hacer esto será parte de :ref:`homework1`.
Clonaremos tu repositorio y comprobaremos que *testfile.txt* se ha creado y modificado
como se indica a continuación. 

.. On the machine you're working on::

#. En la máquina en la que estes trabajando:: 

    $ git config --global user.name "Your Name"
    $ git config --global user.email you@example.com

   Esto se usará cuando realices cambios. Si no haces esto, podrías tener un
   mensaje de advertencia la primera vez.

   .. These will be used when you commit changes.  If you don't do this, you might get a warning message the first time you try to commit.

      .. #. Go to `<http://bitbucket.org/>`_ and click on "Sign up now" if you don't
	 already have an account.

#. Ve a `<http://bitbucket.org/>`_ y haz click en "Sign up now" si aún no tienes una cuenta.

   .. #. Fill in the form, make sure you remember your username and password.

#. Rellena el formulario, asegurate de recordar tu nombre de usuario y tu contraseña.

   .. #. You should then be taken to your account.  Click on "Create" next to "Repositories".

#. Deberías ser dirigido a tu cuenta. Haz click en "Create" y después a "Repositories".

   .. #. You should now see a form where you can specify the name of a repository 
	 and a description.  The repository name need not be the same as your user 
 	 name (a single user might have several repositories).  For example, the class
   	 repository is named *uwhpsc*, owned by user *rjleveque*.
    	 To avoid confusion, you should probably not name your repository
   	 *uwhpsc*.  

#. Deberías ver un formulario donde puedes especificar el nombre del repositorio y 
   una descripción. El nombre del repositorio no necesita ser el mismo que el nombre de usuario
   (un mismo usuario pude tener varios repositorios). Por ejemplo, el repositorio de clase se llama
   *uwhpsc*, creado por *rjleveque*. Para evitar confusiones, no deberías llamar a tu repositorio
   *uwhpsc*.

   .. You should stick to lower case letters and numbers in your repository
      name, e.g. *myhpsc* or *amath583* might be good choices.  Upper case and 
      special symbols such as underscore sometimes get modified by bitbucket
      and the repository name you try to paste into the homework submission
      form might not agree with what bitbucket expects.

   Deberías usar minúsculas y números al nombre de tu repositorio, por ejemplo *myhsc*
   o *amath583* pueden ser buenas opciones. Las mayúsculas y los caracteres especiales
   tales como la barra baja a veces se modifican en bitbucket y el nombre del repositorio
   al que intentes pegar la tarea puede no concordar con lo que bitbucket espera.

   .. Don't name your repository *homework1* because you will be using the 
      same repository for other homeworks later in the quarter.

   No llames a tu repositorio *homework1* porque se usará el mismo repositorio para otras tareas a lo
   largo del cuatrimestre.

   .. #. Make sure you click on "Private" at the bottom.  Also turn "Issue
      tracking" and "Wiki" on if you wish to use these features.  

#. Asegurate que haces click en "Private" en la parte de abajo. Enciende también
   "Issue tracking" y "Wiki" si desea usar estas características.

   .. #. Click on "Create repository".  

#. Haz click en "Create repository".

   .. #. You should now see a page with instructions on how to *clone* your 
      (currently empty) repository.  In a Unix window, *cd* to the directory where
      you want your cloned copy to reside, and perform the clone by typing in
      the clone command shown.  This will create a new directory with the same
      name as the repository.

#. Deberías ver una página con instrucciones de como *clonar* tu repositorio (actualemente vacío).
   En una ventana Unix, has *cd* al directorio donde quieras colocar tu clon, y realiza el clonado
   escribiendo el comando mostrado. Esto creará un nuevo directorio con el mismo nombre que el repositorio.

   .. #. You should now be able to *cd* into the directory this created.

#. Deberías ahora se capaz de usar *cd* en el directorio creado.

   .. #. To keep track of where this directory is and get to it easily in the
      future, create an environment variable *MYHPSC* from inside this directory
      by::

#. Para mantener la pista de donde está el directorio y llegar a él fácilmente en el futuro,
   crea una variable de entorno *MYHPSC* dentro de este directorio de la siguiente manera::

        $ export MYHPSC=`pwd`

   .. See the discussion above in section :ref:`classgit` for what this does.  You
      will also probably want to add a line to your *.bashrc* file to define 
      *MYHPSC* similar to the line added for *UWHPSC*.

   Ver la sección :ref:`classgit` para ver que hace. Probablemente querrás añadir una línea a tu
   archivo *.bashrc* para definir *MYHPSC* deforma similar a la línea añadida para *UWHPSC*.

   .. #. The directory you are now in will appear empty if you simply do::

#. El directorio en que te encuentras ahora aparecerá vacío si simplementes haces::

        $ ls

   .. But try::

   Pero prueba::

        $ ls -a
        ./  ../  .git/


   .. the *-a* option causes *ls* to list files starting with a dot, which are
      normally suppressed.  See :ref:`ls` for a discussion of *./* and *../*.
      The directory *.git* is the directory that stores all the information
      about the contents of this directory and a complete history of every file
      and every change ever committed.  You shouldn't touch or modify the files in
      this directory, they are used by *git*.

   la opción *-a* hace que *ls* liste los archivos que comienzan con un punto, que
   normalmente son suprimidos. Ver :ref:`ls` para una exlicación de *./* y *../*.
   El directorio *.git* es el directorio que almacena toda la información sobre los
   contenidos de este directorio y el historial completo de cada archivo y cada cambio
   realizado. No deberías tocar o modificar los archivos de este directorio, son usados
   por *git*.

   .. #. Add a new file to your directory::

#. Añade un nuevo archivo a tu directorio::

        $ cat > testfile.txt
        This is a new file 
        with only two lines so far.
        ^D

   .. The Unix *cat* command simply redirects everything you type on the
      following lines into a file called *testfile.txt*.  This goes on until
      you type a <ctrl>-d (the 4th line in the example
      above).  After typing <ctrl>-d you should get the Unix
      prompt back.  Alternatively, you could create the file testfile.txt using
      your favorite text editor (see :ref:`editors`).

   El comando de Unix *cat* simplemente redirecciona todo lo que escribas en las
   siguientes líneas en un archivo llamado *textfile.txt*. Esto continua hasta que
   escribas <ctrl>-d (la 4ª línea en el ejemplo superior). Después de escribir <ctrl>-d
   deberías obtener el prompt de Unix. Alternativamente, puedes crear el archivo testfile.txt
   usando tu editor de texto favorito (ver :ref:`editors`).

   .. #. Type::

#. Escribe::

        $ git status -s

   .. The response should be::

   La respuesta debería ser::

        ?? testfile.txt

   .. The ?? means that this file is not under revision control.  
      The *-s* flag results in this *short* status list.  Leave it off for more
      information.

   ?? significa que este archivo no está bajo control de versiones. La bandera *-s*
   produce esta lista de estado *corta*. Déjala apagada para más información.
  

   .. To put the file under revision control, type::
   
   Para poner el archivo bajo control de versiones, escribe::

        $ git add testfile.txt
        $ git status -s
        A  testfile.txt

   .. The A means it has been added.  However, at this point *git* is not
      we have not yet taken a *snapshot* of this version of the file.
      To do so, type::

   La A significa que ha sido añadido. Sin embargo, en este punto *git* aún no tiene ninguna *instantánea*
   de esta versión del archivo. Para hacer esto, escribe::

        $ git commit -m "My first commit of a test file."

   .. The string following the -m is a comment about this commit that may help
      you in general remember why you committed new or changed files.  

   La cadena siguiente  a -m es un comentario sobre este registro que quizás te ayude
   a recordar por qué realizaste un nuevo archivo o un cambio.

   .. You should get a response like::

   Deberías obtener una respuesta como::

        [master (root-commit) 28a4da5] My first commit of a test file.
         1 file changed, 2 insertions(+)
         create mode 100644 testfile.txt


   .. We can now see the status of our directory via::

   Podemos ahora ver el estado de nuestro directorio via::

        $ git status
        # On branch master
        nothing to commit (working directory clean)

   .. Alternatively, you can check the status of a single file with::

   Alternativamente, puedes comprobar el estado de un solo archivo con::

        $ git status testfile.txt

   .. You can get a list of all the commits you have made (only one so far)
      using::

   Puedes obtener una lista de todos los registros que hallas hecho (por ahora solo uno)
   usando:: 

    $ git log

    commit 28a4da5a0deb04b32a0f2fd08f78e43d6bd9e9dd
    Author: Randy LeVeque <rjl@ned>
    Date:   Tue Mar 5 17:44:22 2013 -0800

        My first commit of a test file.

   .. The number 28a4da5a0deb04b32a0f2fd08f78e43d6bd9e9dd above is the "name"
      of this commit and you can always get back to the state of your files as
      of this commit by using this number.  You don't have to remember it, you
      can use commands like *git log* to find it later.

   El número 28a4da5a0deb04b32a0f2fd08f78e43d6bd9e9dd de arriba es el "nombre"
   de este registro y puedes volver atrás al estado de tus archivo en este registro
   usando este número. No necesitas recordarlo, puedes usar comando como *git log*
   para encontrarlo más adelante.

   .. Yes, this is a number... it is a 40 digit hexadecimal number, meaning it
      is in base 16 so in addition to 0, 1, 2, ..., 9, there are 6 more digits
      a, b, c, d, e, f representing 10 through 15.  This number is almost
      certainly guaranteed to be unique among all commits you will ever
      do (or anyone has ever done, for that matter).  It is computed based
      on the state of all the files in this snapshot as a `SHA-1
      Cryptographic hash function <http://en.wikipedia.org/wiki/SHA-1>`_,
      called a SHA-1 Hash for short.

   Sí, esto es un número... es un número hexadecimal de 40 dígitos, esto significa que está
   en base 16, esto significa que además de 0, 1, 2, ..., 9, hay 6 dígitos más a, b, c, d, e, f
   representando del 10 hasta el 15. Este número está casi garantizado que será unico para todos
   los registros que harás (o que cualquiera halla hecho, para ese tema). Es un cálculo basado en
   el estado de todos los archivos en esa instantánea usando una función hash `SHA-1 Cryptographic
   <http://en.wikipedia.org/wiki/SHA-1>`_, llamada función SHA-1 Hash para abreviar. 

   .. Modifying a file 
   .. ----------------

   Modificando un archivo
   ----------------------

   .. Now let's modify this file::
   
   Ahora modifiquemos este archivo::

        $ cat >> testfile.txt
        Adding a third line
        ^D

   .. Here the *>>* tells *cat* that we want to add on to the end of an
      existing file rather than creating a new one.  (Or you can edit the file
      with your favorite editor and add this third line.)

   Aquí *>>* le dice a *cat* que queremos añadir al final de un archivo ya existente
   en lugar de crear uno nuevo. (O puedes editar el archivo con tu editor favorito y añadir
   esta tercera línea.)

   .. Now try the following::

   Ahora prueba lo siguiente::

        $ git status -s
         M testfile.txt

   .. The M indicates this file has been modified relative to the most recent
      version that was committed.  

   La M indica que este archivo ha sido modificado con respecto a la versión
   más reciente que se ha registrado.

   .. To see what changes have been made, try::

   Para ver los que cambios que se han realizado, prueba::

        $ git diff testfile.txt

   .. This will produce something like::

   Esto producirá algo como::

        diff --git a/testfile.txt b/testfile.txt
        index d80ef00..fe42584 100644
        --- a/testfile.txt
        +++ b/testfile.txt
        @@ -1,2 +1,3 @@
         This is a new file
         with only two lines so far
        +Adding a third line

   .. The + in front of the last line shows that it was added.
      The two lines before it are printed to show the context.  If the
      file were longer, *git diff*
      would only print a few lines around any change to indicate the context.

   El + al principio de la última linea muestra que se ha añadido. 
   Las dos líneas anteriores se escriben para mostrar el contexto. Si el archivo
   fuera más largo, *git diff* solo imprimiría unas pocas líneas alrededor de los cambios
   para indicar el contexto.

   .. Now let's try to commit this changed file::

   Ahora, intentemos registrar este archivo modificado::

        $ git commit -m "added a third line to the test file"

   .. This will fail!  You should get a response like this::

   ¡Esto fallará! Deberías obtener una respuesta como esta::

        # On branch master
        # Changes not staged for commit:
        #   (use "git add <file>..." to update what will be committed)
        #   (use "git checkout -- <file>..." to discard changes in working
        #   directory)
        #
        #   modified:   testfile.txt
        #
        no changes added to commit (use "git add" and/or "git commit -a")

   .. *git* is saying that the file *testfile.txt* is modified but that no
      files have been **staged** for this commit.

   *git* estña diciendo que el archivo *textfile* está modificado pero que ningún
   archivo ha sido **establecido** para este registro.

   .. If you are used to Mercurial, *git* has an extra level of complexity (but
      also flexibility):  you can choose which modified files will be included
      in the next commit.  Since we only have one file, there will not be a
      commit unless we add this to the .:**index** of files staged for the next
      commit::

   Si estás acostumbrado a Mercurial, *git* tiene un nivel extra de complejidad
   (pero también flexibilidad): puedes elegir que archivos modificados serán incluidos
   en el próximo registro. Como sólo tenenemos un archivo, no habrá un registro a menos
   que lo añadamos al **índice** de archivos **establecidos** para el próximo registro::

        $ git add testfile.txt 

   .. Note that the status is now::

   Nota que el estado ahora es::

        $ git status -s
        M  testfile.txt

   .. This is different in a subtle way from what we saw before: The *M* is 
      in the first column rather than the second, meaning it has been both
      modified and staged.

   Esto es diferente de una forma sutil a lo que hemos hecho antes: La *M* está
   en la primera columna en lugar de en la segunda, esto significa que ambos han
   sido modificados y establecidos.

   .. We can get more information if we leave off the *-s* flag::

   Podemos obtenner más información si no usamos la bandera *-s*::

        $ git status

        # On branch master
        # Changes to be committed:
        #   (use "git reset HEAD <file>..." to unstage)
        #
        #   modified:   testfile.txt
        #

   .. Now *testfile.txt* is on the index of files staged for the next commit.

   Ahora *textfile.txt* está en el índice de archivos establecidos para el próximo registro.

   .. Now we can do the commit::

   Ahora hacemos el registro::

        $ git commit -m "added a third line to the test file"

        [master 51918d7] added a third line to the test file
         1 file changed, 1 insertion(+)

   .. Try doing *git log* now and you should see something like::

   Prueba a hacer *git log* ahora y deberías ver algo como::

        commit 51918d7ea4a63da6ab42b3c03f661cbc1a560815
        Author: Randy LeVeque <rjl@ned>
        Date:   Tue Mar 5 18:11:34 2013 -0800

            added a third line to the test file

        commit 28a4da5a0deb04b32a0f2fd08f78e43d6bd9e9dd
        Author: Randy LeVeque <rjl@ned>
        Date:   Tue Mar 5 17:44:22 2013 -0800

            My first commit of a test file.

   .. If you want to revert your working directory back to the first snapshot 
      you could do::

   Si quieres que el directorio de trabajo vuelva a la primera instantánea, podrías hacer::

        $ git checkout 28a4da5a0de
        Note: checking out '28a4da5a0de'.

   .. You are in 'detached HEAD' state. You can look around, make experimental
      changes and commit them, and you can discard any commits you make in this
      state without impacting any branches by performing another checkout.

   Estás ahora en el estado 'detached HEASD'. Puedes ojear, hacer cambios experimentales y
   registrarlos, y puedes descartar cualquier registro que hagas en este estado sin ningún
   impacto en ninguna rama realizando otro checkout.

        HEAD is now at 28a4da5... My first commit of a test file.

   .. Take a look at the file, it should be back to the state with only two
      lines.  

   Echa un vistazo al archivo, deberías poder volver a este estado con solo dos líneas.

   .. Note that you don't need the full SHA-1 hash code, the first few digits
      are enough to uniquely identify it.

   Nota que no necesitas el código SHA-1 hash completo, los primeros dígitos son
   suficiente para identificarlo de forma única.

   .. You can go back to the most recent version with::
   
   Puedes volver a la versión más reciente con::

        $ git checkout master
        Switched to branch 'master'

   .. We won't discuss branches, but unless you create a new branch, the
      default name for your main branch is *master* and this *checkout* command 
      just goes back to the most recent commit.

   No hablaremos sobre ramas, pero a menos que crees una nueva rama, el nombre por defecto
   de la rama principal es *master* y el comando *checkout* solo vuelve al registro más reciente.
  

   .. #. So far you have been using *git* to keep track of changes in your own
      directory, on your computer.  None of these changes have been seen by
      Bitbucket, so if someone else cloned your repository from there, they
      would not see *testfile.txt*.

#. Hasta ahora has estado usando *git* para seguir el rasro de los cambios en tu propio
   directorio, en tu ordenador. Ninguno de estos cambos han sido vistos por Bitbucket, así que
   si alguien clona tu repositorio de allí, no verán *testfile.txt*.

   .. Now let's *push* these changes back to the Bitbucket repository::

   Ahora, usemos *push* para enviar los cambios al repositorio de Bitbucket::

   .. First do

   Primero hacemos::

        $ git status

   .. to make sure there are no changes that have not been committed.  This
      should print nothing.

    para asegurarnos que no haya cambios sin registrar. Esto no debería imprimir nada.

   .. Now do::

   Ahora haz::

        $ git push -u origin master

   .. This will prompt for your Bitbucket password and should then print
      something indicating that it has uploaded these two commits to
      your bitbucket repository. 

   Esto debería primero pedir tu contraseña de Bitbucket y entonces debería imprimir algo
   indicando que que se han actualizado estos dos registros a tu repositorio de bitbucket.
  

   .. Not only has it copied the 1 file over, it has added both changesets, so
      the entire history of your commits is now stored in the repository.  If
      someone else clones the repository, they get the entire commit history
      and could revert to any previous version, for example.

   No solo ha copiado un archivo, sino que ha añadido los cambio, así que el historial completo
   de tus registros está ahora almacenado en el reposiatorio. Si alguién clona el repositorio, obtiene
   el historial completo de registros y puede volver a cualquier versión anterior, por ejemplo.

   .. To push future commits to bitbucket, you should only need to do::

   Para subir futuros registros a bitbucket, solo necesitas hacer::

        $ git push

   .. and by default it will push your master branch (the only branch you
      have, probably) to `origin`, which is the shorthand name for the 
      place you originally cloned the repository from.  To see where this
      actually points to::

   y por defecto se subirán a la rama master (la única rama que tengas, probablemente) de
   `origin`, que es la abreviatura del nombre del lugar del que clonaste el repositorio. Para
   ver a donde apunta::

        $ git remote -v

   .. This lists all `remotes`.  
      By default there is only one, the place you cloned the repository from.
      (Or none if you had created a new repository using `git init` rather
      than cloning an existing one.)  

   Esto enumera todos los `remotes`. Por defecto solo hay uno, el lugar de donde
   copiaste el repositorio. (O ninguno si has creado un nuevo repositorio usando `git init`
   en lugar de clonar uno existente.)

   .. #. Check that the file is in your Bitbucket repository:  Go back to that web
      page for your repository and click on the  "Source" tab at the top.  It
      should display the files in your repository and show *testfile.txt*.

#. Comprueba que el archivo está en tu repositorio de Bitbucket: Vuelve a la página web
   de tu repositorio y haz click en la pestaña "Source" en la parte superior. Debería enseñar
   los archivos en tu repositorio y mostrar *testfile.txt*.

   .. Now click on the "Commits" tab at the top.  It should show that you
      made two commits and display the comments you added with the *-m* flag
      with each commit.

   Ahora haz click en la pestaña "Commits" arriba. Debería mostrar que has hecho
   dos registros y enseñar los comentarios que añadistes con la bandera *-m* con
   cada registro.

   .. If you click on the hex-string for a commit, it will show the 
      *change set* for this commit.  What you
      should see is the file in its final state, with three lines.  The third
      line should be highlighted in green, indicating that this line was added
      in this changeset.  A line highlighted in red would indicate a line deleted
      in this changeset.  (See also :ref:`bitbucket`.)

   Si haces click en la cadena hexadecimal de un registro, te mostrara el *conjunto de cambios*
   de ese registro. Lo que deberías ver es el archivo en su estado final, con tres líneas. La tercera
   debería estar marcada en verde indicando que esta línea fue añadida en estos cambios. Una línea marcada
   en rojo indicaría que la línea fue borrada con esos cambios. (Ver tambien :ref:`bitbucket`.)
 
.. This is enough for now!  

¡Esto es suficiente por ahora!

.. :ref:`homework1` instructs you to add some additional files to the Bitbucket
   repository.

:ref:`homework1` tiene más instrucciones para añadir algunos archivos adicionales al repositorio de Bitbucket.

.. Feel free to experiment further with your repository at this point.

Siente libre de experimentar más con tu repositorio en este punto.

.. Further reading
.. ---------------

Más lecturas
------------

.. Next see :ref:`bitbucket` and/or :ref:`git_more`.

A continuación mira :ref:`bitbucket` y/o :ref:`git_more`.

.. Remember that you can get help with *git* commands by typing, e.g.::

Recuerda que puedes obtener ayuda con los comandos de *git* escribiendo por ejemplo::

        $ git help
        $ git help diff  # or any other specific command name

.. Each command has lots of options!

¡Cada comando tiene muchas opciones!

.. :ref:`biblio_git` contains references to other sources of information and
   tutorials.

:ref:`biblio_git` contiene referencias a otras fuentes de informaciń y tutoriales.

