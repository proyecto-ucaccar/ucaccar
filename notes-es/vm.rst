
.. _vm:

.. =============================================================
   Virtual Machine for this class [2014 Edition]
   =============================================================

==================================================
La Máquina Virtual para esta clase [Edición 2014]
==================================================

.. We are using a wide variety of software in this class, much of which is
   probably not found on your computer.  It is all open source software (see
   :ref:`licences`) and links/instructions
   can be found in the section :ref:`software_installation`.

Estamos usando una amplia variedad de software en esta clase, muchos de los cuales probablemente
no se hallen en vuestros ordenadores. Todos son software de código abierto
(ver :ref:`licences`) y los enlaces/instrucciones pueden ser encontrados en la sección :ref:`software_installation`.

.. An alternative, which many will find more convenient, is to download and
   install the [VirtualBox]_ software and then download a Virtual Machine (VM)
   that has been built specifically for this course.  VirtualBox will run this
   machine, which will emulate a specific version of Linux that already has
   installed all of the software packages that will be used in this course.

Una alternativa que muchos encontrarán más conveniente, es descargar e instalar el software
[VirtualBox]_ y entonces descargar la Máquina Virtual (VM) que ha sido construida específicamente
para este curso, VirtualBox correrá en tu máquina, lo que emulara una versión específica de Linux
que ya tiene instalada todo los paquetes de software que se usarán en este curso.

.. You can find the VM on the `class 
   webpage <http://faculty.washington.edu/rjl/classes/am583s2014/>`_
   in the file `uwhpsc.zip
   <http://faculty.washington.edu/rjl/classes/am583s2014/uwhpsc.zip>`_.

Puedes encontrar la VM en `la página de la clase <http://faculty.washington.edu/rjl/classes/am583s2014/>`_
en el archivo `uwhpsc.zip
<http://faculty.washington.edu/rjl/classes/am583s2014/uwhpsc.zip>`_.

.. Note that the file is quite
   large (approximately 803 MB compressed), and if possible you should
   download it from on-campus to shorten the download time.  The TA's will also
   have the VM on memory sticks for transferring.

Nota que el archivo es bastante grande (aproximadamente 803 MB comprimido), y si es posible deberías
instalarlo en el campus para un tiempo de descarga menor. El TA también tendrá la VM en un pendrive para
pasarlo.

.. System requirements
   -------------------

Requerimientos del sistema
---------------------------

.. The VM is around 2 GB in size, uncompressed, and the virtual disk
   image may expand to up to 8 GB, depending on how much data you store
   in the VM.  Make sure you have enough free space available before
   installing.  You can set how much RAM is available to the VM when
   configuring it, but it is recommended that you give it at least 512
   MB; since your computer must host your own operating system at the
   same time, it is recommended that you have at least 1 GB of total RAM.

La VM pesa aproximadamente 2 GB, descomprimida, y el disco virtual puede expandirse hasta 8 GB, dependiendo
de cuantos datos almacenes en ella. Asegúrate que tienes suficiente espacio libre antes de instalarla. Puedes
establecer cuanta memoria RAM dejas disponible para la VM cuando la configuras, pero se recomienda que 
le des al menos 512 MB; ya que tu ordenador debe mantener a tu propio sistema operativo al mismo tiempo,
se recomienda que al menos tenga 1GB de RAM total.

.. Setting up the VM in VirtualBox
   -------------------------------

Configurando la VM  en VirtualBox
---------------------------------

.. Once you have downloaded and uncompressed the virtual machine disk
   image from the class web site, you can set it up in VirtualBox, by
   doing the following:

Una vez que hallas descargado y descomprimido la imagen de disco de la máquina virtual
del sitio web de la clase, puedes configurarla en VirtualBox, haciendo lo siguiente:

   .. #. Start VirtualBox

#. Enciende VirtualBox

   .. #. Click the *New* button near the upper-left corner

#. Haz click en el botón *New* cerca de la esquina superior-izquierda

   .. #. Click *Next* at the starting page

#. Haz click en *Next* en la página que sale

   .. #. Enter a name for the VM (put in whatever you like); for *OS Type*,
         select "Linux", and for *Version*, select "Ubuntu".  Click *Next*.

#. Introduce un nombre para la VM (pon lo que quieras); en *OS Type* selecciona
   "Linux", y para *Version*, selecciona "Ubuntu". Haz click en *Next*. 

   .. #. Enter the amount of memory to give the VM, in megabytes.  
         512 MB is the recommended minimum.  Click *Next*.

#. Introduce la cantidad de memoria que le darás a la VM en megabytes. 512 MB es el mínimo
   recomendado. Haz click en *Next*.
 
   .. #. Click *Use existing hard disk*, then click the folder icon next to
         the disk list.  In the Virtual Media Manager that appears, click
         *Add*, then select the virtual machine disk image you downloaded
         from the class web site.  Ignore the message about the recommended
         size of the boot disk, and leave the box labeled "Boot Hard Disk
         (Primary Master)" checked.  Once you have selected the disk image,
         click *Next*.

#. Haz cick en *Use existing hard drive*, luego haz click en en el icono de la carpeta al
   lado de la lista de discos. En el Virtual Media Manager que aparece, haz click en *Add*,
   entonces selecciona el disco imagen de la máquina virtual que descargaste del sitio web de
   la clase. Ignora el mensaje sobre el tamaño recomendado del y disco de arranque, deja marcada
   el cuadro "Boot Hard Disk (Primary Master)". Una vez que hallas seleccionado el disco imagen,
   haz click en *Next*.

   .. #. Review the summary VirtualBox gives you, then click *Finish*.  Your
         new virtual machine should appear on the left side of the VirtualBox
         window.

#. Echa un ojo al resumen que VirtualBox te da, entonces haz clik en *Finish*. Tu nueva máquina
   virtual debería aparecen en la izquierda de la pantalla de VirtualBox.

.. Starting the VM
   ---------------

Iniciando la VM
-----------------

.. Once you have configured the VM in VirtualBox, you can start it by
   double-clicking it in the list of VM's on your system.  The virtual
   machine will take a little time to start up; as it does, VirtualBox
   will display a few messages explaining about mouse pointer and
   keyboard capturing, which you should read.

Una vez que hallas configurada la VM en VirtualBox, puedes iniciala haciendo doble
click en la lista de VMs de tu sistema. La máquina virtual tardará un tiempo corto
en arracar, y cuando lo haga, VirtualBox mostrará algunos mensajes sobre el puntero del ratón
y la captura del teclado, que deberías leer.

.. After the VM has finished booting, it will present you with a login
   screen; the login and password are both ``uwhpsc``.  (We would have
   liked to set up a VM with no password, but many things in Linux assume
   you have one.)

Después de que la VM acabe de arrancar, se te presentará una pantalla  de inicio; el login
y la contraseña son ambas ``uwhpsc``. (Nos hubiera gustado tener la VM sin contraseñas, pero para muchas
cosas Linux asume que tienes una.)

.. Note that you will also need this password to quit the VM.

Nota que necesitarás esta contraseña para dejar la VM.

.. Running programs
   ----------------

Ejecutando programas
---------------------

.. You can access the programs on the virtual machine through the Applications
   Menu (the mouse on an *X* symbol in the upper-left corner of the
   screen), or by clicking the quick-launch icons next to the menu
   button.  By default, you will have quick-launch icons for a command
   prompt window (also known as a *terminal window*), a text editor, and
   a web browser.  After logging in for the first time, you should start
   the web browser to make sure your network connection is working.

Puedes acceder a los programas de la máquina virtual a través del Menu de Aplicaciones
(el ratón con el símbolo *X* en la esquina superior izquierda de la pantalla), o haciendo
click en los iconos de inicio rápido del botón del menú. Por defecto, tendremos el icono de inicio
rápido de una ventana de consola (también conocida como *ventana de terminal*), un editor de texto, y
un navegador web. Después de entrar la primera vez, deberías inicar el navegador web para asegurarte
que tu conexión de red está funcionando.

.. Fixing networking issues
   ------------------------

.. When a Linux VM is moved to a new computer, it sometimes doesn't
   realize that the previous computer's network adaptor is no longer
   available.  

Cuando una VM de Linux se mueve a un nuevo ordenador, a veces no se da cuenta que
el adaptador de red del ordenador anterior ya no está disponible.

.. Also, if you move your computer from one wireless network to another while
   the VM is running, it may lose connection with the internet.  

También, si trasladas el ordenador de una red inalámbrica a otra mientras la VM
está funcionando, puede perder la conexión con internet.

.. If this happens, it should be sufficient to shut down the VM (with the 0/1
   button on the top right corner) and then restart it.
   On shutdown, a script is automatically run that does the following, which in
   earlier iterations of the VM had to be done manually...

Si esto sucede, debería ser suficiente apagar la VM (con el botón 0/1 en la esquina superior
derecha) y entonces reiniciarla. Al apagarse, un script se ejecuta automáticamente que hace lo siguiente, lo que en versiones anteriores de la VM tenía que hacerse manualmente...

 $ sudo rm /etc/udev/rules.d/70-persistent-net.rules

.. This will remove the incorrect settings; Linux should then autodetect
   and correctly configure the network interface it boots.  

Esto eliminará las configuraciones incorrectas; Linux debería entonces autodetectar y corregir
la configuración de la interfaz de red.

.. Shutting down
   -------------

Apagando
---------

.. When you are done using the virtual machine, you can shut it down by
   clicking the 0/1 button on the top-right corner of the VM.
   You will need the password `uwhpsc`.

Cuando hallas acabado con la máquina virtual, tu puedes apagarla haciendo click en el botón
0/1 en la esquina superior derecha de la VM. Necesitarás la contraseña `uwhpsc`.

.. Cutting and pasting
   -------------------

Copiando y pegando
------------------

.. If you want to cut text from one window in the VM and paste it into another,
   you should be able to highlight the text and then type ctrl-c (or in a
   terminal window, ctrl-shift-C, since ctrl-c is the interrupt signal). To
   paste, type ctrl-v (or ctrl-shift-V in a terminal window).

Si quieres cortar texto de una ventana de la VM y pegarlo en otra, deberías poder hacerlo
marcando el texto y entonces pulsando ctrl-c (o una terminal, ctrl-shift-C, ya que ctrl-c es
señal de interrumpir). Para pegar, presiona ctrl-c (o ctrl-shift-V en una terminal).

.. If you want to be able to cut and paste between a window in the VM and a
   window on your host machine, click on Machine from the main VitualBox menu
   (or `Settings` in the Oracle VM VirtualBox Manager window), then click on
   `General` and then `Advanced`.  Select `Bidirectional` from the `Shared
   Clipboard` menu.

Si quieres ser capaz de copiar y pegar entre una ventana en la VM y una ventana de tu propio
ordenador, haz click en la máquina del menú principal de VirtualBox (o en `Settings` en la ventana Oracle VM VirtualBox Manager), luego haz click en `General` y entonces `Advanced`. Selecciona `Bidirectional` del menú `Shared Clipboard`.

.. Shared Folders
   --------------

Carpetas Compartidas
---------------------

.. If you create a file on the VM that you want to move to the file system of
   the host machine, or vice versa, you can create a "shared folder" that is
   seen by both.  

Si creas un archivo en la VM que quieres mover al sistema de archivos de tu máquina anfitriona,
o vicersa, puedes crear una "carpeta compartida" que sea visible en ambos.

.. First create a folder (i.e. directory) on the host machine, e.g. via::

Primero crea una carpeta (esto es, un directorio) en tu máquina anfitriona, por ejemplo, via::

    $ mkdir ~/uwhpsc_shared

..  This creates a new subdirectory in your home directory on the host machine. 

Esto crea un nuevo subdirectorio en tu directorio home en la máquina anfitriona.

.. In the VirtualBox menu click on `Devices`, then click on
   `Shared Folders`.  Click the + button on the right side and then type in the
   full path to the folder you want to share under `Folder Path`, including the
   folder name, and then the folder name itself under `Folder name`.  
   If you click on `Auto-mount` then this will be mounted every time you start
   the VM.  

En el menú de VirtualBox haz click en `Devices`, luego haz click en `Shared Folders`. Haz
click en el botón + en el lado derecho y entonces escribe la ruta completa de la carpeta
que quieras compartir bajo `Folder Path`, incluyendo el nombre de la carpeta, y entonces el nombre
de la propia carpeta bajo `Folder name`. Si haces click en `Auto-mount` entonces estará montado cada vez
que arranques la VM.

.. Then click `OK` twice.  

Entonces haz click en `OK` dos veces.

.. Then, in the VM (at the linux prompt), type the following commands::

Entonces, en la VM (en el prompt de linux), escribe los siguientes comandos::

    sharename=uwhpsc_shared   # or whatever name the folder has
    sudo mkdir /mnt/$sharename 
    sudo chmod 777 /mnt/$sharename 
    sudo mount -t vboxsf -o uid=1000,gid=1000 $sharename /mnt/$sharename 

.. You may need the password `uwhpsc` for the first `sudo` command.

Puedes necesitar la contraseña `uwhpsc` para el primer comando `sudo`.

.. The folder should now be found in the VM in `/mnt/$sharename`.
   (Note `$sharename` is a variable set in the first command above.)

La carpeta debería poder encontrarse en la VM en `/mnt/$sharename`. (Nota que
`$sharename` es una variable establecida en el primer comando de arriba).

.. If auto-mounting doesn't work properly, you may need to repeat the final
   `sudo mount ...` command  each time you start the VM.  

Si el auto-montado no funciona propiamente, podrías necesitar repetir el comando final
`sudo mount ...` cada vez que arranques la VM.

.. Enabling more processors
   ------------------------

Activando más procesadores
---------------------------

.. If you have a reasonably new computer with a multi-core
   processor and want to be able to run parallel programs across multiple
   cores, you can tell VirtualBox to allow the VM to use additional
   cores.  To do this, open the VirtualBox
   *Settings*.  Under *System*, click the *Processor*
   tab, then use the slider to set the number of processors the VM will
   see.  Note that some older multi-core processors do not support the
   necessary extensions for this, and on these machines you will only be
   able to run the VM on a single core.

Si tienes un ordenador razonablemente nuevo con un procesador multi-núcleo y quieres
ser capaz de ejecutar programas en paralelo usando varios núcleos, puedes decirle a VirtualBox
que permita a la VM que use núcleos adicionales. Para hacer esto, abre *Settings* en VirtualBox. Bajo
*System*, haz click en la pestaña *Processor*, y entonces usa la barra deslizante para establecer
el número de procesadores que la VM verá. Nota que algunos procesadores multi-núcleos antiguos no
soportarán extensiones para esto, en estas máquinas solo serás capaz de ejecutar la VM en un solo núcleo.


.. Problems enabling multiple processors...
   ------------------------

Problemas al usar varios procesadores...
-----------------------------------------

.. Users may encounter several problems with enabling mutliple processors. Some users may not
   be able to change this setting (it will be greyed out). Other users when may find no improved 
   performance after enabling multiple processors. Still others may encounter an error such as::

Los usuarios pueden encontrarse con varios problemas al habilitar varios procesadores. Algunos usuarios
quizás no puedan cambiar esta configuración (estará en gris). Otros usuarios quizás no encuentren
mejora en el rendimiento al usar varios procesadores. Otros pueden encontrar un error como::

    VD: error VERR_NOT_SUPPORTED

.. All of these problems indicate that virtualization has not been enabled on your processors. 

Todos estos problemas indican que la virtualización no ha sido realizada en tus procesadores.

.. Fortunately this has an easy fix. You just have to enable virtualization in your BIOS
   settings. 

Afortunadamente esto tiene fácil arreglo. Solo debes desbloquear la virtualización en los ajustes de tu BIOS.

.. 1. To  access the BIOS settings you must restart your computer and press a certain
   button on startup. This button will depend on the company that manufactures your computer 
   (for example for Lenovo's it appears to be the f1 key).

1. Para acceder a la ajuste de la BIOS debes reiniciar tu ordenador y presionar cierta tecla al arrancar.
   Este botón depende de la compañia que fabricara tu ordenador. (por ejemplo para Lenovo es la tecla f1).

.. 2. Next you must locate a setting that will refer to either virtualization, VT, or VT-x. 
   Again the exact specifications will depend on the computer's manufacturer, however
   it should be found in the Security section (or the Performance section if you are using a Dell). 

2. A continuación debes localizar un ajuste que se refiera o a la virtualización, a VT o a VT-x.
   De nuevo las especificaciones exactas dependen del fabricante del ordenador, sin embargo debería
   encontrarse en la sección Security (o en la sección Performance si estás usando un Dell).

.. 3. Enable this setting,
   then save and exit the bios settings.After your computer reboots you should be able to start the VM using
   multiple processors now. 

3. Permite este ajuste, entonces guarda y sal de los ajustes de la bio. Después de que tu ordenador se
   reinicie deberías poder arrancar la VM con multiples procesadores.

.. 4. If your BIOS does not have any settings like this it is possible that your BIOS is set up to hide this
   option from you, and you
   may need to follow the advice here: http://mathy.vanvoorden.be/blog/2010/01/enable-vt-x-on-dell-laptop/

4. Si tu BIOS no tiene ningún ajuste como este es posible que tu BIOS esté preparada para esconderte esta
   opción, y quizás necesites seguir siguiente consejo:
   http://mathy.vanvoorden.be/blog/2010/01/enable-vt-x-on-dell-laptop/

.. Note: Unfortunately some older hardware does not support virtualization, and so if these solutions don't
   work for you it may
   be that this is the case for your processors. There also may be other possible problems... so don't be
   afraid to ask the TAs for help! 

Nota: Desafortunadamente algunos hardwares viejos no soporta la virtualización, así que si estas soluciones
no te funcionan puede ser que este sea el caso de tus procesadores. Puede haber también otros problemas...
así que no tengas miedo de pedir ayuda al TAs.

.. Changing guest resolution/VM window size
   ----------------------------------------

Cambiando la resolución/tamaño de la ventana de la VM
------------------------------------------------------

.. seealso:: 
   The section :ref:`vm_additions`, which makes this easier.

.. It's possible that the size of the VM's window may be too large for
   your display; resizing it in the normal way will result in not all of
   the VM desktop being displayed, which may not be the ideal way to
   work.  Alternately, if you are working on a high-resolution display,
   you may want to *increase* the size of the VM's desktop to take
   advantage of it.  In either case, you can change the VM's display size
   by going to the Applications menu in the upper-left corner, pointing to
   *Settings*, then clicking *Display*.  Choose a resolution from the
   drop-down list, then click *Apply*.

Es posible que el tamaño de la ventana de la VM ser demasiado grande para tu pantalla;
rescalándola de la manera normal pude provocar que no todo el escritorio de la máquina virtual sea
mostrado, lo cual puede que no sea lo ideal para tu trabajo. De forma alternativa, si estás trabajando
en una pantalla de alta resolución, puedes querer *incrementar* el tamaño del escritorio de la VM para
tener ventaja de esto. En ambos casos, puedes cambiar el tamaño de la pantalla de la VM en el menú Applications en la esquina superior derecha, señalando *Settings*, y luego haciendo click en *Display*.
Elige una resolució de la lista desplegable, y entonces haz click en *Apply*.

.. Setting the host key
   --------------------

Configurando tecla anfitriona
------------------------------

.. seealso:: 
   The section :ref:`vm_additions`, which makes this easier.

.. When you click on the VM window, it will capture your mouse and future mouse
   actions will apply to the windows in the VM.  To uncapture the mouse you
   need to hit some control key, called the *host key*.  It should give you a
   message about this.  If it says the host key is Right Control, for example,
   that means the Control key on the right side of your keyboard (it does *not*
   mean to click the right mouse button).

Cuando haces click en la ventana de la VM, capturará tu ratón y las futuras acciones del ratón
se aplicarán a la ventana de la VM. Para liberar el ratón necesitas pulsar una tecla de control,
llamada *tecla anfitriona*. Deberían darte algún mensaje acerca de esto. Si dice que la tecla anfitriona
es el Control Derecho, por ejemplo, significa la tecla Control situada en el lado derecho del teclado
(no significa hacer click con el botón derecho del ratón).

.. On some systems, the host key that transfers input focus between the
   VM and the host operating system may be a key that you want to use in
   the VM for other purposes.  To fix this, you can
   change the host key in VirtualBox.  In the main VirtualBox window (not
   the VM's window; in fact, the VM doesn't need to be running to do
   this), go to the *File* menu, then click *Settings*.  Under *Input*,
   click the box marked "Host Key", then press the key you want to use.

En algunos sistemas, la tecla anfitriona que transfiere la entrada entre la VM y el sistema operativo
anfitrión puede ser una tecla que quieras usar en tu sistema operativo para otros propósitos. Para
arreglar esto, puedes cambiar la tecla anfitriona en VirtualBox. En la ventana principal de VirtualBox
(no en la ventana de la VM; de hecho, la VM no necesita estar encendida para hacer esto), ve al menú *File*,
entonces haz click en *Settings*. Bajo *Input*, haz click en la caja marcada como "Host Key", entonces
presiona la tecla que quieras usar.

.. _vm_additions:

.. Guest Additions
.. ---------------

Adición de huéspedes
---------------------

.. While we have installed the VirtualBox guest additions on the class
   VM, the guest additions sometimes stop working when the VM is moved to
   a different computer, so you may need to reinstall them.
   Do the following so that the VM will automatically capture and uncapture
   your mouse depending on whether you click in the VM window or outside it,
   and to make it easier to resize the VM window to fit your display.

Mientras que hemos instalado la adición de huéspedes en VirtualBox, esta pueden dejar de funcionar
cuando la VM se mueve a un ordenador diferente, así que puedes que necesitar reinstalarla. Haz lo
siguiente para que que la VM capture y libere automáticamente el ratón dependiendo de si haces click
en la ventana de la VM o fuera de ella, y para hacer más fácil el rescalamiento de la ventana de la VM
para que se ajuste a tu pantalla.

   .. 1. Boot the VM, and log in.

   1. Enciende la VM, y entra.

   .. 2. In the VirtualBox menu bar on your host system, select Devices -->
      Install Guest Additions...  (Note: click on the window for the class
      VM itself to get this menu, not on the main "Sun VirtualBox" window.)

   2. En la barra de menú de VirtualBox de tu sisema anfitrión, selecciona Devices --> Install
      Guest Additions... (Nota: haz click en la ventana de la VM de la clase para obtener este menú,
      no en la ventana principal de "Sun VirtualBox").

   .. 3. A CD drive should appear on the VM's desktop, along with a popup
      window.  (If it doesn't, see the additional instructions below.)
      Select "Allow Auto-Run" in the popup window.  Then enter the
      password you use to log in.

   3. Un CD debería aperecer en el escritorio de la VM, junto con una ventana emergente. (Si no lo
      hace, mira las instrucciones adicionales de abajo). Selecciona "Allow Auto-Run" en la ventana. Entonces
      introduce la contraseña que usas para entrar.

   .. 4. The Guest Additions will begin to install, and a window will appear,
      displaying the progress of the installation.  When the installation is done,
      the window will tell you to press 'Enter' to close it.

   4. Guest Additions se comenzará a instalar, y una ventana aparecerá, mostrando el progreso de
      instalación. Cuando la instalación esté hecha, la ventana te dirá que presiones 'Enter' para cerrarla.

   .. 5. Right-click the CD drive on the desktop, and select 'Eject'.

   5. Haz click derecho en el CD del escritorio, y seleciona 'Eject'.

   .. 6. Restart the VM.

   6. Reinicia la VM.

.. If step 3 doesn't work the first time, you might need to:

Si el paso 3 no funciona a la primera, puedes necesitar:

   .. Alternative Step 3:
    
   Paso 3 alternativo:

   ..  #. Reboot the VM.
   
   #. Arranque la VM.

      ..  #. Mount the CD image by right-clicking the CD drive icon, and clicking
          'Mount'.

   #. Monta la imagen de CD haciendo click derecho en el icono del CD, y haz click en 'Mount'.

      .. #. Double click the CD image to open it.
   
   #. Haz doble click en la imagen del CD para abirlo.

      .. #. Double click 'autorun.sh'.

   #. Haz doble click en 'autorun.sh'.

      .. #. Enter the VM password to install. 

   #. Introduce la contraseña de la VM para instalarlo.

.. How This Virtual Machine was made
   -----------------------------------

Como se ha hecho esta Máquina Virtual
--------------------------------------

      .. 1. Download Ubuntu 12.04 PC (Intel x86) alternate install ISO from 
            http://cdimage.ubuntu.com/xubuntu/releases/12.04.2/release/xubuntu-12.04.2-alternate-i386.iso

   1. Descarga Ubuntu 12.04 PC (Intel x86) ISO de instalación alternativa de 
      http://cdimage.ubuntu.com/xubuntu/releases/12.04.2/release/xubuntu-12.04.2-alternate-i386.iso

      .. 2. Create a new virtual box

   2. Crea una nueva máquina virtual

      .. 3. Set the system as Ubuntu

   3. Establece el sistema como Ubuntu

      .. 4. Use defualt options

   4. Usa opciones predeterminadas

      .. 5. After that double click on your new virtual machine...a dropdown
            box should appear where you can select
            your ubuntu iso

   5. Después de hacer doble click en tu nueva máquina virtual... un desplegable deberia aparecer
      donde puedas seleccionar tu imagen iso.

      .. 6. As you are installing...at the first menu hit F4 and install a 
            command line system

   6. Cuando estes instalando... en el primer menú presiona F4 e instala un sistema de línea de comandos

      .. 7. Let the install proceed following the instructions as given. On most
            options the default answer will be appropriate. 
            W hen it comes time to format the hard drive, choose the manual option.
            Format all the free space and set it as the mount
            point. From the next list choose root (you dont need a swap space). 

   7. Instálalo según las instrucciones dadas. Para la mayoría de las opciones la opción predeterminada
      será apropiada. Cuando llegue la hora de formatear el disco duro, elige la opción manual. Formatea todo
      el espacio
      libre y establecelo como punto de montado. De la siguiente lista elige root (no necesitas intercambiar
      espacio).

      .. 8. Install the necessary packages

   8. Instala los paquetes necesarios.

       .. literalinclude:: install.sh

      .. 9. To setup the login screen edit the file Xresources so that the 
            greeting line says.::

   9. Para configurar la pantalla de inicio edita el archivo Xresource así la línea de bienvenida dice.::

	xlogin*greeting: Login and Password are uwhpsc

      .. 10. Create the file uwhpscvm-shutdown.::

   10. Crea el archivo uwhpscvm-shutdown.::

       .. literalinclude:: uwhpscvm-shutdown

      .. 11. Save it at.::
   
   11. Guárdalo como.:: 

	/usr/local/bin/uwhpscvm-shutdown

      .. 12. Execute the following command command.::

   12. Ejecuta el siguiente comando.::

	$ sudo chmod +x /usr/local/bin/uwhpscvm-shutdown

      .. 13. Right click on the upper panel and select add new items and choose
             to add a new launcher.

   13. Haz click derecho en el panel superior, selecciona añadir nuevos items y elige añadir un
       un nuevo lanzador.

      .. 14. Name the new launcher something like shutdown and in the command 
             blank copy the following line.::

   14. Llama al nuevo lanzador algo así como apagar y en el comando en blanco copia la siguiente línea.::

	 gksudo /usr/local/bin/uwhpscvm-shutdown

      .. 15. Go to preferred applications and select Thunar for file managment 
          and the xfce4 terminal.

   15. Ve a las aplicaciones preferidas y selecciona Thuna para la gestión de archivos y la termina xfce4.

      .. 16. Run jockey-gtk and install guest-additions.

   16. Ejecuta jocket-gtk e instala guest-additions.

      .. 17. Go to Applications then Settings then screensaver and select 
             "disable screen saver" mode

   17. Ve a Apliactions, luego a Settings luego protector de pantalla y seleciona el modo "disable
       screen saver".

      .. 18. In the settings menu select the general settings and hit the advanced
             tab. Here you can set the clipboard and drag
             and drop to allow Host To Guest.

   18. En el menú de configuración selecciona la configuración general y haz click en la pestaña avanzada.
       Aquí puedes establecer el portapapeles y arrastrar y soltar para permitir Host to Guest. 

      ..  19. Shutdown the machine and then go to the main virtualbox screen.
              Click on the virtualmachine and then hit the settings button.

   19. Apaga la máquin y entonces ve a la pantalla principal de virtualbox. Haz click en la máquina
       virtual y entonces pulsa el botónn de ajustes.

      .. 20. After, in the system settings click on the processor tab. This may let
             you allow the virtual machine to use more than one processor (depending
             on your computer). Choose a setting somewhere in the green section of
             the Processors slider. 

   20. Despues, en el sistema de ajustes haz click en la pestaña de procesadores. Esto puede permitirte
       que la máquina virtual use más de un procesador (dependiendo de tu ordenador). Elige un ajuste
       en alguna parte de la sección verde de la barra Processors.

.. About the VM
   ------------

Acerca de la VM
----------------

.. The class virtual machine is running XUbuntu 12.04, a variant of Ubuntu
   Linux (`<http://www.ubuntu.com>`_), which itself is an offshoot of
   Debian GNU/Linux (`<http://www.debian.org>`_).  XUbuntu is a
   stripped-down, simplified version of Ubuntu suitable for running on
   smaller systems (or virtual machines); it runs the *xfce4* desktop
   environment.

La máquina virtual ejecuta XUbutu 12.04, una variante de Ubunto Linux (`<http://www.ubuntu.com>`_),
que en sí mismo es una rama de Debian GNU/Linux (`<http://www.debian.org>`_). XUbutu es una
versión simplificada de Ubuntu que se puede ejecutar en pequeños sistemas (o en
máquinas virtuales); ejecuta el entorno de escritorio *xfce4*.

.. Further reading
   ---------------

Más lecturas
-------------

[VirtualBox]_

[VirtualBox-documentation]_
