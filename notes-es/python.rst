
.. _python:

=============================================================
Python
=============================================================

.. These notes only scratch the surface of Python, with discussion of a few
.. features of the language that are most important to getting started and to
.. appreciating how Python can be used in computational science.

Estas notas sólo arañan la superficie de Python, contando características del lenguaje que son importantes
para empezar a apreciar cómo Python se puede usar en la ciencia computacional.


.. See the references below or the :ref:`biblio_python` section of the
.. :ref:`biblio` for more detailed references.
Vea la referencia de abajo o :ref:`biblio_python` sección de :ref:`biblio` para referencias detalladas. 
See also the slides from lectures.

.. Interactive Python
.. ------------------

Python interactivo 
------------------

.. The IPython shell is generally recommended for interactive work in Python
.. (see `<http://ipython.org/documentation.html>`_), but for most examples we'll display the >>> prompt of the
.. standard Python shell.

El terminar IPython es generalmente recomendado para trabajo interactivo en Python
(vea  `<http://ipython.org/documentation.html>`_), pero para más ejemplos pondremos >>> en el terminal estandar de Python. 

.. Normally multiline Python statements are best written in a text file rather
.. than typing them at the prompt, but some of the short examples below are
.. done at the prompt. 
Normalmente las secuencias de varias líneas en Python se escriben mejor
en un archivo de texto que en el prompt, pero muchos de los siguientes ejemplos son cortos
y se pueden hacer bien en el prompt.

.. If type a line that Python recognizes as an unfinished block, it will give a line starting with three dots, like::
Si escribe una línea que Python reconoce como un bloque sin terminar, se le dará una línea que comienza con tres puntos, como:

    >>> if 1>2:
    ...    print "oops!"
    ... else:
    ...    print "this is what we expect"
    ... 
    this is what we expect
    >>> 

??????Once done with the full command, typing <return> alone at the ... prompt
tells Python we are done and it executes the command.

.. Indentation
.. -----------

Sangría
-----------

.. Most computer languages have some form of begin-end structure, or
.. opening and closing braces, or some such thing to clearly delinieate
.. what piece of code is in a loop, or in different parts of an
.. if-then-else structure like what's shown above.  
La mayoría de los lenguajes de programación tienen alguna forma de empezar las estructuras inicio-final,
abriendo y cerrando corchetes, o algo parecido para dejar claro qué parte del código está en el bucle, 
o en diferentes partes de una estructura if-then-else como la mostrada arriba.

.. Good programmers generally also indent their code so it is easier for a reader to
.. see what is inside a loop, particularly if there are multiple nested
.. loops.  But in most languages this is indentation is just a matter
.. of style and the begin-end structure of the language determines how it is
.. actually interpreted by the computer.
Buenos programadores generalmente también sangran sus códigos para que el lector vea claramente que hay dentro del bucle,
particularmente si hay múltiples bucles anidados.
Pero en muchos lenguajes la sangría es sólo una cuestión de estilo y la estructura begin-end del lenguahe determina
cómo realmente es interpretado por el ordenador.


.. **In Python, indentation is everything**.  There are no begin-end's, only
.. indentation.  Everything that is supposed to be at one level of a loop must
.. be indented to that level.  Once the loop is done the indentation must go
.. back out to the previous level.  There are some other rules you need to
.. learn, such as that the "else" in and if-else block like the above has to be
.. indented exactly the same as as the "if".  See :ref:`if_else` for more about
.. this.

**En Python, las sangrías son todo**. No hay begin-end, solo sangrías.
Todo lo que se supone que está en un nivel de un bucle debe tener una sangría a ese nivel.
Una vez que el bucle se hace la sangría debe volver al nivel anterior.
Hay algunas otras reglas que necesitas saber, como que "else" en un bloque if-then-else como el de arriba tiene que tener
la misma sangría exactamente que el if.
Vea: :ref:`if_else` para más información sobre esto.


.. How many spaces to indent each level is a matter of style, but you must be
.. consistent within a single code.  The standard is often 4 spaces.
Cuántos espacios para sangrar cada nivel es cuestión de estilo, pero debes ser coherente con un único código.
Lo estándar es 4 espacios.

.. Wrapping lines
.. --------------

¿¿¿¿ Líneas de embalaje 
------------------

.. In Python normally each statement is one line, and there is no need to use
.. separators such as the semicolon used in some languages to end a line.  
.. One the other hand you can use a semicolon to put several short statements on a
.. single line, such as::
En Python normalmente cada declaración es una línea, y no hay necesidad de usar separadores como el punto y coma usado
en algunos lenguajes para acabar una línea.
Por otro lado puede usar el punto y coma para poner varias declaraciones cortas en una misma línea, como:

    >>> x = 5; print x
    5

.. It is easiest to read codes if you avoid this in most cases.
Es más fácil leer códigos si evita esto.

.. If a line of code is too long to fit on a single line, you can break it into
.. multiple lines by putting a backslash at the end of a line::
Si una línea es demasiado larga para caber en una simple línea, se puede romper en múltiples líneas poniendo
una barra invertida al final de la línea.

    >>> y = 3 + \
    ...     4
    >>> y
    7

.. Comments
.. --------

Comentarios
-----------

.. Anything following a # in a line is ignored as a comment (unless of course
.. the # appears in a string)::
Cualquier cosa precedida por # en una línea es ignorada como un comentario (al menos que # aparezca en una cadena)

    >>> s = "This # is part of the string"  # this is a comment
    >>> s
    'This # is part of the string'

.. There is another form of comment, the *docstring*, discussed below following an introduction to strings.
Hay otra forma de hacer comentarios, el *docstring* que veremos después de las cadenas.


.. Strings
.. -------

Cadenas
-------

.. Strings are specified using either single or double quotes::
Las cadenas se especifican mediante comillas simples o dobles:

    >>> s = 'some text'
    >>> s = "some text"

.. are the same.  
.. This is useful if you want strings that themselves contain
.. quotes of a different type.
Esto es útil si quiere cadenas que contengan en sí mismas comillas de un tipo diferente.

.. You can also use triple double quotes, which have the advantage that they
.. such strings can span multiple lines::
También puede usar tres dobles comillas, lo cual tiene la ventaja de que puede poner cadenas de ocupen varias líneas.

    >>> s = """Note that a ' doesn't end
    ... this string and that it spans two lines"""

    >>> s
    "Note that a ' doesn't end\nthis string and that it spans two lines"

    >>> print s
    Note that a ' doesn't end
    this string and that it spans two lines


.. When it prints, the carriage return at the end of the line show up as "\n".
.. This is what is actually stored. When we "print s" it gets printed as a
.. carriage return again.
Cuando se imprime, el retorno de carro al final de la línea se muestran como "\ n".
Esto es lo que se almacena en realidad.
?????Cuando pintamos s 

.. You can put "\n" in your strings as another way to break lines::
Puede poner "\n" en su cadena como forma para romper líneas:

    >>> print "This spans \n two lines"
    This spans 
     two lines

.. See :ref:`python_strings` for more about strings.
Vea :ref:`python_strings` para saber más sobre cadenas.

???Docstrings
----------

.. Often the first thing you will see in a Python script or module, or in a
.. function or class defined in a module, is a brief description that is
.. enclosed in triple quotes.   
Muchas veces lo primero que vea en una secuencia de comandos o módulo de Python, o en una función es una breve descripción que  
está entre comillas triples.

.. Although ordinarily this would just be a
.. string, in this special position it is interpreted by Python as a comment
.. and is not part of the code.  It is called the *docstring* because it is
.. part of the documentation and some Python tools automatically use the
.. docstring in various ways.  See :ref:`ipython` for one example. 
Aunque ordinariamente, esto sería justamente una cadena, en esta especial posición es interpretado por Python como un comentario
y no forma parte del código.
A esto le llamamos *doctring* porque es parte de la documentación y algunas herramientas de Python utilizan automáticamente el
*docstring* de diversas maneras.
Vea: :ref:`ipython` para un ejemplo.

.. Also the documentation formatting program Sphinx that is used to create these class
.. notes can automatically take a Python module and create html or latex
.. documentation for it by using the docstrings, the original purpose for which
.. Sphinx was developed. See :ref:`sphinx` for more about this.

También Sphinx se usa para crear esa clase de notas que puedan automaticamente coger
módulos Pyhton y crear htlm o documento latex usando docstrings, el propósito original 
por el cual fue desarrollado Sphinx.
Vea:ref:`sphinx` para más información sobre esto.

.. It's a good idea to get in the habit of putting a docstring at the top of
.. every Python file and function you write.
Es buena idea tener el hábito de poner un docstring al comienzo de cada archivo Python y en cada función que escriba.

.. Running Python scripts
.. ----------------------

Ejecutar Python scripts
----------------------

.. Most Python programs are written in text files ending with the .py
.. extension.  
.. Some of these are simple *scripts* that are just a set of Python
.. instructions to be executed, the same things you might type at the >>>
.. prompt but collected in a file (which makes it much easier to modify or
.. reuse later).  
.. Such a script can be run at the Unix command
.. line simply by typing "python" followed by the file name.

.. See :ref:`python_scripts_modules` for some examples.
.. The section :ref:`importing_modules`
.. also contains important information on how to "import" modules,
.. and how to set the path of directories that are searched for modules when
.. you try to import a module.

La mayoría de los programas en Python se escriben en ficheros con extensión .py
Muchos de ellos son simples *scripts* que tienen un conjunto de instrucciones de Python para ser ejecutado,
lo mismo que puedes escribir en >>>prompt pero agrupadas en un archivo (lo cual hace más fácil modificar o ser usado más tarde).
Como script puede ser ejecutado en línea de comando Unix escribiendo "python" seguido por el nombre del archivo.

Vea: ref:`python_scripts_modules` para ejemplos.
La sección: ref:`importing_modules`contiene información importante de cómo importar módulos, y la forma de establecer la ruta de 
directorios cuando intenta importar un módulo.

.. _python_objects:

.. Python objects
.. --------------

Objetos Python
--------------

.. Python is an object-oriented language, which just means that virtually
.. everything you encounter in Python (variables, functions, modules, etc.) is
.. an *object* of some *class*.  
.. There are many classes of objects built into
.. Python and in this course we will primarily be using these pre-defined
.. classes. 
.. For large-scale programming projects you would probably define
.. some new classes, which is easy to do.  (Maybe an example to come...)

Python es un lenguaje orientado a objetos, lo cual significa que virtualmente
todo lo que encuentra en Python (variables, funciones, módulos, etc) es un *objeto* de alguna *clase*.
Hay muchas clase de objeto construidas en Python y en este curso usaremos principalmente clases predefinidas.
Para los proyectos de programación a gran escala probablemente definiríamos nuevas clases, lo cual es fácil hacer.
(Un ejemplo para emepezar..)

.. The *type* command can be used to reveal the type of an object::
El tipo de comando puede ser usado para saber el tipo de un objeto::
    >>> import numpy as np
    >>> type(np)
    <type 'module'>

    >>> type(np.pi)
    <type 'float'>

    >>> type(np.cos)
    <type 'numpy.ufunc'>

.. We see that *np* is a module, *np.pi* is a floating point real number, and
.. *np.cos* is of a special class that's defined in the numpy module.
.. The *linspace* command creates a numerical array that is also a special
.. numpy class::
Vemos que *np* es un módulo, *np.pi* es un número real, y *np.cps* es una clase especial que se define en el módulo numpy.
El comando *linespace* crea una matriz numérica que también es una clase especial de numpy.

    >>> x = np.linspace(0, 5, 6)
    >>> x
    array([ 0.,  1.,  2.,  3.,  4.,  5.])
    >>> type(x)
    <type 'numpy.ndarray'>

.. Objects of a particular class generally have certain operations that are
.. defined on them as part of the class definition.  For example, NumPy
.. numerical arrays have a *max* method defined, which we can use on *x* in one
.. of two ways::
Los objetos de una clase determinada generalmente tienen ciertas operaciones que se definen en ellos 
como parte de la definición de clase. 
Por ejemplo, NumPy matrices numéricas tienen un método definido * max *, que podemos utilizar en * x * en una de estas formas::

    >>> np.max(x)
    5.0
    >>> x.max()
    5.0

.. The first way applies the method *max* defined in the *numpy* module to *x*.
.. The second way uses the fact that *x*, by virtue of being of type
.. *numpy.ndarray*, automatically has a *max* method which can be invoked (on
.. itself) by calling the function *x.max()* with no argument.  Which way is
.. better depends in part on what you're doing.
La primera forma se aplica el método * max * definido en el módulo *numpy* a *x*.
La segunda manera utiliza el hecho de que *x*, por ser de tipo *numpy.ndarray*, tiene automáticamente un método *max*
que puede ser llamado (en sí mismo) llamando a la función *x.max()* sin argumento. 
Qué camino es mejor depende de lo que se esté haciendo.

.. Here's another example::
Aquí hay otro ejemplo::

    >>> L = [0, 1, 2]
    >>> type(L)
    <type 'list'>

    >>> L.append(4)
    >>> L
    [0, 1, 2, 4]

.. *L* is a list (a standard Python class) and so has a method *append* that
.. can be used to append an item to the end of the list.  
L es una lista (clase estandar de Python) y por eso tiene un método *append*
que puede ser usado para añadir un elemento al final de lista.

.. Declaring variables?
.. --------------------

Declarar variables
--------------------

.. In many languages, such as Fortran, you must generally declare variables before 
.. you can use them and once you've specified that *x* is a real number, say,
.. that is the only type of things you can store in *x*, and a statement like
.. *x = 'string'* would not be allowed.
En muchos lenguajes, como Fortran, generalmente las variables se declaran antes de usarlas
y una vez especificado que *x* es un número real, solo puedes almacenar en 'x' cosas de ese tipo, 
y no está permitido hacer *x='cadena'*.

.. In Python you don't declare variables, you can just type, for example::
En Python no se declaran variables, tu puedes escribir simplemente, por ejemplo::

    >>> x = 3.4
    >>> 2*x
    6.7999999999999998

    >>> x = 'string'
    >>> 2*x
    'stringstring'

    >>> x = [4, 5, 6]
    >>> 2*x
    [4, 5, 6, 4, 5, 6]


.. Here *x* is first used for a real number, then for a character string, then
.. for a list.  Note, by the way,
.. that multiplication behaves differently for objects of
.. different type (which has been specified as part of the definition of each
.. class of objects).
Aquí *x* es primero usado como un número real, luego como una cadena de caracteres,
y luego como una lista.
Tenga en cuenta que la multiplicación se comporta de diferente forma
con los distintos tipos de objetos (lo cual se ha especificado como parte de la definición de cada clase de objetos).

.. In Fortran if you declare *x* to be a real variable then it sets aside a
.. particular 8 bytes of memory for *x*, enough to hold one floating point
.. number.  There's no way to store 6 characters or a list of 3 integers in
.. these 8 bytes.
En Fortran si declara *x* para ser una variable real entonces resrva 8 bytes de memoria para *x*, suficiente
para un número real.
Pero no hay forma de almacenar 6 caracteres o una lista de 3 elementos en esos 8 bytes.

.. In Python it is often better to think of *x* as simply being a pointer
.. that points to some object.  When you type "x = 3.4" Python creates an
.. object of type *float* holding one real number and points *x* to that.  When
.. you type *x = 'string'* it creates a new object of type *str* and now points *x*
.. to that, and so on.
En Python a veces es mejor pensar que *x* es simplemente un puntero que apunta a algún objeto.
Cuando tu escribes *x=3.4* Python crea un objeto de tipo *float* que apunta a *x*.
Cuando escribes *x='cadena'* se crea un nuevo objeto de tipo *str* y apunta a *x* etcétera.

.. _lists:

.. Lists
.. -----

Lists
-----

.. We have already seen lists in the example above.  
Ya hemos visto lista en los ejemplos anteriores.

.. Note that indexing in Python always starts at 0::
Tenga en cuenta que la enumeración en Python siempre empieza en 0::

    >>> L = [4,5,6]
    >>> L[0]
    4
    >>> L[1]
    5


.. Elements of a list need not all have the same type.  For example, here's a
.. list with 5 elements::
Elementos de una lista no necesitar ser todos del mismo tipo.
Por ejemplo, aquí hay una lista con 5 elementos::

    >>> L = [5, 2.3, 'abc', [4,'b'], np.cos]

.. Here's a way to see what each element of the list is, and its type::
Aquí hay una forma para ver cada elemento de una lista y su tipo::

    >>> for index,value in enumerate(L):
    ...     print 'L[%s] is %16s     %s' % (index,value,type(value))
    ... 
    L[0] is                5     <type 'int'>
    L[1] is              2.3     <type 'float'>
    L[2] is              abc     <type 'str'>
    L[3] is         [4, 'b']     <type 'list'>
    L[4] is    <ufunc 'cos'>     <type 'numpy.ufunc'>

.. Note that *L[3]* is itself a list containing an integer and a string and
.. that *L[4]* is a function.
Tenga en cuenta que *L[3]* es una lista que contiene un entero y una cadena
y que *L[4]* es una función.

.. One nice feature of Python is that you can also index backwards from the
.. end:  since *L[0]* is the first item, *L[-1]* is what you get going one to
.. the left of this, and wrapping around (periodic boundary conditions in math
.. terms)::

?????Una característica muy buena de Pyhton es que se puede enumerar desde el final al principio:
*L[0]* es el primer elemento, *L[-1]* es el elemento 

    >>> for index in [-1, -2, -3, -4, -5]:
    ...     print 'L[%s] is %16s' % (index, L[index])
    ... 
    L[-1] is    <ufunc 'cos'>
    L[-2] is         [4, 'b']
    L[-3] is              abc
    L[-4] is              2.3
    L[-5] is                5

.. In particular, *L[-1]* always refers to the *last* item in list *L*.
En particular, *L[-1]* siempre se refiere al último elemento de la lista *L*

.. Copying objects
.. ---------------

Copia de objetos
----------------

.. One implication of the fact that variables are just pointers to
.. objects is that two names can point to the same object, which can sometimes
.. cause confusion.  Consider this example::
Una consecuencia del hecho de que las variables son punteros a objeto es que
dos nombres pueden apuntar a un mismo objeto, lo cual puede causar confusión.
Considere este ejemplo::


    >>> x = [4,5,6]
    >>> y = x
    >>> y
    [4, 5, 6]

    >>> y.append(9)
    >>> y
    [4, 5, 6, 9]

.. So far nothing too surprising.  We initialized *y* to be *x* and then we
.. appended another list element to *y*.  But take a look at *x*::
Hasta ahora nada demasiado sorprendente. Inicializamos *y* como *x* y
luego añadimos otro elemento a *y*.
Pero veamos *x*::


    >>> x
    [4, 5, 6, 9]

.. We didn't really append 9 to *y*, we appended it to the object *y* points
.. to, which is the same object *x* points to!
Realmente no añadimos el 9 a *y*, lo añadimos al objeto al que apunta *y*, el cual
es el mismo objeto al que apunta *x*.

.. Failing to pay attention to this sort of thing can lead to programming
.. nightmares.
No prestar atención a este tipo de cosas puede ser una pesadilla al programar.

.. What if we really want *y* to be a different object that happens to be
.. initialized by copying *x*?  We can do this by::
¿Qué hacemos si realmente queremos que *y* sea un objeto diferente pero copia de *x*?
Podemos hacer esto::

    >>> x = [4,5,6]
    >>> y = list(x)
    >>> y
    [4, 5, 6]

    >>> y.append(9)
    >>> y
    [4, 5, 6, 9]

    >>> x
    [4, 5, 6]

.. This is what we want.  Here *list(x)* creates a new object, that is a list,
.. using the elements of the list *x* to initialize it, and *y* points to this
.. new object.  Changing this object doesn't change the one *x* pointed to.
Esto es lo que queremos. Aquí *list(x)* crea un nuevo objeto, que es una lista,
usando los elementos de la lista *x* para inicializarlo.
*y* apunta a ese nuevo objeto. Cambiando este objeto, no cambiamos al que *x* apunta.


.. You could also use the *copy* module, which works in general for any
.. objects::
Se podía usar también el módulo *copy*, el cual trabaja en general con cualquier objeto.
    >>> import copy
    >>> y = copy.copy(x)

.. Sometimes it is more complicated, if the list *x*
..itself contains other objects.  See
.. `<http://docs.python.org/library/copy.html>`_ for more information.
A veces esto es más complicado, si la lista *x* contiene otro tipo de objeto.
Vea:  `<http://docs.python.org/library/copy.html>`_ para más información.

.. There are some objects that cannot be changed once created (*immutable
.. objects*, as described further below).  
.. In particular, for  *floats* and *integers*, you can do things like::
Hay algunos objetos que no pueden ser modificados una vez creados (*objetos inmutables*, como se describe
más adelante)


    >>> x = 3.4
    >>> y = x
    >>> y = y+1
    >>> y
    4.4000000000000004

    >>> x
    3.3999999999999999

.. Here changing *y* did not change *x*, luckily.
.. We don't have to explicitly make a copy of *x* for *y* in this case.  If we
.. did, writing any sort of numerical code in Python would be a nightmare.
Aquí cambiando *y* sin cambiar *x* afortunadamente.
No tenemos que hacer una copia explicitamente de x en este caso.
Si lo hiciésemos, escribir cualquier tipo de código numérico en Python sería una pesadilla.

.. We didn't because the command::
No lo hacemos porque el comando::

    >>> y = y+1

.. above is not changing the object *y* points to, instead it is creating a new
.. object that *y* now points to, while *x* still points to the old object.
No está cambiando el objeto al que *y* apunta, en lugar de esto crea un nuevo objeto
al que *y* apunta ahora, mientras *x* apunta al anterior objeto.

.. For more about built-in data types in Python, see
.. `<http://docs.python.org/release/2.5.2/ref/types.html>`_.
Para más información vea: `<http://docs.python.org/release/2.5.2/ref/types.html>`_.

.. Mutable and Immutable objects
.. -----------------------------
Objetos variables e invariables
-------------------------------


.. Some objects can be changed after they have been created and others cannot
.. be.  Understanding the difference is key to understanding why the examples
.. above concerning copying objects behave as they do.
Algunos objetos se pueden modificar una vez creados y otros no pueden.
Entender la diferencia es la clave para entender por qué los objetos en los 
ejemplos anteriores se comportan como lo hacen.


.. A list is a *mutable* object.  The statement::
??statement??? Una lista es un objeto *variable*.  The statement::

    $ x = [4,5,6]

.. above created an object that *x* points to, and the data held in this object
.. can be changed without having to create a new object.   The statement::
antes creamos un objeto al que apunta *x*, y la información contenida en este objeto
??statement??? se puede cambiar sin tener que crear un nuevo objeto. The statement::

    $ y = x

.. points *y* at the same object, and since it can be changed, any change will
.. affect the object itself and be seen whether we access it using the pointer
.. *x* or *y*.
*y* apunta al mismo objeto, cualquier cambio afectará al propio objeto
y será visto si accedemos usando el puntero *x* o *y*.


.. We can check this by::
Comprobemos esto::

    >>> id(x)
    1823768

    >>> id(y)
    1823768

.. The *id* function just returns the location in memory where the object is
.. stored.  If you do something like `x[0] = 1`, you will find that the
.. objects' id's have not changed, they both point to the same object, but the
.. data stored in the object has changed.
La función *id* devuelve la localización en memoria de donde está almacenado el 
objeto. Si hace algo como 'x[0]=1', verá que el objeto 'id' no ha cambiado, 
ambos apuntan al mismo objeto, pero la información almacenada en el objeto
ha cambiado.

.. Some data types correspond to *immutable* objects that, once created,
.. cannot be changed.  Integers, floats, and strings are immutable::
Algunos tipos de datos son objeto *invariables* que, una vez creados,
no se pueden cambiar. Enteros, reales y cadenas son invariables::

    >>> s = "This is a string"

    >>> s[0]
    'T'

    >>> s[0] = 'b'
    Traceback (most recent call last):
      File "<stdin>", line 1, in <module>
    TypeError: 'str' object does not support item assignment

.. You can index into a string, but you can't change a character in the string.
.. The only way to change *s* is to redefine it as a new string (which will be
.. stored in a **new object**)::
Se puede insertar en una cadena, pero no puede cambair un caracter de la cadena.
La única forma de cambiar *s* es redefinirla como una nueva cadena(que será 
almacenada como un **nuevo objeto**)::

    >>> id(s)
    1850368

    >>> s = "New string"
    >>> id(s)
    1850128

.. What happened to the old object?  It depends on whether any other variable
.. was pointing to it.  If not, as in the example above, then Python's *garbage
.. collection* would recognize it's no longer needed and free up the memory for
.. other uses.  But if any other variable is still pointing to it, the object
.. will still exist, e.g. ::
¿Qué ocurre con el antiguo objeto? Eso depende de si hay otra variable estaba
apuntándole. Si no, como en el ejemplo anterior, Python reconocerá que ya no es
necesario y liberará la memoria para otro uso.
Pero si otra variable aún apunta a él, el objeto permanecerá. ejemplo::

    >>> s2 = s
    >>> id(s2)                     # same object as s above
    1850128 

    >>> s = "Yet another string"   # creates a new object
    >>> id(s)                      # s now points to new object
    1813104

    >>> id(s2)                     # s2 still points to the old one
    1850128

    >>> s2
    'New string'

.. _tuples:

.. Tuples
.. ------
Tuplas
------

.. We have seen that lists are mutable.  For some purposes we need something
.. like a list but that is immuatable (e.g. for dictionary keys, see below).  A
.. tuple is like a list but defined with parentheses `(..)` rather than square
.. brackets `[..]`::
Hemos visto que las listas se pueden variar. 
Para ciertos propósitos necesitamos algo como una lista pero que sea invariable 
(por ejemplo las claves de diccionarios que veremos abajo).
Una tupla es como una lista pero definida con paréntesis en lugar de llaves.

    >>> t = (4,5,6)
    
    >>> t[0]
    4

    >>> t[0] = 9
    Traceback (most recent call last):
      File "<stdin>", line 1, in <module>
    TypeError: 'tuple' object does not support item assignment


.. Iterators
.. ---------
Iteradores
----------

.. We often want to iterate over a set of things.  In Python there are many
.. ways to do this, and it often takes the form::
A veces queremos iterar sobre un conjunto de cosas. En Python hay muchas
formas de hacerlo, y muchas tienen la forma::

    >>> for A in B: 
    ...     # do something, probably involving the current A

.. In this construct *B* is any Python object that is *iterable*, meaning it
.. has a built-in way (when B's class was defined) of starting with one thing
.. in *B* and progressing through the contents of *B* in some hopefully logical
.. order.
?????En esta estructura *B* es cualquier objeto que sea *iterable*,
significando esto

.. Lists and tuples are
.. iterable in the obvious way: we step through it one element at a
.. time starting at the beginning::
???Listas y tuplas son iterables en la manera obvia: 

    >>> for i in [3, 7, 'b']:
    ...     print "i is now ", i
    ... 
    i is now  3
    i is now  7
    i is now  b

.. _range:

range
-----
Rango
-----

.. In numerical work we often want to have i start at 0 and go up to some
.. number N, stepping by one.  We obviously don't want to have to construct the
.. list [0, 1, 2, 3, ..., N] by typing all the numbers
.. when *N* is large, so Python has a way of doing this::
En trabajo numérico a veces queremos tener i que empiece en 0 y acabe en un
número N, pasando de uno en uno.
Obviamente no queremos tener que construir una lista [0,1,2,3,...,N] escribiendo todos 
los números cuando *N* es muy grande, para ello Python tiene la siguiente manera de 
hacerlo::
    >>> range(7)
    [0, 1, 2, 3, 4, 5, 6]

.. NOTE:  The last element is 6, not 7.  The list has 7 elements but starts by
.. default at 0, just as Python indexing does.  This makes it convenient for
.. doing things like::
NOTA: El último elemento es 6, no 7. La lista tiene 7 elementos pero empieza en 0.
Esto es cómodo para hacer cosas como::

    >>> L = ['a', 8, 12]
    >>> for i in range(len(L)):
    ...     print "i = ", i, "  L[i] = ", L[i]
    ... 
    i =  0   L[i] =  a
    i =  1   L[i] =  8
    i =  2   L[i] =  12

.. Note that *len(L)* returns the length of the list, so *range(len(L))* is
.. always a list of all the valid indices for the list *L*.
Vea que *len(L)* devuelve la longitud de la lista, así que
*range(len(L))* es siempre una lista con todos los índices válidos para
la lista *L*.

.. _enumerate:

.. enumerate
.. ---------
enumeración
-------------

.. Another way to do this is::
Otra forma de hacer esto es::

    >>> for i,value in enumerate(L):
    ...     print "i = ",i, "  L[i] = ",value
    ... 
    i =  0   L[i] =  a
    i =  1   L[i] =  8
    i =  2   L[i] =  12


.. *range* can be used with more arguments, for example if 
.. you want to start at 2 and step by 3 up to 20::
*rango* se puede usar con más argumentos, por ejemplo si
quiere empezar en 2 y acabar en 20 dando pasos de 3 en 3::

    >>> range(2,20,3)
    [2, 5, 8, 11, 14, 17]

.. Note that this doesn't go up to 20.  Just like *range(7)* stops at 6, this
.. list stops one item short of what you might expect.
Vea que no llega a 20 de la misma manera que *range(7)* para en 6,
esta lista para un elemento antes del que se espera.

.. NumPy has a *linspace* command that behaves like Matlab's, which is
.. sometimes more useful in numerical work, e.g.::
NumPy tiene un comando *linspace* que se comporta como Matlab's, 
que es a veces muy útil en trabajo numérico, ejemplos::

    >>> np.linspace(2,20,7)
    array([  2.,   5.,   8.,  11.,  14.,  17.,  20.])

.. This returns a NumPy array with 7 equally spaced points between 2 and 20,
.. including the endpoints.  Note that the elements are floats, not integers.
.. You could use this as an iterator too.
??array se traduce??Esto devuelve un array con 7 puntos equiespaciados entre 2 y 20,
incluyendo los puntos finales.
Vea que los elementos son reales, no enteros.
Debería usar esto como un iterador.


.. If you plan to iterate over a lot of values, say 1 million, it may 
.. be inefficient to generate a list object with 1 million elements using
.. *range*.  So there is another option called *xrange*, that does the
.. iteration you want without explicitly creating and storing the list::
Si pretende iterar sobre muchos valores, como 1 millón, puede ser
ineficiente generear una lista con 1 millón de elementos usando *range*.
Así que otra opción es usar *xrange* que hace la iteración que desea sin crear 
explícitamente ni almacenar la lista::

    for i in xrange(1000000):
        # do something

.. does what we want.
hace lo que queremos.

.. Note that the elements in a list you're iterating on need not be numbers.
.. For example, the sample module *myfcns* in $UWHPSC/codes/python defines two
.. functions *f1* and *f2*.  If we want to evaluate each of them at x=3., we
.. could do::
Vea que los elementos de la lista sobre la que está iterando no 
necesitan ser números.
Por ejemplo, el módulo *myfcns* en $UWHPSC/codes/python define dos funciones
*f1* y *f2*. Si queremos evaluar cada una de ellas en x=3., nosotros podemos::

    >>> from myfcns import f1, f2
    >>> type(f1)
    <type 'function'>

    >>> for f in [f1, f2]:
    ...     print f(3.)
    ... 
    5.0
    162754.791419

.. This can be very handy if you want to perform some tests for a set of test
.. functions.
??? Esto puede ser muy práctico si quiere 





Further reading
---------------
Otras lecturas
---------------

See the :ref:`biblio_python` section of the :ref:`biblio`.
Vea: :ref:`biblio_python` section of the :ref:`biblio`.

.. In particular, 
.. see the [Python-2.5-tutorial]_  or [Python-2.7-tutorial]_ for good overviews
.. (these two versions of Python are very similar).
En particular, vea [Python-2.5-tutorial]_  o [Python-2.7-tutorial]_ 
(ambas versiones de Python son similares)

.. There are several introductory Python pages at the [software-carpentry]_
.. site.
Hay varias páginas introductorias de Python en [software-carpentry]_

.. For more on basic data structures: 
Para más información sobre las estructuras de datos básicos:
`<http://docs.python.org/2/tutorial/datastructures.html>`_

