
.. _animation:

Animation in Python
===================
Animaciones en Python
=====================

.. `matplotlib` has a package `animation` that can be used directly, 
.. see for example
.. `<http://matplotlib.org/examples/animation/dynamic_image2.html>`_
.. or `this blog post
.. <http://jakevdp.github.io/blog/2012/08/18/matplotlib-animation-tutorial/>`_.
`matplotlib` tiene un paquete `animation` que se puede usar directamente, 
vea por ejemplo
`<http://matplotlib.org/examples/animation/dynamic_image2.html>`_
o `this blog post
<http://jakevdp.github.io/blog/2012/08/18/matplotlib-animation-tutorial/>`_.


.. Nicer webpage animations (within IPython notebooks or as stand-alone movies)
.. can be created using the package `JSAnimation` created by
.. Jake Vanderplas.  For an example see this `rendered example
.. <http://nbviewer.ipython.org/github/jakevdp/JSAnimation/blob/master/animation_example.ipynb>`_
.. or :ref:`lab15`.
??Las mejores páginas web de animación pueden ser creadas usando el paquete
`JSAnimation` creado por Jake Vanderplas.
Para ejemplos vea `rendered example <http://nbviewer.ipython.org/github/jakevdp/JSAnimation/blob/master/animation_example.ipynb>`_
o :ref:`lab15`


Installing JSAnimation
======================
Instalar JSAnimation
======================

.. First clone it from Github::
Primero clone esto de Github::
    $ cd $HOME
    $ git clone https://github.com/jakevdp/JSAnimation.git
    $ cd JSAnimation
    
.. On your own laptop or the VM, you can probably install it via::
???VM???En su propio ordenador o el VM, probablemente pueda instalarlo via::   
    $ python setup.py install

.. On SageMathCloud you don't have access to the system folder where it
.. normally installs Python packages, but you can install it for use in
.. one project only via::
En GaseMathCloud no tiene acceso a la carpeta del sistema donde normalmente se
instalan los paquetes de Python, pero puede instalarlos para usarlos
en un solo proyecto via::
   
    $ sage -python setup.py install --user

.. Then you should be able to open Python or IPython and `import JSAnimation`
 Luego debe ser capaz de abrir Python o IPython e `import JSAnimation`
 
Alternative to installing
-------------------------
Alternativa para instalar
-------------------------

.. Rather than installing it as a package, 
.. you can just add the JSAnimation directory to 
.. the Python search path.  For running it from scripts in a bash shell, 
.. you might want to add this line to your 
.. Or you can just add $HOME/JSAnimation to your PYTHONPATH environment
.. variable::
En lugar de instalarlo como un paquete, se puede agregar 
el directorio JSAnimation a la ruta de búsqueda de Python.
??bash shell??Para ejecutarlo ...,


    $ export PYTHONPATH=$PYTHONPATH:$HOME/JSAnimation

??This appends the path to the end of whatever path is already specified in
this environment variable.

.. As a last resort, you can also modify the path from within a Python
.. session::
Como último recurso, también puede modificar la ruta de acceso desde una sesión Python ::

    >>> import os, sys
    >>> HOME = os.environ['HOME']
    >>> JSAnimation_path = os.path.join(HOME, 'JSAnimation')
    >>> sys.path.append(JSAnimation_path)


??JSAnimation_frametools
----------------------

For animations of complex plots, it is sometimes easier to simply
create the plot for each frame as usual using `matplotlib` and then
save a `.png` file for each frame.  These can be created and then
combined to create an animation using some tools in
`JSAnimation_frametools.py`, currently found in :ref:`lab15` along
with some demos.

Matplotlib issues
------------------
Cuestiones de Matplotlib 
-----------------------

.. JSAnimation requires a recent version of `matplotlib`.
.. (In particular, older Ubuntu versions may not have a recent version.)
.. If you're having problems with `matplotlib` in this context, you might
.. want to try using the `Anaconda Python distribution
.. <https://store.continuum.io/cshop/anaconda>`_, or switch to :ref:`smc`.
JSAnimation requiere una versión reciente de `matplotlib`.
(En particular, versiones anteriores de Ubuntu pueden no tener una versión reciente.)
Si está teniendo problemas de este tipo con `matplotlib`, puede intentar usar
`Anaconda Python distribution <https://store.continuum.io/cshop/anaconda>`_, o cambiar a :ref:`smc`.

