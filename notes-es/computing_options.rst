
.. _computing_options:

.. ================================
   Computing Options [2014 version]
   ================================

===================================
Opciones de cálculo [versión 2014]
===================================

.. All of the software we will use this year is open source, so in principle
   you can install it all on your own computer.  See :ref:`software_installation` 
   for some tips on doing so.

Todo el software que usarás este año es libre, así que en principio puedes instalarlo todo en tu propio
ordenador. Ver :ref:`software_installation` para obtener algunas indicaciones para hacerlo.

.. However, there are several reasons you might want to use a different
   computing environment for this class:

Sin embargo, hay algunas razones por la que quieras usar un entorno de cálculo distinto para esta clase:

.. * To avoid having to install many packages yourself,
.. * To make sure you have the same computing environment as fellow students 
     and the TAs,
.. * To have access to a multi-core machine if your own computer is has a
     single processor, since much of the course material concerns parallel computing.
.. * To work together during lab sessions.

* Para evitar tener que instalar muchos paquetes por ti mismo,
* Para asegurarte que tienes el mismo entorno de cálculo que tus compañeros y TAs,
* Para tener acceso a una máquina multi-núcleo si tu ordenador tiene un sólo procesador, ya que mucho del
  material de este curso trata sobre cálculo en paralelo.
* Para trabajar juntos en las sesiones de laboratorio.

.. _options_smc:

SageMathCloud
--------------

.. This is the recommended computing platform  and what we will mostly use
   during the T-Th lab sessions.  SageMathCloud is a freely
   available cloud computing resource developed by the Sage Team, led by
   Prof. William Stein in the UW Mathematics Department.  You can easily create
   an account at `SageMathCloud <https://cloud.sagemath.com/>`_.

Esta es la plataforma de cálculo recomendada y que usaremos mayoritariamente durante las sesiones de
laboratorio. SageMathCloud es un recurso de cálculo en la nube libre desarrollado por Sage Team, liderado
por Prof. William Stein de UW Mathematics Department. Puedes crearte una cuenta fácilmente en 
`SageMathCloud <https://cloud.sagemath.com/>`_.

.. You should create an account using your UW email address `netid@uw.edu`.
   This will make it easiest for us to add you as a collaborator on projects.

Deberías crearte una cuenta usando tu dirección de correo de la UW `netid@uw.edu`. Esto nos hará más fácil
que podamos añadirte como colaborador en los proyectos.

.. All of the software needed this quarter is installed on SageMathCloud.

Todo el software que necesitas este cuatrimestre está instalado en SageMatheCloud.

.. Use of SageMathCloud will be demonstrated during the first Lab session on
   Tuesday April 1, 2014.  

Se mostrará como usar SageMathCloud durante la primera sesión de laboratorio el Martes 1 de Abril de 20014.

.. For some tips on using it, see :ref:`smc`.

Para algunas indicaciones de como usarlo, ver :ref:`smc`.

.. _options_vm:

.. Virtual Machine
   ---------------

Máquina Virtual
---------------

.. If you want to be able to compute on your own computer but don't want to
   try installing all the necessary software packages 
   individually, another option is to
   run a *virtual machine* using the VirtualBox software.  See :ref:`vm`
   for more information.

Si quieres poder hacer los cálculos en tu propio ordenador pero no quieres instalar todos los paquetes de
software necesarios de manera individual, otra opción es ejecutar una *máquina virtual* usado el software
VirtualBox. Ver :ref:`vm` para más información.

.. Amazon Web Services
   -------------------

.. Another possibility for cloud computing is to use Amazon Web Services.  
.. An Amazon Machine Image (a virtual machine) has been created that has all
.. the software needed for this class.  For more detail on how to launch an
.. instance running this AMI, see :ref:`aws`.

Otra posibilidad para el cálculo en la nube es usar Amazon Web Services.  Una Amazon Machine Image (una 
máquina virtual) ha sido creada de forma que contenga todo el software que necesitas para esta clase. Para
más detalles de como ejecutar una orden ejecutando esta AMI, ver :ref:`aws`.
