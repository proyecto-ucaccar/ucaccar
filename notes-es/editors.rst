
.. _editors:

.. =============================================================
   Text editors
   =============================================================

===================
Editores de textos
===================

Leafpad
-------

.. A simple and lightweight editor, installed on the course VM.

Un editor simple y ligero, instalado en la VM del curso.

.. To use it, click on the green leaf in the bottom menu, or at the bash
   prompt::

Para usarlo, haz click en la hoja verde en el menú de abajo, o en el prompt de bash::

    $ leafpad <filename>


gedit
-----

.. ``gedit`` is a simple, easy-to-use text editor with support for syntax
   highlighting for a variety of programming languages, including Python
   and Fortran.  It is installed by default on most GNOME-based Linux
   systems, such as Ubuntu.  It can be installed on the course VM via::

``gedit`` es un editor de texto simple, sencillo de usar con ayuda marcando las sintasis en una gran variedad
de lenguajes de programación, incluyendo Python y Fortran. Está instalado por defecto en la mayoría de
sistemas Linux basados en GNOME, como Ubuntu. Puede ser instalado en la VM via::

    $ sudo apt-get install gedit

NEdit
-----

.. Another easy-to-use editor, available from `<http://www.nedit.org/>`_.
   NEdit is available for almost all Unix systems, including Linux and
   Mac OS X; OS X support requires an X Windows server.

Otro editor fácil de usar, disponible en `<http://www.nedit.org/>`_. NEdit está disponible para la mayoría
de sistemas Unix, incluyendo Linux y Mac OS X; el soporte de OS X requiere de un servidor de X Windows. 

XCode
-----

.. XCode is a free integrated development environment available for Mac
   OS X from Apple at
   `<http://developer.apple.com/technologies/tools/xcode.html>`_.  It
   should be more than adequate for the needs of this class.

XCode es un entorno de desarrollo integrado gratuito disponible para Mac OS X desde Apple en
`<http://developer.apple.com/technologies/tools/xcode.html>`_. Debería ser más que adecuado para lo que
necesitamos en esta clase.


TextWrangler
------------

.. TextWrangler is another free programmer's text editor for Mac OS X,
   available at `<http://www.barebones.com/products/TextWrangler/>`_.

TextWrangler es otro editor de texto gratuito para Mac OS C, disponible en
`<http://www.barebones.com/products/TextWrangler/>`_.

.. vi or vim
   ---------

vi o vim
---------

.. ``vi`` ("Visual Interface") / ``vim`` ("``vi`` iMproved") is a fast,
   powerful text editor.  Its interface is extremely different from
   modern editors, and can be difficult to get used to, but ``vi`` can
   offer substantially higher productivity for an experienced user.  It
   is available for all operating systems; see `<http://www.vim.org/>`_
   for downloads and documentation.

``vi`` ("Visual Interface") / ``vim`` ("``vi`` iMproved") es un poderoso y rápido editor de texto. Su interfaz
es extremadamente diferente de los editores modernos y puede ser dificil de acostumbrarse a ella, pero ``vi``
puede ofrecer una productividad más alta para un usuario con experiencia. Está disponible para todos los
sistemas operativos; ver <http://www.vim.org/>`_ para descargas y documentación.

.. A command line version of ``vi`` is already installed in the class VM.

Una versión de línea de comando de ``vi`` ya está instalada en la VM de clase.

emacs
-----

.. ``emacs`` ("Editing Macros") is another powerful text editor.  Its
   interface may be slightly easier to get used to than that of ``vi``,
   but it is still extremely different from modern editors, and is also
   extremely different from ``vi``.  It offers similar productivity
   benefits to ``vi``.  See `<http://www.gnu.org/software/emacs/>`_ for
   downloads and documentation.  On a Debian or Ubuntu Linux system, such
   as the class VM, you can install it via::

``emacs`` ("Editing Macros") es otro poderoso editor de textos, Su interfaz es ligeramente más fácil de usar
que la de ``vi``, pero aún así es extremadamente distinta de los editores modernos, y es también
extremadamente distinta de ``vi``. Ofrece unos beneficios en productividad similares a ``vi``. Ver
`<http://www.gnu.org/software/emacs/>`_ para descargas y documentación. En sistemas Debian o Ubuntu, como la
VM de clase, puedes instalarlo via::

    $ sudo apt-get install emacs

