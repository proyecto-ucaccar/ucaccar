
.. _special_functions:

.. =============================================================
   Special functions
   =============================================================

=====================
Funciones especiales
=====================

.. There are many functions that are used so frequently that they are given
   special names. Familiar examples are sin, cos, sqrt, exp, log, etc.

Hay muchas funciones que se usan tan frecuentemente que se les dan nombres especiales. Ejemplos familiares
son sin, cos, sqrt, exp, log, etc.

.. Most programming languages have build-in (intrinsic) functions with these
   same names that can be called to compute the value of the function for
   arbitrary arguments. 

La mayoría de los lenguajes de programación tienen construidas (intrínsecas) funciones con estos mismos
nombres que puedan ser llamados para calcular el valor de la función para argumentos arbitrarios.

.. But "under the hood" these functions must be evaluated somehow.  The basic
   arithmetic units of a computer are only capable of doing very basic
   arithmetic in hardware: addition, subtraction, multiplication, and division.
   Every other function must be *approximated* by applying some sequence of
   arithmetic operations.

Pero "tras el telón" estas funciones deben ser evaluadas de alguna manera. Las unidades arimética básicas
de un ordenador sólo son capaces de hacer aritmética muy básica con el hardware: sumas, restas,
multiplicaciones y divisiones. Cualquier otra función debe ser *aproximada* aplicando alguna secuencia de
operaciones ariméticas.

.. There are several ways this might be done....

Hay muchas maneras en la que se puede hacer esto...

Taylor series expansions
------------------------

.. A finite number of terms in the Taylor series expansion of a function gives
   a polynomial that approximates the desired function.  Polynomials can be
   evaluated at any point using basic arithmetic.

Un número finito de términos de la expansión en la serie de Taylor de una función nos da un polinomio que
aproxima a la función deseada. Los polinomios pueden ser evaluados en cualquier punto usando aritmética
básica.

.. For example, the section on :ref:`fortran_taylor` discusses an
   implementation of the Taylor series approximation to the exponential
   function,

Por ejemplo, la sección :ref:`fortran_taylor` habla sobre la implementación de la aproximación de la
exponencial mediante series de Taylor:

    :math:`exp(x) = 1 + x + \frac 1 2 x ^2 + \frac 1 6 x^3 + \cdots`

.. If this is truncated after the 4 terms shown, for example, we obtain a
   polynomial of degree 3 that is easily evaluated at any point.

Si esto se trunca después de los 4 términos mostrados, por ejemplo, obtenemos un polinomio de grado 3 que es
fácilmente evaluado en cualquier punto.

.. Some other Taylor series that can be used to approximate functions:

Algunas otras serires de Taylor que se pueden considerar para aproximar funciones:

    :math:`sin(x) = x - \frac{1}{3!} x^3 + \frac 1 {5!} x^5 - \frac{1}{7!} x^7 + \cdots`

    :math:`cos(x) = 1 - \frac 1 2 x^2 + \frac{1}{4!} x^4 - \frac{1}{6!} x^6 + \cdots`

.. _special_newton:

.. Newton's method for the square root
   -----------------------------------

Método de Newton para la raíz cuadrada
---------------------------------------

.. One way to evaluate the square root function is to use *Newton's method*, a
   general procedure for estimating a *zero* of a function :math:`f(x)`, i.e. a 
   value :math:`x^*` for which :math:`f(x^*) = 0`.

Una manera de evaluar la raíz cuadrada es usar el *método de Newton*, un procedimiento general para estimar
un *zero* de una función :math:`f(x)` , esto es un valor :math:`x^*` tal que :math:`f(x^*) = 0` .

.. The square root of any value :math:`a>0` is a zero of the function :math:`f(x)
   = x^2 - a`.  (This function has two zeros, at :math:`\pm\sqrt{a}`.

La raíz cuadrada de cualquier valor :math:`a>0` es un cero de la función :math:`f(x)= x^2 - a` . (Esta función tiene dos ceros, en :math:`\pm\sqrt{a}` ).

.. Newton's method is based on taking a current estimate :math:`x_k` to
   :math:`x^*` and (hopefully) improving it by setting 

El método de Newton se basa en tomar una estimación actual :math:`x_k` de :math:`x^*` y (con suerte) mejorarla
estableciendo

    :math:`x_{k+1} = x_k - \delta_k`

.. The increment :math:`\delta_k` is determined by *linearizing* the function
   :math:`f(x)` about the current esimate :math:`x_k` using only the linear
   term in the Taylor series expansion.  This linear function :math:`G_k(x)` is

El incremento :math:`\delta_k` está determinado por la *linearización* de la función :math:`f(x)` sobre la
estimación actual :math:`x_k` usando solo el término lineal de la expansión en serie de Taylor.

    :math:`G_k(x) = f(x_k) + f'(x_k)(x-x_k)` .

.. The next point :math:`x_{k+1}` is set to be the zero of this linear
   function, which is trivial to find, and so

El siguiente punto :math:`x_{k+1}` se establece que sea el cero de esta función linal, lo cual es trivial
de encontrar, y así

    :math:`\delta_k = f(x_k) / f'(x_k)` .

.. Geometrically, this means that we move along the tangent line to
   :math:`f(x)` from the point :math:`(x_k,f(x_k))` to the point where this
   line hits the x-axis, at :math:`(x_{k+1}, 0)`.  See the figures below.

Geometricamente, esto significa que nos movemos a lo largo de la recta tangente de :math:`f(x)` desde 
:math:`(x_k,f(x_k))` al punto donde esta línea toca al eje X, en :math:`(x_{k+1}, 0)` . Ver figura inferior.

.. There are several potential difficulties with this approach, e.g.

Hay varias dificultades potenciales con esta aproximación, por ejemplo.

.. * To get started we require an *initial guess* :math:`x_0`.
   * The method may converge to a zero from some starting points but not
     converge at all from others, or may converge to different zeros depending
     on where we start.

* Para comenzar se requiere un *acierto inicial* :math:`x_0` .  

* Este método puede converger a un cero desde algunos valores iniciales pero no converge para todos ellos,
  o puede converger a distintos ceros  dependiendo de donde comencemos.

.. For example, applying Newton's method to find a zero of :math:`f(x) = x^2-2`
   coverges to :math:`\sqrt{2} \approx 1.414` if we start at :math:`x_0=0.5`,
   but convergest o :math:`-\sqrt{2} \approx -1.414` if we start at
   :math:`x_0=-0.5` as illustrated in the figures below.

Por ejemplo, aplicando el método de Newton para encontrar el cero de :math:`f(x) = x^2-2` converge a
:math:`\sqrt{2} \approx 1.414` si comenzamos en :math:`x_0=0.5` , pero converge a
:math:`-\sqrt{2} \approx -1.414` si comenzamos en :math:`x_0=-0.5` como se ve en la figura inferior.

.. image:: images/newtonsqrt2m.png
   :width: 10cm
.. image:: images/newtonsqrt2.png
   :width: 10cm

.. An advantage of Newton's method is that it is guaranteed to converge to a
   root provided the function :math:`f(x)` is smooth enough and the starting
   guess :math:`x_0` is sufficiently close to the zero.  Moreover, in general
   one usually observes *quadratic convergence*.  This means that once we get
   close to the zero, the error roughly satisfies

Una ventaja del método de Newton es que se garantiza la convergencia a una raíz dada una función :math:`f(x)`
suficientemente suave y con el valor inicial :math:`x_0` suficientemente cerca del cero. Más aún, en general
normalmete se observa *convergencia cuadrática*. Esto significa que una vez que estamos cerca del cero, el
error satisface

    :math:`|x_{k+1} - x^*| \approx C|x_k-x^*|^2`

.. The error is roughly squared in each step.  In practice this means that once
   you have 2 correct digits in the solution, the next few steps will produce
   approximations with roughly 4, 8, and 16 correct digits (doubling each
   time), and so it rapidly converges to full machine precision.

El error se eleva el cuadrado aproximadamente en cada paso. En la práctica esto significa que una vez que
tengamos dos dígitos correctos en la solución, los siguientes pasos producirán aproximaciones con
aproximadamente 4, 8 y 16 dígitos correctos (doblándolos cada vez), así que rápidamente converge a la 
precisión completa de la máquina.

.. For example, the approximations to :math:`\sqrt{2}` generated by Newton's
   method starting at :math:`x_0=0.5` are::

Por ejemplo, las aproximaciones a :math:`\sqrt{2}` generadas por el método de Newton empezando en
:math:`x_0=0.5` son::

    k, x, f(x):   1    0.500000000000000E+00   -0.175000E+01
    k, x, f(x):   2    0.225000000000000E+01    0.306250E+01
    k, x, f(x):   3    0.156944444444444E+01    0.463156E+00
    k, x, f(x):   4    0.142189036381514E+01    0.217722E-01
    k, x, f(x):   5    0.141423428594007E+01    0.586155E-04
    k, x, f(x):   6    0.141421356252493E+01    0.429460E-09
    k, x, f(x):   7    0.141421356237310E+01    0.444089E-15

.. The last value is correct to all digits.

El último valor tiene correctos todos sus dígitos.

.. There are many other methods that have been developed for finding zeros of
   functions, and a number of software packages that are designed to be more
   robust than Newton's method (less likely to fail to converge) while still
   converging very rapidly.

Hay muchos otros métodos que han sido desarrollados para encontrar ceros de funciones, y un gran número de paqetes de software están diseñados para ser más robustos que el método de Newton (menos propensos a fallar
en la convergencia) mientras que aún convergen muy rápidos.

