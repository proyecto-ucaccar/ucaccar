
.. _python_plotting:

.. =============================================================
.. Plotting with Python 
.. =============================================================

=============================================================
Gráficos con Python 
=============================================================

.. _pylab:

 ?????? matplotlib and pylab
--------------------

.. There are nice tools for making plots of 1d and 2d data (curves, contour
.. plots, etc.) in the module 
.. `matplotlib <http://matplotlib.sourceforge.net/>`_.
.. Many of these plot commands are very similar to those in Matlab.

Hay buenas herramientas para hacer gráficos en 1 dimensión y 2 dimensiones (curvas, contornos, representaciones...)
en el módulo `matplotlib <http://matplotlib.sourceforge.net/>`_.
Muchos de esos comandos son muy similares a otros de Matlab.

.. To see some of what's possible (and learn how to do it), visit the 
.. `matplotlib gallery <http://matplotlib.sourceforge.net/gallery.html>`_.
.. Clicking on a figure displays the Python commands needed to create it.
Para ver algunas (y aprender cómo hacerlo), vea
`matplotlib gallery <http://matplotlib.sourceforge.net/gallery.html>`_.
Al hacer clic en una figura muestra los comandos de Python necesarios para crearlo.

.. The best way to get matplotlib working interactively in a standard Python
.. shell is to do::
La mejor manera de trabajar de forma interactiva matplotlib en un terminal de Python estándar es hacer:

    $ python
    >>> import pylab
    >>> pylab.interactive(True)

.. *pylab* includes not only *matplotlib* but also *numpy*.  
.. Then you should be able to do::
*pyLab* incluye no solo *matplotlib* sino también *numpy*.
Con esto debe ser capaz de hacer:
    >>> x = pylab.linspace(-1, 1, 20)
    >>> pylab.plot(x, x**2, 'o-')

.. and see a plot of a parabola appear.  You should also be able to use the
.. buttons at the bottom of the window, e.g click the
.. magnifying glass and then use the mouse to select a rectangle in the plot to
.. zoom in.

y verá aparecer el gráfico de una parábola.
También debe ser capaz de utilizar los botones en la parte inferior de la ventana, 
haga clic en la lupa y luego use el ratón para seleccionar un rectángulo y acercar la imagen.


.. Alternatively, you could do::
De forma alternativa, puede:
    >>> from pylab import *
    >>> interactive(True)

.. With this approach you don't need to start every pylab function name with
.. pylab, e.g.::

Con esta manera, no necesita empezar cada función pylab nombrando pylab, ejemplo:

    >>> x = linspace(-1, 1, 20)
    >>> plot(x, x**2, 'o-')

.. In these notes we'll generally use module names just so it's clear where
.. things come from.

??????En estas notas, usaremos generalmente los nombres de los módulos, de forma que es claro saber de dónde vienen las cosas. 

.. If you use the IPython shell, you can do::
Usando el terminal de IPython, se puede hacer:

    $ ipython --pylab

    In [1]: x = linspace(-1, 1, 20)
    In [2]: plot(x, x**2, 'o-')

.. The --pylab flag causes everything to be imported from pylab and set up for
.. interactive plotting.

???La bandera --pylab hace que todo sea importada desde pylab y configurado para el trazado interactivo

.. _mayavi:

?? Mayavi and mlab
---------------

.. Mayavi is a Python plotting package designed primarily for 3d plots.  See:
Mayavi es un paquete de Python diseñado principalmente para gráficos 3D. Vea:

 * `Documentation <http://code.enthought.com/projects/mayavi/docs/development/html/mayavi/index.html>`_

 * `Gallery <http://code.enthought.com/projects/mayavi/docs/development/html/mayavi/auto/examples.html>`_

.. See :ref:`software_installation` for some ways to install Mayavi.
Vea: ref:`software_installation` para instalar Mayavi.

.. _visit:

VisIt
-----

.. VisIt is an open source visualization package being developed at Lawrence Livermore
.. National Laboratory.  It is designed for industrial-strength visualization problems
.. and can deal with very large distributed data sets using MPI.

VISIT es un paquete de visualización de código abierto que se está desarrollando en el Laboratorio Nacional Lawrence Livermore. 
Está diseñado para los problemas de visualización de potencia industrial y puede hacer frente a grandes conjuntos
de datos distribuidas utilizando MPI.

.. There is a GUI interface and also a Python interface for scripting.
Hay una interfaz GUI y también una interfaz de Python para escritura.

.. See:
Vea:

 * `Documentation <https://wci.llnl.gov/codes/visit/doc.html>`_

 * `Gallery <https://wci.llnl.gov/codes/visit/gallery.html>`_

 * `Tutorial <http://www.visitusers.org/index.php?title=Short_Tutorial>`_

.. _paraview:

ParaView
--------

.. ParaView is another open source package developed originally for work at the
.. National Labs.  
ParaView es otro paquete de código abierto desarrollado originalmente para el trabajo en el laboratorio nacional.


.. There is a GUI interface and also a Python interface for scripting.
??????? repetido ????? Hay una interfaz GUI y también una interfaz de Python para escritura.

.. See:
Vea:

 * `Documentation <http://www.paraview.org/paraview/help/documentation.html>`_

 * `Gallery <http://www.paraview.org/paraview/project/imagegallery.php>`_


