
.. ..  _about:
.. _Acerca de:

.. =============================================================
.. About these notes -- important disclaimers:
.. =============================================================

=========================================================
Acerca de estas notas -- renuncia de responsabilidades
=========================================================

.. These note on high performance scientific computing are being developed for
.. the course `Applied Mathematics <http://www.amath.washington.edu/>`_ `483/583
.. <http://www.amath.washington.edu/courses/583-spring-2014/index.html>`_ 
.. at the `University of Washington
.. <http://www.washington.edu>`_, Spring Quarter, 2014.  

Estas notas en computación científica de alto rendimiento están siendo
desarrolladas para el curso `Applied Mathematics <http://www.amath.washington.edu/>`_ `483/583 <http://www.amath.washington.edu/courses/583-spring-2014/index.html>`_ en la `University of Washington <http://www.washington.edu>`_, Segundo semestre, 2014.

.. They are very much a work in progress.  Many pages are not yet here and the
.. ones that are will mostly be modified and supplemented as the quarter
.. progresses.   

Éstas son un trabajo en progreso. Muchas páginas no están aún aquí y las que
están serán modificadas y suplimentadas a medida que el curso avance.

.. It is not intended to be a complete textbook on the subject, by any means.
.. The goal is to get the student started with a few key concepts and
.. techniques and then encourage further reading elsewhere.  
.. So it is a collection of brief introductions to various important topics
.. with pointers to books, websites, and other references for more details.
.. See the :ref:`biblio` for more references and other suggested readings.

No se pretende crear un libro de texto de la asignatura, bajo ningún concepto.
La meta es iniciar al estudiante con unos pocos conceptos y técnicas claves y
entonces animarlo a que profundice en cualquier otra parte. Así estas notas son
breves introducciones a varios temas importantes con enlaces a libros, páginas
web, y otras referencias para más detalle. Ver :ref:`biblio` para más
referencias y otras lecturas recomendadas.

.. There are many pointers to Wikipedia pages sprinkled through the notes and
.. in the bibliography, simply because these pages often give a good overview
.. of issues without getting into too much detail.  They are not necessarily
.. definitive sources of accurate information.  

Hay muchos enlaces a la Wikipedia esparcidos por las notas y en la bibliografía,
simplemente porque estas páginas en ocasiones dan una buena perspectiva global
de los temas a tratar sin entrar en demasiados detalles. No hay ninguna fuente
de información definitiva.

.. These notes are mostly written in Sphinx (see :ref:`sphinx`) and the input
.. files are available using git (see :ref:`classgit`).

Estas notas están escritas en su gran mayoría usando Sphinx (:ref:`sphinx`) y los
archivos de entrada están disponibles usando git (ver :ref:`classgit`).

.. .. _license:
.. _licencia:

.. License
.. -------

Licencia
--------

.. These notes are being made freely available and are released under the 
.. `Creative Commons <http://creativecommons.org/>`_ `CC BY license
.. <http://creativecommons.org/licenses/by/3.0/>`_.
.. This means that you are welcome to use them and quote from them
.. as long as you give appropriate attribution.

Estas notas están disponible de forma libre y son lanzadas bajo
`Creative Commons <http://creativecommons.org/>`_ `CC BY license  <http://creativecommons.org/licenses/by/3.0/>`_.
Esto significa que eres libre para usarlas y citarlas siempre y cuando  hagas una atribución adecuada.

.. Of course you should always do this with any material you find on the web or
.. elsewhere, provided you are allowed to reuse it at all.  

Desde luego deberías hacer esto con cualquier material que encuentres en la red o en cualquier otra parte, siempre y cuando
se te permita poder reutilizarlo.

.. You should also give some thought to licensing issues whenever you post your
.. own work on the web, including computer code.
.. Whether or not you want others to be able to make use
.. of it for various purposes, make your intentions known.

También deberías pensar sobre temas de licencias cuando publiques tu propio trabajo en la red, incluyendo código fuente.
Si quieres o no que los demás puedan usarlo para propósitos varios, hazlo saber.
