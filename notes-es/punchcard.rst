

.. _punchcard:

.. ===============================================
   Punch cards
   ===============================================

=====================
Tarjetas perforadas
=====================


.. Once upon a time (through the 1970s) many computer programs were written on
   punch cards of the type shown here [`image source
   <http://en.wikipedia.org/wiki/File:FortranCardPROJ039.agr.jpg>`_]:

Érase una vez (durante los 70s) programas de ordenador escritos en tarjetas perforadas como la que se muestran
aquí [`image source
<http://en.wikipedia.org/wiki/File:FortranCardPROJ039.agr.jpg>`_]:

.. image:: images/FortranCardPROJ039-agr.jpg
   :width: 20cm


.. This is a form of binary memory (see :ref:`memory`)
   where a specific set of locations each has a
   hole punched (representing 1) or not (representing 0).
   Each character typed on the top line is represented by the bits reading down
   the corresponding column, e.g. the first character Z is represented by
   001000000001 since there are only two holes punched in this column.

Esto es una forma de memoria binaria (ver :ref:`memory`) donde un conjunto específico de lugares
tiene un agujero perforado (representando 1) o no (representando 0). Cada caracter escrito en la línea
superior está representado por sus bits leidos abajo en su columna correspondiente, por ejemplo, el primer
caracter Z está representado por 001000000001 ya que sólo hay dos perforados en esta columna.

.. When programs were written on cards, one card was required for each line of
   the program.  The early conventions of the :ref:`fortran` programming
   language are related to the columns on a punch card.  
   Only the first 72 columns were used for the program statements.  The last 8
   columns could be used to print a card number, so that the unlucky programmer
   who dropped a deck of cards had some chance of reordering them properly.
   Many Fortran 77  compilers still ignore any characters beyond column 72 in a
   line of a program, leading to bugs that can be hard to find if care is not 
   taken, e.g. a called *xnew* might be truncated to *x* if the *x* is in
   column 72.  If the program also uses a variable called *x*, this will not be
   caught by the compiler.

Cuano los programas se escribían en tarjetas, se requería una tarjeta por cada línea del programa.
Las primeras convenciones del lenguaje de programación :ref:`fortran` estaban relacionadas con las columnas
en una tarjeta perforada. Sólo las primeras 72 columnas se usaban para comandos del programa. Las últimas 8
columnas se podían usar para escribir el número de la tarjeta, así al desafortunado programador al que se
le cayera el mazo de tarjetas tendría una oportunidad de reordearlas propiamente. Muchos compiladores de
Fortran 77 todavía ignoran las columnas más allá de la 72 en una línea de programa, conduciendo a bugs que
pueden ser difíciles de encontrar si no se tiene cuidado, por ejemplo, una llamada a *xnew* puede truncarse
a *x* si la *x* está en la columna 72. Si el programa también usa una variable llamada *x*, esto no lo
detectará el compilador.

.. A program would require a *punch card deck* as shown in this photo
   [`image source
   <http://en.wikipedia.org/wiki/File:PunchCardDecks.agr.jpg>`_]:

Un  pograma requiriría un *mazo de tarjetas perforadas* como las que se muestran en esta foto
[`image source
<http://en.wikipedia.org/wiki/File:PunchCardDecks.agr.jpg>`_]:

.. image:: images/PunchCardDecks-agr.jpg
   :width: 15cm


.. Punch cards were a great step forward from *punched tape* `[wikipedia]
   <http://en.wikipedia.org/wiki/Punched_tape>`_ where a long strip of paper
   was used to store the entire program (and making a mistake required retyping
   more than just one card).

Las tarjetas perforadas fueron un gran paso hacia adelante desde las *cintas perforadas*. 
`[wikipedia]
<http://en.wikipedia.org/wiki/Punched_tape>`_ donde se usaba una larga tira de papel para almacenar un
programa entero (y cometer un error requería escribir más que una sola tarjeta).
